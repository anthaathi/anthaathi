pluginManagement {
    val quarkusPluginVersion: String by settings
    val quarkusPluginId: String by settings

    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
    }
    plugins {
        id(quarkusPluginId) version quarkusPluginVersion
    }
}

rootProject.name = "anthaathi"

include(
    ":apps:anthaathi-commerce-mobile-client",
    ":apps:anthaathi-crm-web-client",
    ":apps:anthaathi-authentication",
    ":apps:anthaathi-commerce-typesense-sync",
    ":apps:anthaathi-commerce:integration:payment:anthaathi-hyper-payment-integrator",
    ":apps:anthaathi-graphql-gateway",
    ":apps:anthaathi-commerce:modules:anthaathi-commerce-address",
    ":apps:anthaathi-commerce:modules:anthaathi-commerce-customer"
)

include(
    ":apps:anthaathi-commerce-checkout-engine",
    ":apps:anthaathi-commerce-product-engine",
    ":apps:anthaathi-common-proto",
)

include(
    ":tools:node-tooling"
)
include("apps:anthaathi-common-proto")
