import React, {useMemo} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ProductListPage from '../../pages/ProductListPage';
import categoryJson from '../../config/category.json';
import {Colors, useTheme} from 'react-native-paper';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../types/Route';
import {MD3Colors} from 'react-native-paper/lib/typescript/types';
import Header from '../../features/CMS/containers/Header';
import {SafeAreaView} from 'react-native';
import {gql, useQuery} from '@apollo/client';

const Tab = createMaterialTopTabNavigator();

export const getCategoryQuery = gql`
  query getCategoryByHandle {
    categories {
      total_count
      items {
        id
        name
        children_count
        children {
          id
          name
          image
          url_key
          children_count
          children {
            id
            name
          }
        }
      }
    }
  }
`;

export function ProductTopTab(
  props: NativeStackScreenProps<RootStackParamList, 'ProductListPage1'>,
) {
  const {loading, data: dataCategory} = useQuery(getCategoryQuery);

  const categoryList = useMemo(
    () => (loading ? [] : dataCategory?.categories?.items[0].children),
    [dataCategory?.categories?.items, loading],
  );

  const theme = useTheme();
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#FFF'}}>
      <>
        <Header
          leftIcon={'arrow-left'}
          leftOnPress={() => props.navigation.goBack()}
          languageIcon={true}
          cartIcon={true}
          cartOnPress={() => {
            props.navigation.navigate('CartPage');
          }}
          mailIcon={false}
          searchIcon={true}
          logoImage={
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x.png?v=1653569545'
          }
        />

        {!loading && (
          <Tab.Navigator
            initialRouteName={props?.route?.params?.categoryName}
            style={{marginVertical: 5}}
            screenOptions={{
              tabBarScrollEnabled: true,
              tabBarItemStyle: {
                height: 34,
                minHeight: 30,
                backgroundColor: Colors.green400,
                padding: 0,
                marginVertical: 4,
                marginHorizontal: 6,
                borderRadius: 50,
              },
              tabBarStyle: {
                padding: 0,
                margin: 0,
                height: 42,
              },
              tabBarLabelStyle: {
                color: '#FFF',
              },
              tabBarIndicatorStyle: {
                backgroundColor: (theme.colors as MD3Colors).background,
              },
              lazy: true,
            }}>
            {categoryList.map((data: any) => (
              <Tab.Screen
                name={data.url_key}
                component={ProductListPage}
                key={data.id}
                options={{tabBarLabel: data.name}}
              />
            ))}
          </Tab.Navigator>
        )}
      </>
    </SafeAreaView>
  );
}
