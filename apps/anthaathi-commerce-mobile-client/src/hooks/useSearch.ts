import Typesense from 'typesense';
import {SearchParams} from 'typesense/lib/Typesense/Documents';
import {TSProductProps} from '../pages/ProductListPage';

let client = new Typesense.Client({
  nodes: [
    {
      host: '192.168.0.109',
      port: 8108,
      protocol: 'http',
    },
  ],
  apiKey: 'xyz',
  connectionTimeoutSeconds: 10,
});

export const useSearch = () => {
  async function getRecords(
    title: string,
    query_by?: string,
    page_no?: number,
    filter_by?: string,
  ) {
    const records: TSProductProps[] = [];
    let found: number = 0;
    let searchParameters: SearchParams = {
      q: title,
      filter_by: filter_by, //'collections:= [juice, fruits]'
      query_by: query_by ? query_by : 'title',
      page: page_no,
      per_page: 250,
    };
    await client
      .collections('products')
      .documents()
      .search(searchParameters)
      .then(function (searchResults) {
        if (searchResults.found > 0 && title !== '') {
          searchResults.hits?.map((data: any) => {
            records.push(data.document);
          });
          found = searchResults.found;
        }
      });
    return {
      records: records,
      hasNextPage: page_no ? found / (page_no * 250) >= 1 : false,
    };
  }

  return {getRecords};
};
