import React from 'react';
import {Pressable, View, TextInput} from 'react-native';
// import {connectSearchBox} from 'react-instantsearch-native';
import {useIntl} from 'react-intl';
import {useTheme} from 'react-native-paper';
import {useDimension} from '../../../../utils/useDimension';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const SearchBar = ({
  setValue,
  value,
  onBackPress,
}: {
  value: string;
  setValue: (input: string) => void;
  onBackPress: () => void;
}) => {
  const theme = useTheme();
  const intl = useIntl();
  const [getWidth] = useDimension();

  return (
    <View style={{paddingHorizontal: 5, paddingVertical: 10}}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Pressable
          style={{
            height: 52,
            width: 50,
            backgroundColor: '#E7E7E7',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={onBackPress}>
          <MaterialCommunityIcons
            name="arrow-left"
            size={24}
            color={theme.colors.primary}
          />
        </Pressable>

        <TextInput
          style={{
            borderLeftWidth: 1,
            borderRightWidth: 1,
            borderLeftColor: '#fff',
            borderRightColor: '#fff',
            paddingHorizontal: 10,
            height: 52,
            width: getWidth - 110,
            borderRadius: 0,
            backgroundColor: '#E7E7E7',
          }}
          placeholder={intl.formatMessage({defaultMessage: 'Search'})}
          onChangeText={e => setValue(e)}
          value={value}
        />
        <Pressable
          style={{
            height: 52,
            width: 50,
            backgroundColor: '#E7E7E7',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => setValue('')}>
          <MaterialCommunityIcons
            name="close"
            size={24}
            color={theme.colors.primary}
          />
        </Pressable>
      </View>
    </View>
  );
};

export default SearchBar;
