import {IconButton, Text, useTheme} from 'react-native-paper';
import {View} from 'react-native';
import * as React from 'react';
import {useState} from 'react';
import {useRecoilState} from 'recoil';
import {CartItemData, useCart} from '../../../../../../hooks/useCart';

const CartQuantityChange = ({
  sku,
  trashIcon,
  onChangeQuantity,
  initialValue = 0,
}: {
  sku: string;
  id: number;
  initialValue?: number;
  trashIcon?: boolean;
  onChangeQuantity: (input: number) => void;
}) => {
  const theme = useTheme();
  const [cartItem] = useRecoilState(CartItemData);
  const {updateProductToCart, addProductToCart} = useCart();
  const cartProductData: any = React.useMemo(() => {
    if (cartItem.some(el => el.product.sku === sku)) {
      let cartObj = cartItem.find(el => el.product.sku === sku);
      return cartObj;
    }
  }, [cartItem, sku]);

  return (
    <View
      style={{
        width: 110,
        flexDirection: 'row',
        backgroundColor: '#F1F9F4',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 35,
        marginVertical: 10,
        borderRadius: 50,
      }}>
      <IconButton
        style={{
          borderRadius: 50,
          height: 35,
          marginLeft: 0,
          width: 35,
          backgroundColor: '#D4EDDD',
        }}
        icon={initialValue === 1 && trashIcon ? 'delete' : 'minus'}
        iconColor={theme.colors.greenTextColor}
        size={20}
        onPress={() => {
          updateProductToCart(cartProductData.uid, cartProductData.quantity);
        }}
      />
      <Text
        style={{
          marginHorizontal: 5,
          fontSize: 14,
          color: theme.colors.titleTextColor,
          fontWeight: '700',

          textAlign: 'center',
        }}>
        {initialValue}
      </Text>
      <IconButton
        style={{
          borderRadius: 50,
          height: 35,
          marginRight: 0,
          backgroundColor: '#D4EDDD',
          width: 35,
        }}
        icon="plus"
        iconColor={theme.colors.greenTextColor}
        size={20}
        onPress={() => {
          addProductToCart(sku, 1);
        }}
      />
    </View>
  );
};

export default CartQuantityChange;
