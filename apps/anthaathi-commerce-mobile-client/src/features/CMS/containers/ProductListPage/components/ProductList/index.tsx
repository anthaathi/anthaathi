/* eslint-disable react/no-unstable-nested-components */
import * as React from 'react';
import {
  ActivityIndicator,
  Avatar,
  Button,
  Text,
  Title,
  useTheme,
} from 'react-native-paper';
import {
  Image,
  Pressable,
  TouchableOpacity,
  View,
  VirtualizedList,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {useResponsiveValue} from '../../../../utils/useResponsiveValue';
import {useIntl} from 'react-intl';
import {ProductListPageComponentType} from '../../../../types/common';
import {useRecoilState} from 'recoil';
import {CartItemData, useCart} from '../../../../../../hooks/useCart';

export interface ProductProps {
  id: number;
  name: string;
  description?: string;
  price: number;
  currency: string;
  image: string;
  weight_unit: string;
  packaging: string;
  key: string;
  notes: string;
}

export interface ProductListProps {
  products: any[];
  pageLoad: boolean;
  handlePress?: (item: any) => void;
  handleInfoPress?: (item: any) => void;
  loading?: boolean;
  loadMore?: boolean;
  loadMorePress?: () => void;
}

export default function ProductList({
  products,
  handlePress,
  handleInfoPress,
  loading,
  pageLoad,
  loadMore,
  loadMorePress,
}: ProductListProps) {
  const itemHeight = useResponsiveValue([160, 200, 220, 280]);
  const theme = useTheme();
  const productSplitted: any[][] = React.useMemo(() => {
    return products.reduce<any[][]>(
      (previousValue, currentValue, currentIndex) => {
        if (currentIndex % 2) {
          const newArray: any[][] = [...previousValue];
          newArray[previousValue.length - 1].push(currentValue);
          return newArray;
        } else {
          return [...previousValue, [currentValue]];
        }
      },
      [],
    );
  }, [products]);

  if (pageLoad) {
    return (
      <View>
        <ActivityIndicator
          style={{
            marginVertical: 10,
            marginHorizontal: 5,
          }}
          size="large"
          color={theme.colors.primary}
        />
      </View>
    );
  }

  return (
    <View style={{marginTop: 10}} testID="productList">
      <VirtualizedList<any[]>
        data={productSplitted}
        contentContainerStyle={{paddingBottom: itemHeight * 1.4}}
        testID="productListData"
        renderItem={({item}) => (
          <ItemRendererColumn
            item={item}
            itemHeight={itemHeight}
            handlePress={handlePress || (() => {})}
            handleInfoPress={handleInfoPress || (() => {})}
          />
        )}
        getItemCount={() => productSplitted.length}
        keyExtractor={(item, index) => item?.[0]?.id || index + ''}
        getItem={(res, index) => res[index]}
        ListFooterComponent={() => (
          <>
            {loadMore && (
              <Button
                loading={loading}
                mode="text"
                style={{
                  marginVertical: 10,
                  marginHorizontal: '4%',
                  borderRadius: 4,
                  width: '92%',
                }}
                labelStyle={{
                  paddingVertical: 5,
                }}
                onPress={loadMorePress}>
                Load More...
              </Button>
            )}
          </>
        )}
      />
    </View>
  );
}

function ItemRendererColumn({
  item,
  itemHeight,
  handlePress,
  handleInfoPress,
}: {
  item: any[];
  itemHeight: number;
  handlePress: (item: any) => void;
  handleInfoPress: (item: any) => void;
}) {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 15,
        marginHorizontal: 10,
      }}>
      {item.map(element => (
        <ItemRenderer
          key={element.id}
          item={element}
          itemHeight={itemHeight}
          handlePress={() => handlePress(element)}
          handleInfoPress={handleInfoPress}
        />
      ))}
    </View>
  );
}

function ItemRenderer({
  item,
  itemHeight,
  handlePress,
  handleInfoPress,
}: {
  item: any;
  itemHeight: number;
  handlePress: () => void;
  handleInfoPress: (item: any) => void;
}) {
  const [cartItem] = useRecoilState(CartItemData);
  const {updateProductToCart, addProductToCart} = useCart();
  const cartProductData: any = React.useMemo(() => {
    if (cartItem.some(el => el.product.sku === item.sku)) {
      let cartObj = cartItem.find(el => el.product.sku === item.sku);
      return cartObj;
    }
  }, [cartItem, item.sku]);

  const theme = useTheme();
  const intl = useIntl();

  return (
    <View
      style={{
        marginVertical: 5,
        width: '48%',
        borderColor:
          cartProductData && cartProductData.product.sku === item.sku
            ? theme.colors.greenTextColor
            : '#e7e7e7',
        backgroundColor: '#f0f0f0',
        borderWidth: 1,
        borderRadius: 12,
      }}
      key={item.id}>
      <View>
        <Pressable
          onPress={() => {
            addProductToCart(item.sku, 1);
          }}>
          <View style={{height: itemHeight, width: '100%'}}>
            <Image
              style={{
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
                height: itemHeight,
                width: '99.9%',
                paddingHorizontal: '0.1%',
                backgroundColor: '#fff',
              }}
              source={{
                uri: item.image?.url,
              }}
            />
            {cartProductData && cartProductData.product.sku === item.sku && (
              <>
                <Pressable
                  onPress={() => {
                    updateProductToCart(
                      cartProductData.uid,
                      cartProductData.quantity,
                    );
                  }}
                  style={{
                    position: 'absolute',
                    left: 3,
                    top: 3,
                    paddingTop: 5,
                    paddingLeft: 5,
                    paddingRight: 10,
                    paddingBottom: 10,
                  }}>
                  <Avatar.Icon
                    icon="minus"
                    size={32}
                    style={{
                      backgroundColor: 'red',
                    }}
                  />
                </Pressable>
                <Pressable
                  onPress={() => {
                    addProductToCart(item.sku, 1);
                  }}
                  style={{
                    position: 'absolute',
                    right: 3,
                    top: 3,
                    paddingTop: 5,
                    paddingLeft: 10,
                    paddingRight: 5,
                    paddingBottom: 10,
                  }}>
                  <Avatar.Text
                    label={cartProductData.quantity.toString()}
                    size={32}
                  />
                </Pressable>
              </>
            )}
          </View>
          <View
            style={{
              alignItems: 'flex-start',
              backgroundColor: '#f0f0f0',
              paddingVertical: 5,
              paddingHorizontal: 8,
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <Title
                style={{
                  fontSize: 12,
                  marginRight: 5,
                  fontWeight: 'bold',
                  flexShrink: 1,
                }}>
                {item?.title ?? item?.name ?? ''}
              </Title>
              <TouchableOpacity
                onPress={() => {
                  handleInfoPress(item);
                }}>
                <Entypo
                  name="info-with-circle"
                  color={theme.colors.titleTextColor}
                  size={18}
                  style={{paddingVertical: 5, paddingLeft: 10}}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                color: theme.colors.greyTextColor,
                fontSize: 12,
                fontWeight: '400',
              }}>
              Dorne
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 5,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: theme.colors.greenTextColor,
                  fontSize: 14,
                  fontWeight: '400',
                }}>
                {intl.formatNumber(
                  +item?.price?.minimum_price.final_price.value?.toString() ??
                    '',
                  {
                    style: 'currency',
                    currency:
                      item?.price?.minimum_price?.final_price?.currency ??
                      'AED',
                  },
                )}
              </Text>
              <Text
                style={{
                  color: theme.colors.greyTextColor,
                  fontSize: 12,
                  fontWeight: '400',
                }}>
                {' / Piece'}
              </Text>
            </View>

            {/* <Text
              style={{
                color: theme.colors.greyTextColor,
                fontSize: 12,
                fontWeight: '400',
              }}>
              {item.notes}
            </Text> */}
          </View>
        </Pressable>
      </View>
    </View>
  );
}

export const ProductListCMSInput = {
  _component: ProductListPageComponentType.ProductList,
  component: ProductList,
};
