import {View, Text, Image, Pressable, Dimensions} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {useResponsiveValue} from '../../../../utils/useResponsiveValue';
import {ScrollView} from 'react-native-gesture-handler';

export interface ImageCarouselProps {
  items: ItemProps[];
  title?: string;
}

export interface ItemProps {
  imageSrc: string;
  onPress?: () => void;
}

const ImageCarousel = ({items, title}: ImageCarouselProps) => {
  const ScrollViewRef = useRef(null);
  const {width} = Dimensions.get('window');
  const [offsetValues, setOffsetValues] = useState({xOffset: 0, yOffset: 0});
  const intervaId = useRef<ReturnType<typeof setInterval>>();
  let index = 0,
    copyXOffset = 0;

  useEffect(() => {
    ScrollViewRef.current?.scrollTo({
      x: offsetValues.xOffset,
      animated: true,
    });
  }, [offsetValues.xOffset]);

  const intervalStart = (x?: number) => {
    let xOffset = x || 0;

    intervaId.current = setInterval(() => {
      setOffsetValues({xOffset: xOffset, yOffset: 0});

      if (index >= items.length) {
        xOffset = 0;
        index = 0;
      } else {
        xOffset += width - 10;
        index++;
      }
    }, 3000);
  };
  useEffect(() => {
    intervalStart();
    return () => {
      clearInterval(intervaId.current);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View testID="imageCorousel">
      {title && (
        <Text
          style={{
            marginBottom: 9,
            fontSize: 18,
            fontWeight: '600',
            marginHorizontal: 5,
            marginVertical: 6,
          }}>
          {title}
        </Text>
      )}

      <View
        style={{
          flexDirection: 'row',
        }}>
        <ScrollView
          ref={ScrollViewRef}
          onTouchStart={() => {
            clearInterval(intervaId.current);
          }}
          onMomentumScrollEnd={event => {
            copyXOffset = event.nativeEvent.contentOffset.x;
            index = event.nativeEvent.contentOffset.x / (width - 10);
          }}
          onTouchEnd={() => {
            intervalStart(copyXOffset);
          }}
          centerContent
          pagingEnabled
          horizontal={true}
          decelerationRate={0}
          snapToAlignment={'center'}>
          {items.map((item, itemIndex) => (
            <ItemRenderer
              key={itemIndex}
              imageSrc={item.imageSrc}
              onPress={item.onPress || (() => {})}
            />
          ))}
        </ScrollView>
      </View>
    </View>
  );
};

const ItemRenderer = (item: ItemProps) => {
  const itemHeight = useResponsiveValue([110, 250, 290, 330]);
  return (
    <Pressable
      onPress={item.onPress}
      style={{
        marginHorizontal: 5,
        marginVertical: 10,
      }}>
      <Image
        source={{uri: item.imageSrc}}
        style={{
          height: itemHeight,
          resizeMode: 'cover',
          flex: 1,
          width: Dimensions.get('screen').width - 20,
          borderRadius: 5,
        }}
      />
    </Pressable>
  );
};

export default ImageCarousel;
