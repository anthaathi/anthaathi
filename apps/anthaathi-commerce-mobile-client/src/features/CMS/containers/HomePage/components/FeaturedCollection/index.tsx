import * as React from 'react';
import {Avatar, Text, Title, useTheme} from 'react-native-paper';
import {
  Image,
  Pressable,
  TouchableOpacity,
  View,
  VirtualizedList,
} from 'react-native';
import {useResponsiveValue} from '../../../../utils/useResponsiveValue';
import {useIntl} from 'react-intl';
import {HomePageComponentType} from '../../../../types/common';
import {CartItemData, useCart} from '../../../../../../hooks/useCart';
import Entypo from 'react-native-vector-icons/Entypo';
import {useRecoilState} from 'recoil';

export interface ProductProps {
  id: number;
  name: string;
  name_ar: string;
  description?: string;
  price: number;
  currency: string;
  image: string;
  weight_unit: string;
  packaging: string;
  key: string;
  notes: string;
}

export interface FeaturedCollectionProps {
  title: string;
  title_ar?: string;
  products: any[];
  handlePress?: () => void; // view all product link
  onProductPress: (item: any) => void;
}

export default function FeaturedCollection({
  title,
  title_ar: titleAr,
  products,
  handlePress,
  onProductPress,
}: FeaturedCollectionProps) {
  const intl = useIntl();
  const itemHeight = useResponsiveValue([140, 250, 290, 330]);
  const itemWidth = useResponsiveValue([150, 240, 280, 320]);

  return (
    <View testID="featuredCollection" style={{backgroundColor: '#fff'}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal: 10,
          marginVertical: 10,
        }}>
        <Text variant="titleLarge" style={{fontSize: 16}}>
          {intl.locale.startsWith('ar') ? titleAr ?? title : title}
        </Text>

        <Pressable onPress={handlePress} testID="onPressCollection">
          <Text
            variant="titleMedium"
            style={{
              textDecorationLine: 'underline',
              fontSize: 14,
            }}>
            {intl.formatMessage({defaultMessage: 'View All'})}
          </Text>
        </Pressable>
      </View>

      <View style={{marginHorizontal: 5}}>
        <VirtualizedList<any[]>
          testID="productList"
          data={products}
          initialNumToRender={4}
          horizontal={true}
          renderItem={({item}) => (
            <ItemRenderer
              item={item}
              key={item?.id}
              itemHeight={itemHeight}
              itemWidth={itemWidth}
              onProductPress={() => onProductPress(item)}
            />
          )}
          getItemCount={() => products.length}
          keyExtractor={item => item?.id}
          getItem={(res, index) => res[index]}
        />
      </View>
    </View>
  );
}

function ItemRenderer({
  item,
  itemHeight,
  itemWidth,
  onProductPress,
}: {
  item: any;
  itemHeight: number;
  itemWidth: number;
  onProductPress: () => void;
}) {
  const [cartItem] = useRecoilState(CartItemData);
  const {updateProductToCart, addProductToCart} = useCart();
  const cartProductData: any = React.useMemo(() => {
    if (cartItem.some(el => el.product.sku === item.sku)) {
      let cartObj = cartItem.find(el => el.product.sku === item.sku);
      return cartObj;
    }
  }, [cartItem, item.sku]);

  const theme = useTheme();
  const intl = useIntl();
  return (
    <View
      style={{
        margin: 5,
        // width: '48%',
        borderColor:
          cartProductData && cartProductData.product.sku === item.sku
            ? theme.colors.greenTextColor
            : '#e7e7e7',
        // borderColor: '#e7e7e7',
        backgroundColor: '#f0f0f0',
        borderWidth: 1,
        borderRadius: 12,
      }}
      key={item.key}>
      <View>
        <Pressable
          onPress={() => {
            addProductToCart(item.sku, 1);
          }}>
          <View style={{height: itemHeight, width: '100%'}}>
            <Image
              style={{
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
                height: itemHeight,
                width: '99.9%',
                paddingHorizontal: '0.1%',
                backgroundColor: '#fff',
              }}
              source={{
                uri: item.image?.url,
              }}
            />
            {cartProductData && cartProductData.product.sku === item.sku && (
              <>
                <Pressable
                  onPress={() => {
                    updateProductToCart(
                      cartProductData.uid,
                      cartProductData.quantity,
                    );
                  }}
                  style={{
                    position: 'absolute',
                    left: 3,
                    top: 3,
                    paddingTop: 5,
                    paddingLeft: 5,
                    paddingRight: 10,
                    paddingBottom: 10,
                  }}>
                  <Avatar.Icon
                    icon="minus"
                    size={32}
                    style={{
                      backgroundColor: 'red',
                    }}
                  />
                </Pressable>
                <Pressable
                  onPress={() => {
                    addProductToCart(item.sku, 1);
                  }}
                  style={{
                    position: 'absolute',
                    right: 3,
                    top: 3,
                    paddingTop: 5,
                    paddingLeft: 10,
                    paddingRight: 5,
                    paddingBottom: 10,
                  }}>
                  <Avatar.Text
                    label={cartProductData.quantity.toString()}
                    size={32}
                  />
                </Pressable>
              </>
            )}
          </View>
          <View
            style={{
              alignItems: 'flex-start',
              backgroundColor: '#f0f0f0',
              paddingVertical: 5,
              paddingHorizontal: 8,
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                width: itemWidth * 0.9,
              }}>
              <Title
                style={{
                  fontSize: 12,
                  marginRight: 5,
                  fontWeight: 'bold',
                  flexShrink: 1,
                }}>
                {item.name}
              </Title>
              <TouchableOpacity onPress={onProductPress}>
                <Entypo
                  name="info-with-circle"
                  color={theme.colors.titleTextColor}
                  size={18}
                  style={{paddingVertical: 5, paddingLeft: 10}}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                color: theme.colors.greyTextColor,
                fontSize: 12,
                fontWeight: '400',
              }}>
              Dorne
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 5,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: theme.colors.greenTextColor,
                  fontSize: 14,
                  fontWeight: '400',
                }}>
                {intl.formatNumber(
                  +item?.price?.minimum_price.final_price.value?.toString() ??
                    '',
                  {
                    style: 'currency',
                    currency:
                      item?.price?.minimum_price?.final_price?.currency ??
                      'AED',
                  },
                )}
              </Text>
              <Text
                style={{
                  color: theme.colors.greyTextColor,
                  fontSize: 12,
                  fontWeight: '400',
                }}>
                {' / ' + item.packaging}
              </Text>
            </View>
          </View>
        </Pressable>
      </View>
    </View>
  );
}

export const FeaturedCollectionCMSInput = {
  _component: HomePageComponentType.FeaturedCollection,
  component: FeaturedCollection,
};
