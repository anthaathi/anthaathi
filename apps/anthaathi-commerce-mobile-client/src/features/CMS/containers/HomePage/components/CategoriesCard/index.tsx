import {Image, Pressable, View} from 'react-native';
import React from 'react';
import {Text, TouchableRipple} from 'react-native-paper';
import {useResponsiveValue} from '../../../../utils/useResponsiveValue';
import {HomePageComponentType} from '../../../../types/common';
import {useIntl} from 'react-intl';

export interface CategoryProps {
  key: string;
  id: number;
  image: string;
}

export interface CategoriesCardProps {
  key: string;
  title?: string;
  items: CategoryProps[];
  onPress: () => void;
}

const CategoriesCard = (props: CategoriesCardProps) => {
  const itemHeight = useResponsiveValue([140, 120, 180, 220]);
  const itemWidth = useResponsiveValue(['100%', '49%', '32%', '33%']);

  return (
    <View style={{alignItems: 'center'}} testID="categoryCard">
      <View style={{alignItems: 'flex-start', width: '100%'}}>
        <Text variant="titleLarge" style={{marginBottom: 5, fontSize: 20}}>
          {props.title}
        </Text>
      </View>
      <View
        style={{
          flexDirection: itemWidth === '100%' ? 'column' : 'row',
          width: '100%',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        {props.items.map(item => {
          return (
            <CategoryItemRenderer
              itemHeight={itemHeight}
              itemWidth={itemWidth}
              key={item.key}
              item={item}
              onPress={props.onPress || (() => {})}
            />
          );
        })}
      </View>
    </View>
  );
};

function CategoryItemRenderer({
  item,
  itemWidth,
  itemHeight,
  onPress,
}: {
  item: CategoryProps;
  itemWidth: any;
  itemHeight: any;
  onPress: () => void;
}) {
  return (
    <Pressable
      onPress={onPress}
      style={{
        display: 'flex',
        flexDirection: 'row',
        height: itemHeight,
        width: itemWidth,
        marginVertical: 10,
        alignItems: 'center',
      }}>
      <Image
        testID="categoryImage"
        source={{
          uri: item.image,
        }}
        style={{
          height: '100%',
          width: '100%',
          borderRadius: 5,
        }}
      />
    </Pressable>
  );
}

export default CategoriesCard;

export const CategoriesCardCMSInput = {
  _component: HomePageComponentType.CategoriesCard,
  component: CategoriesCard,
};
