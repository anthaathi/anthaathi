import {fireEvent, render} from '@testing-library/react-native';
import {IntlProvider} from 'react-intl';
import {ThemeProvider} from 'react-native-paper';
import CouponCodeInput from './index';
import locale from '../../../../../../compiled-locales/en-US.json';
import React from 'react';

describe('CouponCodeInput', () => {
  it('should render the item', function () {
    const temp = render(
      <ThemeProvider>
        <IntlProvider locale="en-US" messages={locale}>
          <CouponCodeInput />,
        </IntlProvider>
        ,
      </ThemeProvider>,
    );

    expect(temp).toMatchSnapshot();
    expect(temp.queryByTestId('couponCodeInput')).toBeTruthy();
  });
  it('should call when we call handlepress', function () {
    const onPress = jest.fn();
    const temp = render(
      <ThemeProvider>
        <IntlProvider locale="en-US" messages={locale}>
          <CouponCodeInput handleOnPress={onPress} />,
        </IntlProvider>
      </ThemeProvider>,
    );

    fireEvent.press(temp.queryByTestId('applyPressed')!);
    expect(onPress).toBeCalledTimes(1);
  });
});
