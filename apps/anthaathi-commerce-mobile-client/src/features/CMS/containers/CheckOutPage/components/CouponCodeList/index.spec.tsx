import React from 'react';
import {render} from '@testing-library/react-native';
import {IntlProvider} from 'react-intl';
import {ThemeProvider} from 'react-native-paper';
import CouponCodeList from './index';
import locale from '../../../../../../compiled-locales/en-US.json';

describe('CouponCodeList', () => {
  it('Should render the item', function () {
    const temp = render(
      <ThemeProvider>
        <IntlProvider locale="en-US" messages={locale}>
          <CouponCodeList
            title="Available Coupons"
            items={[
              {
                imageUrl:
                  'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
                title: 'Get up to AED 50 cashback using ADCB card',
                subtitle:
                  'Valid on total value of items worth AED 500 or more.',
                couponCode: 'ADCBOFFER',
                id: '1',
              },
              {
                imageUrl:
                  'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
                title: 'Get up to AED 50 cashback using ADCB card',
                subtitle:
                  'Valid on total value of items worth AED 500 or more.',
                couponCode: 'ADCBOFFER',
                id: '2',
              },
              {
                imageUrl:
                  'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
                title: 'Get up to AED 50 cashback using ADCB card',
                subtitle:
                  'Valid on total value of items worth AED 500 or more.',
                couponCode: 'ADCBOFFER',
                id: '3',
              },
            ]}
          />
        </IntlProvider>
      </ThemeProvider>,
    );
    expect(temp).toMatchSnapshot();
    expect(temp.queryByTestId('couponCodeList')).toBeTruthy();
  });
});
