import {
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {Button, Text, TextInput, useTheme} from 'react-native-paper';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../../types/Route';
import {useIntl} from 'react-intl';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {useAuth} from '../../../hooks/useAuth';
import Toast from 'react-native-toast-message';

const SignInPage = (
  props: NativeStackScreenProps<RootStackParamList, 'SignIn'>,
) => {
  const theme = useTheme();
  const intl = useIntl();
  const [buttonLoading, setButtonLoading] = useState<boolean>(false);
  const {signInUser} = useAuth();
  const [passwordShow, setPasswordShow] = React.useState(true);
  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .required('Email or username is required.')
      .email('email is invalid'),
    password: Yup.string().required('Password is required.'),
  });

  const formik = useFormik({
    initialValues: {
      email: 'c@v.com',
      password: 'Kedar@09',
    },
    validationSchema,
    onSubmit: async values => {
      setButtonLoading(true);
      const data = await signInUser(values.email, values.password);
      if (data && data.generateCustomerToken) {
        Toast.show({
          text1: 'Loged in Successfully!',
          type: 'success',
          autoHide: true,
          visibilityTime: 5000,
          position: 'bottom',
        });
        props.navigation.goBack();
      } else {
        Toast.show({
          text1: 'Something went wrong!',
          text2: data,
          type: 'error',
          autoHide: true,
          visibilityTime: 5000,
          position: 'bottom',
        });
      }
      setButtonLoading(false);
    },
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
      style={{flex: 1}}>
      <View
        style={{
          backgroundColor: '#fff',
          flex: 1,
        }}>
        <ScrollView style={{paddingHorizontal: 25}}>
          <View
            style={{
              paddingTop: '40%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                width: 138,
                height: 128,
                zIndex: 999,
              }}
              source={{
                uri: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x.png?v=1653569545',
              }}
              resizeMode="contain"
            />
          </View>
          <View style={{marginVertical: 15}}>
            <TextInput
              mode="flat"
              label={intl.formatMessage({
                defaultMessage: 'Username or email address',
              })}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              onChangeText={formik.handleChange('email')}
              value={formik.values.email}
              onBlur={formik.handleBlur('email')}
            />
            {formik.touched.email && formik.errors.email && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.email}
              </Text>
            )}
            <TextInput
              mode="flat"
              label={intl.formatMessage({defaultMessage: 'Password'})}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              secureTextEntry={passwordShow}
              right={
                <TextInput.Icon
                  icon={passwordShow ? 'eye' : 'eye-off'}
                  onPress={() => {
                    setPasswordShow(!passwordShow);
                  }}
                />
              }
              onChangeText={formik.handleChange('password')}
              value={formik.values.password}
              onBlur={formik.handleBlur('password')}
            />
            {formik.touched.password && formik.errors.password && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.password}
              </Text>
            )}
          </View>

          <View style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('ResetPassword');
              }}>
              <Text
                style={{
                  textDecorationLine: 'underline',
                  color: theme.colors.primary,
                  fontWeight: '600',
                  fontSize: 13,
                }}>
                {intl.formatMessage({defaultMessage: 'Forgot Password?'})}
              </Text>
            </TouchableOpacity>
          </View>
          <Button
            mode="contained"
            loading={buttonLoading}
            uppercase={false}
            style={{
              marginVertical: 15,
              marginHorizontal: 30,
              borderRadius: 32,
            }}
            labelStyle={{paddingVertical: 5}}
            onPress={formik.handleSubmit}>
            {intl.formatMessage({defaultMessage: 'Sign In'})}
          </Button>

          <View
            style={{
              paddingBottom: '10%',
              marginTop: 50,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontWeight: '500', fontSize: 14}}>
              {intl.formatMessage({defaultMessage: "Don't have an account?"})}
            </Text>
            <TouchableOpacity
              style={{paddingHorizontal: 10, paddingVertical: 10}}
              onPress={() => {
                props.navigation.navigate('SignUp');
              }}>
              <Text
                style={{
                  color: theme.colors.primary,
                  fontWeight: '700',
                  fontSize: 13,
                }}>
                {intl.formatMessage({defaultMessage: 'Sign Up'})}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default SignInPage;
