import {View} from 'react-native';
import React from 'react';
import {ProfilePageComponentType} from '../../../features/CMS/types/common';
import CMSRenderer from '../../../features/CMS';
import dataJson from '../../../config/data.json';
import {RootStackParamList} from '../../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import Header from "../../../features/CMS/containers/Header";

const AddressInfoPage = (
    props: NativeStackScreenProps<RootStackParamList, 'AddressInfo'>,
) => {
    return (
        <View>
            <Header
                leftIcon={'arrow-left'}
                leftOnPress={() => props.navigation.goBack()}
                languageIcon={true}
                cartIcon={true}
                cartOnPress={() => {
                    props.navigation.navigate('CartPage');
                }}
                mailIcon={false}
                searchIcon={true}
                logoImage={'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x.png?v=1653569545'}
            />
            <CMSRenderer
                components={[
                    {
                        _component: ProfilePageComponentType.DeliveryAddresses,
                        key: '3232',
                        title: 'Address information',
                        userAddress: dataJson.core.profilePage.addressInformation,
                        handlePress: () => {
                            props.navigation.navigate('AddEditAddress');
                        },
                    },
                ]}
            />
        </View>
    );
};

export default AddressInfoPage;
