import {View} from 'react-native';
import React from 'react';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';

import CMSRenderer from '../../features/CMS';
import {CoreComponentType} from '../../features/CMS/types/common';
import {Divider} from 'react-native-paper';
import CouponCodeInput from '../../features/CMS/containers/CheckOutPage/components/CouponCodeInputBox';
import CouponCodeList from '../../features/CMS/containers/CheckOutPage/components/CouponCodeList';

const ApplyCouponPage: React.FC<
  NativeStackScreenProps<RootStackParamList, 'ApplyCouponPage'>
> = props => {
  return (
    <View style={{flex: 1}}>
      <CMSRenderer
        components={[
          {
            _component: CoreComponentType.Header,
            key: '142',
            title: 'Coupons',
            leftIcon: 'close',
            leftOnPress: () => {
              props.navigation.goBack();
            },
          },
        ]}
      />
      <CouponCodeInput
        handleOnPress={() => {
          console.log('pressed');
        }}
      />
      <Divider style={{marginHorizontal: 10}} />
      <CouponCodeList
        title="Available Coupons"
        handlePress={(id: string) => {
          console.log('pressed', id);
        }}
        handleViewDetailsPress={(id: string) => {
          console.log('view pressed', id);
        }}
        items={[
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '1',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '2',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '3',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '4',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '5',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '6',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '7',
          },
          {
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Abu_Dhabi_Commercial_Bank_logo.svg/220px-Abu_Dhabi_Commercial_Bank_logo.svg.png',
            title: 'Get up to AED 50 cashback using ADCB card',
            subtitle: 'Valid on total value of items worth AED 500 or more.',
            couponCode: 'ADCBOFFER',
            id: '8',
          },
        ]}
      />
    </View>
  );
};

export default ApplyCouponPage;
