import {Linking, ScrollView, View} from 'react-native';
import React, {useEffect, useMemo, useState} from 'react';

import CMSRenderer from '../../features/CMS';
import {
  CoreComponentType,
  HomePageComponentType,
} from '../../features/CMS/types/common';

import dataJson from '../../config/data.json';
import categoryJson from '../../config/category.json';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../types/Route';
import {ProductProps} from '../../features/CMS/containers/HomePage/components/FeaturedCollection';
import ImageCarousel from '../../features/CMS/containers/HomePage/components/ImageCarousel';
import {getCategoryQuery} from '../../navigators/ProductTopTab';
import {useQuery, gql} from '@apollo/client';

const getProductList = gql`
  query getCategoryByHandle($handle: String!, $currentPage: Int) {
    categories(filters: {url_key: {eq: $handle}}) {
      items {
        ... on CategoryTree {
          meta_title
          products(currentPage: $currentPage, pageSize: 6) {
            items {
              id: uid
              name
              sku
              title: meta_title
              description: meta_description
              url_key
              price: price_range {
                minimum_price {
                  discount {
                    amount_off
                    percent_off
                  }
                  regular_price {
                    currency
                    value
                  }
                  final_price {
                    value
                    currency
                  }
                }
              }
              image {
                url
              }
            }
            page_info {
              page_size
              current_page
              total_pages
            }
            total_count
          }
        }
      }
    }
  }
`;

const HomePage = (
  props: NativeStackScreenProps<RootStackParamList, 'HomePage'>,
) => {
  const {loading, data} = useQuery(getCategoryQuery);
  const {loading: listLoading, data: listData} = useQuery(getProductList, {
    variables: {handle: 'fruits', currentPage: 1},
  });

  const productList: any[] = useMemo(() => {
    return listLoading ? [] : listData?.categories?.items[0].products.items;
  }, [listData, listLoading]);

  const categoryList: any[] = useMemo(() => {
    return loading ? [] : data.categories?.items[0].children;
  }, [data, loading]);

  useEffect(() => {
    if (!listLoading) {
      // setProductList(listData?.categories?.items[0].products.items);
    }
  }, [listLoading]);

  return (
    <View>
      <ScrollView
        contentContainerStyle={{
          paddingHorizontal: 5,
          paddingBottom: 80,
          backgroundColor: '#FFF',
        }}>
        <ImageCarousel
          items={[
            {
              imageSrc:
                'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1080x.png?v=1666873198',
              onPress: () => {
                console.log('pressed first');
              },
            },
            {
              imageSrc:
                'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_900x.png?v=1666950289',
              onPress: () => {
                console.log('pressed second');
              },
            },
            {
              imageSrc:
                'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_900x.png?v=1666947921',
              onPress: () => {
                console.log('pressed third');
              },
            },
            {
              imageSrc:
                'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1080x.png?v=1666873198',
              onPress: () => {
                console.log('pressed fourth');
              },
            },
          ]}
        />
        <CMSRenderer
          components={[
            {
              _component: HomePageComponentType.DeliveringSelection,
              key: '124',
              userAddresses: [
                {
                  key: 1,
                  title: 'Apartment',
                  subtitle: '14b street, AI Quoz Industrial Area 4',
                },
                {
                  key: 2,
                  title: 'Apartment',
                  subtitle: '1A street, Discovery Gardens',
                },
                {
                  key: 3,
                  title: 'Building No. 17',
                  subtitle: '14b street, AI Quoz Industrial Area 4',
                },
                {
                  key: 4,
                  title: 'Apartment',
                  subtitle: '1A street, Discovery Gardens',
                },
              ],
            },

            {
              _component: HomePageComponentType.HeroCategories,
              key: '127',
              title: categoryJson.heroCategories.title,
              items: categoryList,
              onPress: (value: string) => {
                props.navigation.navigate('ProductListPage1', {
                  categoryName: value,
                });
              },
            },
            {
              _component: HomePageComponentType.FeaturedCollection,
              key: '1251',
              title: 'Special Offers',
              products: productList,
              handlePress: () => {
                props.navigation.navigate('ProductListPage1');
              },
              onProductPress: (item: any) => {
                props.navigation.navigate('ProductPage', {
                  handle: item.url_key,
                });
              },
            },

            {
              _component: HomePageComponentType.CategoriesCard,
              key: '2321',
              title: 'Categories',
              items: [
                {
                  id: 1,
                  key: 'fruits',
                  image:
                    'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_900x.png?v=1666947921',
                },
                {
                  id: 2,
                  key: 'fruitBakset',
                  image:
                    'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_900x.png?v=1666947921',
                },
                {
                  id: 3,
                  key: 'vegetables',
                  image:
                    'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_900x.png?v=1666947921',
                },
              ],
            },

            {
              _component: HomePageComponentType.HeroSlide,
              key: '128',
              backgroundImageSrc:
                dataJson.core.homePage.heroSlide.backgroundImage,
              title: dataJson.core.homePage.heroSlide.title,
              subTitle: dataJson.core.homePage.heroSlide.subTitle,
              buttonTitle: dataJson.core.homePage.heroSlide.buttonTitle,
              handlePress: () => {
                props.navigation.navigate('ProductListPage1', {
                  categoryName: 'juices',
                });
              },
            },

            {
              _component: HomePageComponentType.PromotionalGrid,
              key: '129',
              items: [
                {
                  key: dataJson.core.homePage.promotionalGrid.items[1].key,
                  heading:
                    dataJson.core.homePage.promotionalGrid.items[1].heading,
                  button1Text:
                    dataJson.core.homePage.promotionalGrid.items[1].button1Text,
                  onPress1: () => {
                    props.navigation.navigate('ProductListPage1', {
                      categoryName: 'organic',
                    });
                  },
                  height: [180, 240, 260, 270],
                  image: dataJson.core.homePage.promotionalGrid.items[1].image,
                  width: ['50%', '50%', '100%', '100%'],
                },
                {
                  key: dataJson.core.homePage.promotionalGrid.items[2].key,
                  heading:
                    dataJson.core.homePage.promotionalGrid.items[2].heading,
                  button1Text:
                    dataJson.core.homePage.promotionalGrid.items[2].button1Text,
                  onPress1: () => {
                    props.navigation.navigate('ProductListPage1', {
                      categoryName: 'bulkbuy',
                    });
                  },
                  height: [180, 240, 260, 270],
                  image: dataJson.core.homePage.promotionalGrid.items[2].image,
                  width: ['50%', '50%', '100%', '100%'],
                },
              ],
            },
            {
              _component: HomePageComponentType.SplitOfferCard,
              key: '131',
              title: 'Get Exclusive Offers',
              subtitle:
                'Get exclusive offers & more by signing up for our promotional email',
              image: dataJson.core.homePage.splitOfferCard.image,
              buttonTitle: 'View Offers',
              onPress: () => {
                props.navigation.navigate('ProductListPage1');
              },
            },
            // {
            //   _component: HomePageComponentType.ChatFloatingButton,
            //   key: '1312',
            // },
          ]}
        />
      </ScrollView>
      <CMSRenderer
        components={[
          {
            _component: CoreComponentType.CMSFABButton,
            key: '35435345',
            icon: 'whatsapp',
            buttonRadius: 50,
            buttonBackgroundColor: '#0f8443',
            handlePress: () => {
              Linking.openURL(
                'http://api.whatsapp.com/send?phone=971557707314',
              );
            },
          },
        ]}
      />
    </View>
  );
};

export default HomePage;
