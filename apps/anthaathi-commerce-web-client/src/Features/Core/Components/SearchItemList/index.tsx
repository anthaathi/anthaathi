import { useStyletron } from '@anthaathi/solid-styletron';
import { For } from 'solid-js';
import { Img } from '../Image';
import { SearchItemsQuery } from '../../../../../operations-types';
import { useNavigate } from '@solidjs/router';

const SearchItemList = (props: {
  list: NonNullable<SearchItemsQuery['products']>['items'];
  largeHeight: boolean;
}) => {
  const [css, $theme] = useStyletron();
  const navigate = useNavigate();

  return (
    <div
      class={css({
        maxHeight: '300px',
        width: `calc(299px - ${$theme.sizing.scale800})`,
        position: 'absolute',
        top: props.largeHeight ? '76px' : '76px',
        backgroundColor: '#ffffff',
        borderBottomLeftRadius: '4px',
        borderBottomRightRadius: '4px',
        padding: $theme.sizing.scale400,
        overflowY: 'scroll',
        scrollBehavior: 'smooth',
        border: '1px solid #EEEEEE',
        overflow: 'hidden',
        opacity: 1,
        WebkitTransition: 'all 500ms',
        transition: 'all 500ms',
      })}
    >
      <For each={props.list} fallback={<div>Loading...</div>}>
        {(item) => {
          return (
            <div
              class={css({
                marginTop: $theme.sizing.scale200,
                marginBottom: $theme.sizing.scale200,
                paddingBottom: $theme.sizing.scale200,
                borderBottom: '1px solid #d9d9d9',
                ':hover': {
                  cursor: 'pointer',
                },
              })}
              onClick={() => navigate('/product/' + item?.handle + '.html')}
            >
              <div
                class={css({
                  display: 'flex',
                })}
              >
                <Img
                  src={item?.small_image?.url!!}
                  $override={{
                    Root: {
                      $style: {
                        width: '64px',
                        height: '64px',
                        objectFit: 'cover',
                        border: '1px solid #e4e4d9',
                      },
                    },
                  }}
                />
                <div
                  class={css({
                    flexWrap: 'wrap',
                    paddingLeft: $theme.sizing.scale200,
                  })}
                >
                  <div
                    class={css({
                      fontSize: $theme.typography.font300.fontSize,
                      fontWeight: $theme.typography.font550.fontWeight,
                      marginBottom: $theme.sizing.scale200,
                    })}
                  >
                    {item?.title}
                  </div>
                  <div
                    class={css({
                      fontSize: $theme.typography.font250.fontSize,
                      fontWeight: $theme.typography.font550.fontWeight,
                      color: '#108842',
                    })}
                  >
                    {Intl.NumberFormat('en-US', {
                      currency:
                        item?.price?.minimum_price?.final_price?.currency ||
                        'AED',
                      style: 'currency',
                    }).format(
                      +(item?.price?.minimum_price?.final_price?.value ?? 0),
                    )}
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      </For>
    </div>
  );
};

export default SearchItemList;
