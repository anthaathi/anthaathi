import { useStyletron } from '@anthaathi/solid-styletron';
import Search from '../../../../icons/search.svg';
import { useCssToken } from '~/Features/Core/Hooks/useCssToken';
import { Input } from '~/Features/Core/Components/Input';
import { Button, Kind } from '~/Features/Core/Components/Button';
import { createEffect, createSignal } from 'solid-js';
import SearchItemList from '../SearchItemList';
import { useSearch } from '~/hooks/useSearch';
import debounce from 'lodash.debounce';
import { SearchItemsQuery } from '../../../../../operations-types';

export interface TSProductProps {
  id: string;
  title: string;
  description?: string;
  handle?: string;
  price: string;
  currency: string;
  image: string;
  collections?: string;
  notes?: string;
  packaging?: string;
}

export function Searchbar({ largeHeight }: { largeHeight?: boolean }) {
  const [css] = useStyletron();
  const [focused, setFocused] = createSignal(false);
  const [value, setValue] = createSignal<
    NonNullable<SearchItemsQuery['products']>['items']
  >([]);
  const [search, setSearch] = createSignal<string>('');
  const { getRecords } = useSearch();

  let searchInputRef: HTMLInputElement | null = null;

  const cssVar = useCssToken();

  function onSearchTap() {
    searchInputRef?.focus();
  }

  const searchItem = (searchText: string) => {
    return getRecords(searchText).then((response) => {
      if (search() === searchText) {
        setValue(response.records?.items || []);
      }
    });
  };

  createEffect(() => {
    searchItem(search());
  });

  return (
    <div
      class={css({
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <form
        class={css({
          display: 'flex',
          alignItems: 'center',
          backgroundColor: cssVar('search-background-color', '#EFEFEF'),
          borderRadius: cssVar(
            'search-border-radius',
            cssVar('input-border-radius', '4px'),
          ),
          width: '100%',
        })}
      >
        <Input
          type="search"
          placeholder="Search..."
          ref={(ref) => (searchInputRef = ref)}
          value={search()}
          $overrides={{
            Root: {
              style: {
                flexGrow: 1,
              },
            },
            Input: {
              style: {},
            },
          }}
          onInput={(prev) => {
            setSearch((prev.target as HTMLInputElement).value);
          }}
          onFocusIn={() => {
            setFocused(true);
          }}
          onFocusOut={() => {
            setTimeout(() => {
              setFocused(false);
            }, 300);
          }}
        />

        <Button
          $kind={Kind.Tertiary}
          type="button"
          onClick={onSearchTap}
          $startEnhancer={() => <Search />}
        />
      </form>
      {focused() && (
        <SearchItemList largeHeight={Boolean(largeHeight)} list={value()} />
      )}
    </div>
  );
}
