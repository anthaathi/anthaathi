import {
  IconMinusSmall,
  IconPlusSmall,
  IconTrashOSmall,
} from '@anthaathi/oracle-apex-solid-icons';
import { useStyletron } from '@anthaathi/solid-styletron';
import debounce from 'lodash.debounce';
import { createSignal } from 'solid-js';

export function CartQuantityChange({
  trashIcon,
  onChangeQuantity,
  initialValue = 0,
  packaingKG = false,
}: {
  id: string;
  initialValue?: number;
  trashIcon?: boolean;
  packaingKG?: boolean;
  onChangeQuantity: (input: number) => void;
}) {
  const [css, $theme] = useStyletron();

  const [quantity, setQuantity] = createSignal(initialValue);
  const [loader, setLoader] = createSignal<boolean>(false);

  async function increaseValue() {
    if (!loader()) {
      setLoader(true);
      setQuantity((prev) => {
        if (prev === 0) {
          return prev;
        }
        if (packaingKG && prev >= 1) {
          return prev + 0.25;
        } else {
          return prev + 1;
        }
      });
      await onChangeQuantity(quantity());
      setLoader(false);
    }
  }

  async function decreaseValue() {
    if (!loader()) {
      setLoader(true);

      setQuantity((prev) => {
        if (prev === 0) {
          return prev;
        }
        if (packaingKG && prev > 1) {
          return prev - 0.25;
        } else {
          return prev - 1;
        }
      });
      await onChangeQuantity(quantity());
      setLoader(false);
    }
  }

  return (
    <div
      class={css({
        height: '35px',
        width:
          Number(quantity()) === quantity() && quantity() % 1 !== 0
            ? '140px'
            : '120px',
        border: '0.5px solid #d9d9d9',
        display: 'flex',
        flexDirection: 'row',
        fontSize: '14px',
        justifyContent: 'center',
        textAlign: 'center',
      })}
    >
      <div
        class={css({
          flex: 1,
          height: '100%',
        })}
      >
        <button
          class={css({
            ...$theme.typography.ParagraphMedium,
            border: '0px solid #000000',
            margin: 0,
            paddingLeft: $theme.sizing.scale400,
            paddingRight: $theme.sizing.scale400,
            height: '100%',
            width: '100%',
            backgroundColor: '#d6d5d5',
            cursor: 'pointer',
            ':hover': {
              backgroundColor: '#cac9c9',
            },
          })}
          disabled={!trashIcon && quantity() === 1}
          onClick={() => {
            decreaseValue();
          }}
        >
          {trashIcon && quantity() === 1 ? (
            <IconTrashOSmall width="14px" height="14px" fill="#000" />
          ) : (
            <IconMinusSmall width="14px" height="14px" fill="#000" />
          )}
        </button>
      </div>
      <div
        class={css({
          flex: 1,
          height: '100%',
          alignItems: 'center',
          justifyContent: 'center',
          display: 'flex',
          paddingLeft: $theme.sizing.scale600,
          paddingRight: $theme.sizing.scale600,
        })}
      >
        <h4
          class={css([
            {
              ...$theme.typography.ParagraphMedium,
              fontWeight: 'bold',
              margin: 0,
              backgroundColor: '#ffffff',
            },
          ])}
        >
          {quantity()}
        </h4>
      </div>
      <div
        class={css({
          flex: 1,
          height: '100%',
        })}
      >
        <button
          class={css({
            ...$theme.typography.ParagraphMedium,
            border: '0px solid #000000',
            margin: 0,
            paddingLeft: $theme.sizing.scale400,
            paddingRight: $theme.sizing.scale400,
            height: '100%',
            width: '100%',
            backgroundColor: '#d6d5d5',
            cursor: 'pointer',
            ':hover': {
              backgroundColor: '#cac9c9',
            },
          })}
          onClick={() => {
            increaseValue();
          }}
        >
          <IconPlusSmall width="14px" height="14px" fill="#000" />
        </button>
      </div>
    </div>
  );
}
