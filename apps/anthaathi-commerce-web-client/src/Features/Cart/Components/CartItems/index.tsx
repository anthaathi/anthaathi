import { CartItem } from '~/Features/Commerce/Components/CartItem';
import { For, Show } from 'solid-js';
import { ProductProps } from '~/Features/Commerce/Components/ProductTile';
import {useCheckout} from "~/Features/Cart/Provider/CartProvider";
import { TSProductProps } from '~/Features/Core/Components/Searchbar';
import { cartItemStore } from '../../Hooks';

export interface ItemProps extends TSProductProps {
  numberOfItems: number;
}

export function CartItems() {
  const [cartItem] = cartItemStore;

  return (
    <For each={cartItem}>
      {(item) => {
        return (
          <Show
            when={item.quantity >= 0}
            keyed={true}
            fallback={() => <></>}
          >
            <CartItem numberOfItems={item.quantity} product={item} />
          </Show>
        );
      }}
    </For>
  );
}
