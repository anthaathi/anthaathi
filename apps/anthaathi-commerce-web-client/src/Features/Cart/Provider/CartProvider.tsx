import {Accessor, createContext, createSignal, JSX, onMount, useContext,} from 'solid-js';
import type {ItemProps} from '~/Features/Cart/Components/CartItems';
import {produce} from 'solid-js/store';
import productJson from '~/config/products.json';
import {getCheckoutId} from '~/Features/Cart/GQL/add-line-item';
import {isServer} from "solid-js/web";
import { useCart } from '../Hooks';
import { useAuth } from '~/hooks/useAuth';

type SetCartItems = (
  variantId: string | number,
  count?: number,
  isDelta?: boolean,
) => Promise<void>;

type CartContextType = [
  Accessor<CartState>,
  {
    setCartItems: SetCartItems;
  },
];

const CartContext = createContext<CartContextType>([{}, {}] as never);

export interface CartState {
  totalItems: Accessor<number>;
  lineItems: ItemProps[];
  moneyLines: MoneyLine[];
}

export const useCheckout = () => useContext(CartContext);

export interface MoneyLine {
  title: string;
  content: string;
}

export function CartProvider(props: {
  children: JSX.Element;
  initialValue?: CartState;
}) {
  const [totalItems, setTotalItems] = createSignal(props.initialValue?.totalItems?.() ?? props.initialValue?.lineItems.length ?? 0)

  const [cartState, setCartState] = createSignal<CartState>(
    props.initialValue ?? {
      lineItems: [],
      moneyLines: [],
      totalItems: totalItems,
    },
  );
  const { getCartItem } = useCart();
  const { getCustomerDetails } = useAuth();

  onMount(() => {
    getCartItem();
    getCustomerDetails();
    if (isServer) {
      return;
    }
    const checkoutId = getCheckoutId();

    if (!checkoutId) {
        return;
    }
  });

  const setCartItems: SetCartItems = async (productId, count = 1, isDelta = true) => {
    setCartState(
      produce((state) => {
        const prev = state.lineItems;

        const index = prev.findIndex((item) => item.id === productId);

        const product = productJson.featuredCollection.products.find(
          (res) => res.id === productId,
        );

        if (index === -1) {
          if (count === 0) {
            return state;
          }
          prev.push({
            ...product,
            numberOfItems: count,
          } as never);
        } else {
          if (isDelta) {
            prev[index].numberOfItems += count;
          } else {
            prev[index].numberOfItems = count;
          }

          if (prev[index].numberOfItems === 0) {
            prev.splice(index, 1);
          }
        }

        state.lineItems = prev;

        return state;
      }),
    );
  };

  const state: CartContextType = [
    cartState,
    {
      setCartItems,
    },
  ];

  return (
    <CartContext.Provider value={state}>{props.children}</CartContext.Provider>
  );
}
