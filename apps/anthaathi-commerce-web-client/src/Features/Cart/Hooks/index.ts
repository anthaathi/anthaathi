import { createStore } from 'solid-js/store';
import { gql } from '@apollo/client';
import Cookies from 'js-cookie';
import { createLazyQuery, createMutation } from '@merged/solid-apollo';
import { createSignal } from 'solid-js';
import toast from 'solid-toast';

export interface CartItemStoreProps {
  id: number;
  uid: string;
  product: {
    name: string;
    sku: string;
  };
  quantity: string;
}

export const cartItemStore = createStore<any[]>([]);

const addProductToCartQuery = gql`
  mutation addSimpleProductsToCartQ(
    $cartId: String!
    $sku: String!
    $quantity: Float!
  ) {
    addSimpleProductsToCart(
      input: {
        cart_id: $cartId
        cart_items: [{ data: { quantity: $quantity, sku: $sku } }]
      }
    ) {
      cart {
        items {
          id
          uid
          product {
            id: uid
            name
            sku
            title: meta_title
            description: meta_description
            url_key
            custom_attributes {
              attribute_metadata {
                label
                code
              }
              entered_attribute_value {
                value
              }
            }
            ... on PhysicalProductInterface {
              weight
            }
            price: price_range {
              minimum_price {
                discount {
                  amount_off
                  percent_off
                }
                regular_price {
                  currency
                  value
                }
                final_price {
                  value
                  currency
                }
              }
            }
            image {
              url
            }
          }
          quantity
        }
      }
    }
  }
`;

const updateCartItemQujery = gql`
  mutation updateCartItemQujery(
    $cartId: String!
    $cartItemId: ID!
    $quantity: Float!
  ) {
    updateCartItems(
      input: {
        cart_id: $cartId
        cart_items: [{ quantity: $quantity, cart_item_uid: $cartItemId }]
      }
    ) {
      cart {
        items {
          id
          uid
          product {
            id: uid
            name
            sku
            title: meta_title
            description: meta_description
            url_key
            custom_attributes {
              attribute_metadata {
                label
                code
              }
              entered_attribute_value {
                value
              }
            }
            ... on PhysicalProductInterface {
              weight
            }
            price: price_range {
              minimum_price {
                discount {
                  amount_off
                  percent_off
                }
                regular_price {
                  currency
                  value
                }
                final_price {
                  value
                  currency
                }
              }
            }
            image {
              url
            }
          }
          quantity
        }
      }
    }
  }
`;

const createEmptyCartQuery = gql`
  mutation createEmptyCartQuery {
    createEmptyCart
  }
`;

const mergeCartQuery = gql`
  mutation MergeCartsAfterSignIn(
    $sourceCartId: String!
    $destinationCartId: String!
  ) {
    mergeCarts(
      source_cart_id: $sourceCartId
      destination_cart_id: $destinationCartId
    ) {
      id
      items {
        id
        uid
        product {
          id: uid
          name
          sku
          title: meta_title
          description: meta_description
          url_key
          custom_attributes {
            attribute_metadata {
              label
              code
            }
            entered_attribute_value {
              value
            }
          }
          ... on PhysicalProductInterface {
            weight
          }
          price: price_range {
            minimum_price {
              discount {
                amount_off
                percent_off
              }
              regular_price {
                currency
                value
              }
              final_price {
                value
                currency
              }
            }
          }
          image {
            url
          }
        }
        quantity
      }
    }
  }
`;

export const getCartItemQuery = gql`
  query getCartItem($cartId: String!) {
    cart(cart_id: $cartId) {
      items {
        id
        uid
        product {
          id: uid
          name
          sku
          title: meta_title
          description: meta_description
          url_key
          custom_attributes {
            attribute_metadata {
              label
              code
            }
            entered_attribute_value {
              value
            }
          }
          ... on PhysicalProductInterface {
            weight
          }
          price: price_range {
            minimum_price {
              discount {
                amount_off
                percent_off
              }
              regular_price {
                currency
                value
              }
              final_price {
                value
                currency
              }
            }
          }
          image {
            url
          }
        }
        quantity
      }
    }
  }
`;

export const useCart = () => {
  const [cart, setCart] = cartItemStore;
  const [addProduct, { loading: loading1 }] = createMutation(
    addProductToCartQuery,
  );
  const [updateProduct, { loading: loading2 }] =
    createMutation(updateCartItemQujery);
  const [createEmptyCart] = createMutation(createEmptyCartQuery);
  const [mergeCart] = createMutation(mergeCartQuery);
  const [loadCartItem] = createLazyQuery(getCartItemQuery);
  const [loader, setLoader] = createSignal<boolean>(false);

  async function getCartId() {
    const cartId = await localStorage.getItem(
      'M2_VENIA_BROWSER_PERSISTENCE__cartId',
    );

    if (cartId === null) {
      const id = await createEmptyCart();
      localStorage.setItem(
        'M2_VENIA_BROWSER_PERSISTENCE__cartId',
        JSON.stringify({
          value: JSON.stringify(id.createEmptyCart),
          timeStored: new Date().getTime(),
        }),
      );
      return id.createEmptyCart;
    } else {
      return JSON.parse(JSON.parse(cartId).value);
    }
  }

  async function addProductToCart(sku: string, quantity: number) {
    if (!loading1 && !loading2 && !loader()) {
      setLoader(true);
      const cartId = await getCartId();
      try {
        const addedData = await addProduct({
          variables: {
            cartId: cartId,
            sku: sku,
            quantity: quantity,
          },
        });
        setCart(addedData.addSimpleProductsToCart.cart.items);
        setLoader(false);
        return addedData;
      } catch (e: any) {
        toast.error(e.message);
        setLoader(false);
        return null;
      }
    }
  }

  async function updateProductToCart(
    cartItemId: string,
    currentQuantity: number,
  ) {
    if (!loading1 && !loading2 && !loader()) {
      setLoader(true);
      const cartId = await getCartId();

      const updatedData: any = await updateProduct({
        variables: {
          cartId: cartId,
          cartItemId: cartItemId,
          quantity: currentQuantity - 1,
        },
      });
      setCart(updatedData.updateCartItems.cart.items);
      setLoader(false);
      return updatedData;
    }
  }

  async function getCartItem() {
    const cartId = await getCartId();
    const cartData: any = await loadCartItem({
      variables: {
        cartId: cartId,
      },
    });
    setCart(cartData?.cart?.items);
    return cartData;
  }

  async function createEmptyCartAuthAndMergeCart() {
    const cartId = await localStorage.getItem(
      'M2_VENIA_BROWSER_PERSISTENCE__cartId',
    );

    if (cartId === null) {
      const id = await createEmptyCart();
      localStorage.setItem(
        'M2_VENIA_BROWSER_PERSISTENCE__cartId',
        JSON.stringify({
          value: JSON.stringify(id.createEmptyCart),
          timeStored: new Date().getTime(),
        }),
      );
      return id.createEmptyCart;
    } else {
      const id = await createEmptyCart();
      const data = await mergeCart({
        variables: {
          sourceCartId: JSON.parse(JSON.parse(cartId).value),
          destinationCartId: id.createEmptyCart,
        },
      }).catch((e) => {});
      if (data) {
        setCart(data?.mergeCarts?.items);
      }
      localStorage.setItem(
        'M2_VENIA_BROWSER_PERSISTENCE__cartId',
        JSON.stringify({
          value: JSON.stringify(id.createEmptyCart),
          timeStored: new Date().getTime(),
        }),
      );
      return data;
    }
  }

  return {
    addProductToCart,
    updateProductToCart,
    getCartItem,
    getCartId,
    createEmptyCartAuthAndMergeCart,
  };
};
