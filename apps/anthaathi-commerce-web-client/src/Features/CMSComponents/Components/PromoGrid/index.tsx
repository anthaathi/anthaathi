import {
  useResponsiveStyletron,
  useStyletron,
} from '@anthaathi/solid-styletron';
import { Button, Kind } from '~/Features/Core/Components/Button';
import { Grid } from '~/Features/Core/Components/Grid';
import { Img } from '~/Features/Core/Components/Image';
import { Link } from '@solidjs/router';
import { Responsive } from '~/utils/common';
import { Property } from 'csstype';
import { For } from 'solid-js';

export interface PromoThings {
  srsSet: string[];
  sizes: string;
  src: string;
  alt: string;
  title?: string;
  href: string;
}

export function PromoGrid({
  list,
  shape,
  size,
}: {
  list: PromoThings[];
  shape?: Shape;
  size?: Size;
}) {
  const [css, $theme] = useStyletron();
  const columnData =
    list.length === 1 ? [1] : list.length > 2 ? [1, 2, 3, 3] : [1, 2];
  return (
    <div
      class={css({
        maxWidth: $theme.sizing.maxWidth,
        margin: '0 auto',
        width: `calc(100% - ${$theme.sizing.scale500} - ${$theme.sizing.scale500})`,
        paddingTop: $theme.sizing.scale700,
        paddingBottom: $theme.sizing.scale700,
      })}
    >
      <Grid
        $override={{
          Root: {
            style: {
              maxWidth: $theme.sizing.maxWidth,
              marginLeft: 'auto',
              marginRight: 'auto',
              gridRowGap: '8px',
              gridColumnGap: '24px',
            },
          },
        }}
        columns={columnData}
      >
        <For each={list}>
          {(product) => (
            <PromoGridCell $size={size} $shape={shape} {...product} />
          )}
        </For>
      </Grid>
    </div>
  );
}

export interface PromoGridCellProps {
  src: string;
  srsSet: string[];
  sizes: string;
  alt: string;
  href: string;
  title?: string;
  $shape?: Shape;
  $size?: Size;
}

export enum Shape {
  Default = 'default',
  Circle = 'circle',
}

export enum Size {
  Default = 'default',
  Mini = 'mini',
}

export function PromoGridCell(props: PromoGridCellProps) {
  const [css, $theme] = useStyletron();
  const [css$] = useResponsiveStyletron();
  const srcSet = props.srsSet.join(', ');

  let heightWidth: (Property.Width | undefined)[];

  switch (props.$size) {
    case Size.Mini:
      heightWidth = ['180px', '180px', '220px'];
      break;
    default:
      heightWidth = ['140px', '180px', '240px', '360px'];
  }

  return (
    <Link
      href={`/collections/${props.href}`}
      class={css({
        display: 'flex',
        alignItems: 'center',
        placeContent: 'center',
      })}
    >
      <div
        class={css$({
          height: heightWidth as never,
          width:
            props.$shape === Shape.Circle ? (heightWidth as never) : '100%',
          maxHeight: '100%',
          display: 'flex',
          alignItems: 'center',
          placeContent: 'center',
          flexDirection: 'column',
          position: 'relative',
          borderRadius: props.$shape === Shape.Circle ? '50%' : '2%',
        })}
      >
        <Img
          src={props.src}
          $override={{
            Root: {
              $style: {
                objectFit: 'contain',
                width: '100%',
                height: '100%',
                borderRadius: props.$shape === Shape.Circle ? '50%' : '2%',
                ':hover': {
                  animation: 'fade-in-show .4s',
                  transform: 'scale3d(1.01, 1.01, 1.01)',
                },
              },
            },
          }}
          alt={props.alt}
          data-srcset={srcSet}
          sizes={props.sizes}
          srcSet={srcSet}
        />
      </div>
    </Link>
  );
}
