import { ServerContext } from 'solid-start/server';
import { createEffect, createSignal, useContext } from 'solid-js';
import { EmailSignup } from '~/Features/CMSComponents/Components/EmailSignup';
import { addServerTiming } from '~/utils/add-server-timing';
import { isServer } from 'solid-js/web';
import { NewsLetter } from '~/Features/CMSComponents/Components/NewsLetter';
import { FeaturedCollection } from '~/Features/CMSComponents/Components/FeaturedCollection';
import { SplitOfferCard } from './Components/SplitOfferCard';
import { HeroSlide } from './Components/HeroSlide';
import { HowItWorks } from '~/Features/CMSComponents/Components/HowItWorks';
import { AboutUs } from '~/Features/CMSComponents/Components/AboutUs';
import { PromotionalProductGrid } from './Components/PromotionalProductGrid';
import { ProductInfo } from '~/Features/Commerce/Components/ProductInfo';
import { BlogPosts } from '~/Features/CMSComponents/Components/BlogPosts';
import { FAB } from '~/Features/Core/Components/FAB';
import MobileAppPromoter from '~/Features/CMSComponents/Components/MobileAppPromoter';
import { CategoryList } from './Components/CategoryList';
import productJson from '../../config/products.json';
import categoryJson from '../../config/category.json';
import { BannerSlide } from './Components/BannerSilde';
import { PromoGrid } from './Components/PromoGrid';
import {
  IconClipboardCheckLarge,
  IconIdCardOLarge,
  IconTruckLarge,
} from '@anthaathi/oracle-apex-solid-icons';
import { gql } from '@apollo/client';
import { createLazyQuery, createQuery } from '@merged/solid-apollo';

export const getCollectionQuery = gql`
  query getCategoryByHandle1($handle: String!) {
    categories(filters: { url_key: { eq: $handle } }) {
      items {
        ... on CategoryTree {
          meta_title
          products(currentPage: 1, pageSize: 5) {
            items {
              id: uid
              name
              sku
              title: meta_title
              description: meta_description
              url_key
              stock_status
              custom_attributes {
                attribute_metadata {
                  label
                  code
                }
                entered_attribute_value {
                  value
                }
              }
              country_of_manufacture
              short_description {
                html
              }
              ... on PhysicalProductInterface {
                weight
              }
              price: price_range {
                minimum_price {
                  discount {
                    amount_off
                    percent_off
                  }
                  regular_price {
                    currency
                    value
                  }
                  final_price {
                    value
                    currency
                  }
                }
              }
              image {
                url
              }
            }
            page_info {
              page_size
              current_page
              total_pages
            }
            total_count
          }
        }
      }
    }
  }
`;
export function RenderCMSComponents() {
  const context = useContext(ServerContext);
  const timeStart = new Date();
  const [loadCollectionProduct] = createLazyQuery(getCollectionQuery);
  const [list1, setList1] = createSignal([]);
  const [list2, setList2] = createSignal([]);

  async function getFeaturedCollectionProducts() {
    try {
      const data: any = await loadCollectionProduct({
        variables: {
          handle: 'fruits',
        },
      });
      setList1(data?.categories.items[0].products.items);

      const data1: any = await loadCollectionProduct({
        variables: {
          handle: 'juices',
        },
      });
      setList2(data1?.categories.items[0].products.items);
    } catch (e) {
      console.log(e);
    }
  }

  createEffect(() => {
    getFeaturedCollectionProducts();
  }, []);

  const App = () => [
    <NewsLetter />,
    <MobileAppPromoter />,
    <BannerSlide
      list={[
        {
          id: '1',
          imgSrc:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1080x.png?v=1666873198',
          srcSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_180x.png?v=1666873198 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_360x.png?v=1666873198 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_540x.png?v=1666873198 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_720x.png?v=1666873198 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_900x.png?v=1666873198 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1080x.png?v=1666873198 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1296x.png?v=1666873198 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1512x.png?v=1666873198 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1728x.png?v=1666873198 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1944x.png?v=1666873198 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_2160x.png?v=1666873198 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_2376x.png?v=1666873198 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_2592x.png?v=1666873198 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_2808x.png?v=1666873198 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/1_3024x.png?v=1666873198 3024w',
          ],
          href: '/',
        },
        {
          id: '2',
          imgSrc:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_300x.png?v=1666873207',
          href: '/',
          srcSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_180x.png?v=1666873207 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_360x.png?v=1666873207 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_540x.png?v=1666873207 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_720x.png?v=1666873207 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_900x.png?v=1666873207 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_1080x.png?v=1666873207 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_1296x.png?v=1666873207 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_1512x.png?v=1666873207 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_1728x.png?v=1666873207 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_1944x.png?v=1666873207 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_2160x.png?v=1666873207 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_2376x.png?v=1666873207 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_2592x.png?v=1666873207 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_2808x.png?v=1666873207 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Flash_Sale_3024x.png?v=1666873207 3024w',
          ],
        },
        {
          id: '3',
          imgSrc:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/3_300x.png?v=1666873213',
          href: '/',
          srcSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_180x.png?v=1666873213 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_360x.png?v=1666873213 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_540x.png?v=1666873213 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_720x.png?v=1666873213 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_900x.png?v=1666873213 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_1080x.png?v=1666873213 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_1296x.png?v=1666873213 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_1512x.png?v=1666873213 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_1728x.png?v=1666873213 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_1944x.png?v=1666873213 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_2160x.png?v=1666873213 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_2376x.png?v=1666873213 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_2592x.png?v=1666873213 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_2808x.png?v=1666873213 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/3_3024x.png?v=1666873213 3024w',
          ],
        },
        {
          id: '4',
          imgSrc:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_300x.png?v=1666873233',
          href: '/',
          srcSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_180x.png?v=1666873233 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_360x.png?v=1666873233 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_540x.png?v=1666873233 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_720x.png?v=1666873233 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_900x.png?v=1666873233 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_1080x.png?v=1666873233 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_1296x.png?v=1666873233 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_1512x.png?v=1666873233 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_1728x.png?v=1666873233 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_1944x.png?v=1666873233 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_2160x.png?v=1666873233 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_2376x.png?v=1666873233 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_2592x.png?v=1666873233 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_2808x.png?v=1666873233 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/4_1_3024x.png?v=1666873233 3024w',
          ],
        },
      ]}
    />,
    <CategoryList title="Categories" items={categoryJson.categoryList} />,
    <FeaturedCollection title="In Season" products={list1()} />,
    <PromoGrid
      list={[
        {
          srsSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_180x.png?v=1666946638 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_360x.png?v=1666946638 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_540x.png?v=1666946638 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_720x.png?v=1666946638 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_900x.png?v=1666946638 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_1080x.png?v=1666946638 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_1296x.png?v=1666946638 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_1512x.png?v=1666946638 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_1728x.png?v=1666946638 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_1944x.png?v=1666946638 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_2160x.png?v=1666946638 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_2376x.png?v=1666946638 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_2592x.png?v=1666946638 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_2808x.png?v=1666946638 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_3024x.png?v=1666946638 3024w',
          ],
          sizes: '448px',
          src: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/Fruit_basket_cd2173b2-7c90-411d-aeef-3749ee0b159b_300x.png?v=1666946638',
          alt: '',
          href: '/fruits',
          title: 'Fruits',
        },
        {
          srsSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_180x.png?v=1666946638 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_360x.png?v=1666946638 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_540x.png?v=1666946638 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_720x.png?v=1666946638 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_900x.png?v=1666946638 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_1080x.png?v=1666946638 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_1296x.png?v=1666946638 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_1512x.png?v=1666946638 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_1728x.png?v=1666946638 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_1944x.png?v=1666946638 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_2160x.png?v=1666946638 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_2376x.png?v=1666946638 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_2592x.png?v=1666946638 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_2808x.png?v=1666946638 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_3024x.png?v=1666946638 3024w',
          ],
          sizes: '448px',
          alt: '',
          href: '/vegetables',
          title: 'Vegetables',
          src: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh_Juice_300x.png?v=1666946638',
        },
        {
          title: 'Pre-Packed',
          src: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_300x.png?v=1666946638',
          srsSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_180x.png?v=1666946638 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_360x.png?v=1666946638 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_540x.png?v=1666946638 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_720x.png?v=1666946638 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_900x.png?v=1666946638 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_1080x.png?v=1666946638 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_1296x.png?v=1666946638 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_1512x.png?v=1666946638 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_1728x.png?v=1666946638 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_1944x.png?v=1666946638 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_2160x.png?v=1666946638 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_2376x.png?v=1666946638 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_2592x.png?v=1666946638 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_2808x.png?v=1666946638 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/Most_Selling_3024x.png?v=1666946638 3024w',
          ],
          sizes: '448px',
          alt: '',
          href: '/prepacked',
        },
      ]}
    />,

    <PromoGrid
      list={[
        {
          srsSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_180x.png?v=1666947921 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_360x.png?v=1666947921 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_540x.png?v=1666947921 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_720x.png?v=1666947921 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_900x.png?v=1666947921 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1080x.png?v=1666947921 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1296x.png?v=1666947921 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1512x.png?v=1666947921 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1728x.png?v=1666947921 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1944x.png?v=1666947921 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2160x.png?v=1666947921 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2376x.png?v=1666947921 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2592x.png?v=1666947921 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2808x.png?v=1666947921 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_3024x.png?v=1666947921 3024w',
          ],
          sizes: '448px',
          src: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_300x.png?v=1666947921',
          alt: '',
          href: '/fruits',
          title: 'Fruits',
        },
        {
          srsSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_180x.png?v=1666947921 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_360x.png?v=1666947921 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_540x.png?v=1666947921 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_720x.png?v=1666947921 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_900x.png?v=1666947921 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1080x.png?v=1666947921 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1296x.png?v=1666947921 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1512x.png?v=1666947921 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1728x.png?v=1666947921 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_1944x.png?v=1666947921 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2160x.png?v=1666947921 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2376x.png?v=1666947921 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2592x.png?v=1666947921 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_2808x.png?v=1666947921 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_3024x.png?v=1666947921 3024w',
          ],
          sizes: '448px',
          src: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/DelMonte_a874a34b-6e91-487e-b19f-e848317a77b8_300x.png?v=1666947921',
          alt: '',
          href: '/fruits',
          title: 'Fruits',
        },
      ]}
    />,

    <PromoGrid
      list={[
        {
          srsSet: [
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_180x.png?v=1666950289 180w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_360x.png?v=1666950289 360w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_540x.png?v=1666950289 540w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_720x.png?v=1666950289 720w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_900x.png?v=1666950289 900w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_1080x.png?v=1666950289 1080w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_1296x.png?v=1666950289 1296w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_1512x.png?v=1666950289 1512w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_1728x.png?v=1666950289 1728w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_1944x.png?v=1666950289 1944w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_2160x.png?v=1666950289 2160w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_2376x.png?v=1666950289 2376w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_2592x.png?v=1666950289 2592w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_2808x.png?v=1666950289 2808w',
            '//cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_3024x.png?v=1666950289 3024w',
          ],
          sizes: '448px',
          src: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/organic_Ban_300x.png?v=1666950289',
          alt: '',
          href: '/fruits',
          title: 'Fruits',
        },
      ]}
    />,
    <FeaturedCollection title="Special Offers" products={list2()} />,
    // <ImageAndText />,

    <SplitOfferCard
      title={'Get Exclusive Offers'}
      subtitle={
        'Get exclusive offers & more by signing up for our promotional email'
      }
      buttonTitle={'View Offers'}
      image={
        'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/Newsletter_bg_300x.png?v=1653648705'
      }
    />,
    <HeroSlide
      backgroundImageSrc="https://cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh-squeezed-orange-juice_300x.jpg?v=1653584430"
      title="Fresh Juices"
      subTitle="Perfect for rich lifestyle photography"
      buttonTitle="View All"
    />,
    <PromotionalProductGrid
      products={[
        {
          name: 'Baby Yellow Pepper',
          image:
            'https://burst.shopifycdn.com/photos/fruit-plate.jpg?width=373&height=373&format=pjpg&exif=1&iptc=1',
          price: 12,
          currency: 'AED',
          heading: 'NRTC Fresh',
          buttonTitle: 'Shop',
          label: 'New',
          description: '100% fresh. Sourced from Netherlands',
        },
        {
          name: 'Capsicum mixed',
          image:
            'https://burst.shopifycdn.com/photos/red-and-green-gooseberries-against-white.jpg?width=373&format=pjpg&exif=1&iptc=1',
          price: 23,
          currency: 'AED',
          heading: 'NRTC Fresh',
          buttonTitle: 'Shop',
          label: 'New',
          description: '100% fresh. Sourced from Netherlands',
        },
      ]}
    />,
    <HowItWorks
      title="How It Works"
      subtitle="Shopping for the freshest produce has never been easier"
      list={[
        {
          icon: <IconIdCardOLarge height="70px" width="70px" />,
          title: 'Register',
        },
        {
          icon: <IconClipboardCheckLarge height="70px" width="70px" />,
          title: 'Select Products & Place Order',
        },
        {
          icon: <IconTruckLarge height="70px" width="70px" />,
          title: 'Schedule Delivery',
        },
      ]}
    />,
    <BlogPosts
      title="From the journal"
      mainBlog={{
        id: 1,
        image:
          'https://cdn.shopify.com/s/files/1/0648/1303/9842/articles/Fathers-day-recipe-ideas-by-fresh-fruit-company-in-Dubai-1200x600_1080x.jpg?v=1653586976',
        title: '3 Fantastic Father’s Day Meals with help from NRTC Fresh',
        published_date: 'MAY 26, 2022',
        author: 'Omkar Yadav',
      }}
      blogs={[
        {
          id: 2,
          image:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/articles/5-ways-to-reduce-food-wastage-with-Fresh-fruits-and-Vegetables-1200x600_360x.jpg?v=1653586497',
          title: '5 Fruit and Vegetable preparation tips with NRTC Fresh',
          published_date: 'MAY 26, 2022',
          author: 'Omkar Yadav',
        },
        {
          id: 3,
          image:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/articles/5-tips-to-cook-delicious-vegetable-online-Dubai-1200x600_900x.jpg?v=1653586237',
          title: '5 tips to cook delicious Vegetables from NRTC Fresh',
          published_date: 'MAY 26, 2022',
          author: 'Omkar Yadav',
        },
        {
          id: 4,
          image:
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/articles/Make-perfect-Veggie-burger-with-online-vegetables-Dubai-1200x600_360x.jpg?v=1653585741',
          title:
            'How To Make The Perfect Veggie Burger With NRTC Fresh Vegetables',
          published_date: 'MAY 26, 2022',
          author: 'Omkar Yadav',
        },
      ]}
    />,
    <AboutUs />,
    <FAB />,
  ];

  const renderedElements = (
    <>
      <App />
    </>
  );

  const timeEnd = new Date();

  if (isServer) {
    addServerTiming(
      context.responseHeaders,
      'cms-rendered',
      timeEnd.getTime() - timeStart.getTime(),
      'Render CMS Components Dynamic',
    );
  }

  return renderedElements;
}
