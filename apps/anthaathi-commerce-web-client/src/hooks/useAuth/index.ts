import { gql } from '@apollo/client';
import { createLazyQuery, createMutation } from '@merged/solid-apollo';
import { createStore } from 'solid-js/store';
import { useCart } from '~/Features/Cart/Hooks';

export interface UserProps {
  email: string;
  firstname: string;
  lastname: string;
  token: string;
}

export const userDataStore = createStore<UserProps>({} as UserProps);

const signInQuery = gql`
  mutation SignIn($email: String!, $password: String!) {
    generateCustomerToken(email: $email, password: $password) {
      token
      __typename
    }
  }
`;

const getCustomerQuery = gql`
  query getCustomerData {
    customer {
      email
      firstname
      lastname
    }
  }
`;

const createAccountQuery = gql`
  mutation CreateAccount(
    $email: String!
    $firstname: String!
    $lastname: String!
    $password: String!
    $is_subscribed: Boolean!
  ) {
    createCustomerV2(
      input: {
        email: $email
        firstname: $firstname
        lastname: $lastname
        password: $password
        is_subscribed: $is_subscribed
      }
    ) {
      customer {
        email
        firstname
        lastname
      }
      __typename
    }
  }
`;

const resetPasswordQuery = gql`
  mutation resetPasswordQuery($email: String!) {
    requestPasswordResetEmail(email: $email)
  }
`;

export const useAuth = () => {
  const [user, setUser] = userDataStore;
  const [getCustomer] = createLazyQuery(getCustomerQuery);
  const [signIn] = createMutation(signInQuery);
  const [createAccount] = createMutation(createAccountQuery);
  const [resetPasswordLink, { error }] = createMutation(resetPasswordQuery);
  const { createEmptyCartAuthAndMergeCart } = useCart();

  async function getToken() {
    const userToken = await localStorage.getItem(
      'M2_VENIA_BROWSER_PERSISTENCE__signin_token',
    );

    if (userToken === null) {
      return null;
    } else {
      return JSON.parse(JSON.parse(userToken).value);
    }
  }

  async function signInUser(
    email: string,
    password: string,
    called: boolean = true, // for create account
  ) {
    try {
      const userData: any = await signIn({
        variables: {
          email: email,
          password: password,
        },
      });
      setUser({ ...user, token: userData.generateCustomerToken.token });
      localStorage.setItem(
        'M2_VENIA_BROWSER_PERSISTENCE__signin_token',
        JSON.stringify({
          value: JSON.stringify(userData.generateCustomerToken.token),
          timeStored: new Date().getTime(),
        }),
      );
      await createEmptyCartAuthAndMergeCart();
      if (called) {
        await getCustomerDetails();
      }
      return userData;
    } catch (e: any) {
      return e.message;
    }
  }

  async function createUserAccount(
    email: string,
    password: string,
    firstname: string = '',
    lastname: string = '',
    is_subscribed: boolean = false,
  ) {
    try {
      const userData: any = await createAccount({
        variables: {
          email: email,
          password: password,
          firstname: firstname,
          lastname: lastname,
          is_subscribed: is_subscribed,
        },
      });
      setUser({
        ...user,
        email: userData.createCustomerV2.customer.email,
        firstname: userData.createCustomerV2.customer.firstname,
        lastname: userData.createCustomerV2.customer.lastname,
      });
      await signInUser(email, password, false);
      return userData;
    } catch (e: any) {
      return e.message;
    }
  }

  async function getCustomerDetails() {
    const token = await getToken();
    if (token !== null) {
      const customerData: any = await getCustomer();
      setUser({
        ...user,
        token: token,
        email: customerData?.customer.email,
        firstname: customerData?.customer.firstname,
        lastname: customerData?.customer.lastname,
      });

      return customerData;
    }
    return null;
  }

  async function resetPassword(email: string) {
    try {
      const data: any = await resetPasswordLink({
        variables: {
          email: email,
        },
      });

      return data;
    } catch (e: any) {
      return e.message;
    }
  }

  return {
    getToken,
    signInUser,
    createUserAccount,
    getCustomerDetails,
    resetPassword,
  };
};
