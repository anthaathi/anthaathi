import { useStyletron } from '@anthaathi/solid-styletron';
import { createSignal, For } from 'solid-js';
import {
  ProductProps,
  ProductTile,
} from '~/Features/Commerce/Components/ProductTile';
import { Grid } from '~/Features/Core/Components/Grid';
import { Input } from '~/Features/Core/Components/Input';
import { Button } from '~/Features/Core/Components/Button';
import { client } from '~/utils/fetchGraphQL';
import { gql } from '@apollo/client';
import {
  SearchProductQuery,
  SearchProductQueryVariables,
  SimpleProduct,
} from '../../../operations-types';

export default function Search() {
  const [css, $theme] = useStyletron();
  const [searchText, setSearchText] = createSignal('');
  const [products, setProducts] = createSignal<
    NonNullable<SearchProductQuery['products']>['items']
  >([]);
  const [isLoading, setIsLoading] = createSignal(false);

  function searchItems() {
    setIsLoading(true);
    const s = searchText();
    client
      .query<SearchProductQuery, SearchProductQueryVariables>({
        query: gql`
          query SearchProduct($search: String!) {
            products(search: $search) {
              items {
                id: uid
                name
                sku
                title: meta_title
                description: meta_description
                url_key
                price: price_range {
                  minimum_price {
                    discount {
                      amount_off
                      percent_off
                    }
                    regular_price {
                      currency
                      value
                    }
                    final_price {
                      value
                      currency
                    }
                  }
                }
                image {
                  url
                }
                ... on SimpleProduct {
                  country_of_manufacture
                }
              }
            }
          }
        `,
        variables: {
          search: s as never,
        },
      })
      .then((docs) => {
        setIsLoading(false);
        if (s === searchText()) {
          setProducts(docs.data.products?.items);
        }
      });
  }

  return (
    <div
      class={css({
        backgroundColor: '#FFF',
        padding: $theme.sizing.scale500,
        [$theme.mediaQuery?.md || '']: {
          display: 'none',
        },
      })}
    >
      <div class={css({ display: 'flex', alignItems: 'center' })}>
        <Input
          placeholder="Search"
          $overrides={{ Root: { style: { flexGrow: 1 } } }}
          onChange={(e) => {
            setSearchText((e.target as HTMLInputElement).value);
          }}
          onKeyUp={(e) => {
            if (e.key === 'Enter' || e.keyCode === 13) {
              searchItems();
            }
          }}
          value={searchText() as never}
        />
        <Button onClick={searchItems}>Search</Button>
      </div>

      <div
        class={css(
          isLoading()
            ? {
                '::before': {
                  content: "''",
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  backgroundColor: 'rgba(255, 255, 255, .4)',
                  zIndex: 1,
                },
              }
            : {},
        )}
      ></div>

      <Grid
        $override={{
          Root: {
            style: {
              gridGap: '15px',
              paddingTop: '12px',
            },
          },
        }}
        columns={[2, 2, 3, 4]}
      >
        <For each={products()}>
          {(product) => (
            <ProductTile
              id={product?.id ?? ''}
              sku={product?.sku ?? ''}
              title={product?.title ?? product?.name ?? ''}
              description={product?.description ?? ''}
              price={
                product?.price.minimum_price.final_price.value?.toString() ?? ''
              }
              currency={
                product?.price.minimum_price?.final_price?.currency ?? 'AED'
              }
              image={product?.image?.url ?? ''}
              handle={(product?.url_key ?? '') + '.html'}
            />
          )}
        </For>
      </Grid>
    </div>
  );
}
