import { Input } from '~/Features/Core/Components/Input';
import { useStyletron } from '@anthaathi/solid-styletron';
import { useCssToken } from '~/Features/Core/Hooks/useCssToken';
import { FormControl } from '~/Features/Core/Components/FormControl';
import { Link, useNavigate } from '@solidjs/router';
import { createSignal } from 'solid-js';
import { useAuth, userDataStore } from '~/hooks/useAuth';
import toast from 'solid-toast';

export function validateEmail(email: string) {
  const res =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return res.test(String(email).toLowerCase());
}

export default function Login() {
  const [css, $theme] = useStyletron();
  const cssVar = useCssToken();
  const [state, setState] = createSignal<{ email: string; password: string }>({
    email: '',
    password: '',
  });
  const [error, setError] = createSignal<{ email: string; password: string }>({
    email: '',
    password: '',
  });
  const [showPassword, setShowPassword] = createSignal<boolean>(false);
  const navigate = useNavigate();
  const { signInUser } = useAuth();
  const [user] = userDataStore;

  const loginUser = async () => {
    setError({ ...error(), email: '', passowrd: '' });
    if (state().email === '') {
      setError({ ...error(), email: 'Email is required' });
    } else {
      if (!validateEmail(state().email)) {
        setError({ ...error(), email: 'Enter correct email' });
      }
    }
    if (state().password === '') {
      setError({ ...error(), password: 'Password is required' });
    }
    if (error().email === '' && error().password === '') {
      const data = await signInUser(state().email, state().password);
      if (data && data.generateCustomerToken !== undefined) {
        toast.success('Login successfully!');
        window.open('/my-account/', '_self');
      } else {
        toast.error(data);
      }
    }
  };

  return (
    <>
      <form
        class={css({
          maxWidth: $theme.sizing.maxWidth,
          margin: '0 auto',
          paddingTop: $theme.sizing.scale800,
          paddingBottom: $theme.sizing.scale800,
        })}
      >
        <div
          class={css({
            backgroundColor: cssVar('login-background-color', '#FEFEFE'),
            padding: $theme.sizing.scale800,
            maxWidth: '420px',
            margin: '0 auto',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
          })}
        >
          <h1
            class={css({
              marginLeft: 0,
              marginRight: 0,
              marginBottom: $theme.sizing.scale800,
            })}
          >
            Sign in
          </h1>

          <div class={css({ width: '100%' })}>
            <FormControl for="emailOrPhone" label="Email">
              <Input
                name="email"
                value={state().email}
                error={error().email}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    email: (prev.target as HTMLInputElement).value,
                  });
                }}
                type="text"
                id="emailOrPhone"
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
          </div>
          <div
            class={css({ width: '100%', marginTop: $theme.sizing.scale600 })}
          >
            <FormControl
              for="password"
              label="Password"
              extraLabelContent={() => (
                <Link
                  class={css({
                    textDecoration: 'none',
                    color: cssVar('primary-b-color', '#000'),
                    ...$theme.typography.LabelSmall,
                  })}
                  href="/account/forgot-password"
                >
                  Forgot password?
                </Link>
              )}
            >
              <Input
                type={showPassword() ? 'text' : 'password'}
                id="password"
                name="password"
                error={error().password}
                value={state().password}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    password: (prev.target as HTMLInputElement).value,
                  });
                }}
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
            <div
              class={css({
                display: 'flex',
                marginTop: $theme.sizing.scale200,
                alignItems: 'center',
                ':hover': { cursor: 'pointer' },
              })}
              onClick={() => {
                setShowPassword(!showPassword());
              }}
            >
              <Input
                type="checkbox"
                id="is_subscribed"
                name="is_subscribed"
                checked={showPassword()}
                width="100%"
              />
              <div
                class={css({
                  color: '#000',
                  fontWeight: '500',
                  fontSize: '15px',
                  marginLeft: $theme.sizing.scale300,
                })}
              >
                Show password
              </div>
            </div>
          </div>

          <div class={css({ paddingTop: '12px' })} />

          <div
            onClick={() => {
              loginUser();
            }}
            class={css({
              marginTop: $theme.sizing.scale900,
              width: '100%',
              textAlign: 'center',
              backgroundColor: '#118b44',
              paddingTop: '12px',
              paddingBottom: '12px',
              color: '#fff',
              fontWeight: 'bold',
              fontSize: '18px',
              borderRadius: '4px',
              ':hover': { cursor: 'pointer' },
            })}
          >
            Login
          </div>

          <div
            onClick={() => {
              navigate('/account/create-account');
            }}
            class={css({
              marginTop: $theme.sizing.scale900,
              width: '100%',
              paddingTop: '12px',
              paddingBottom: '12px',
              borderRadius: '4px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              ':hover': { cursor: 'pointer' },
            })}
          >
            <div
              class={css({
                color: '#000',
                fontWeight: '500',
                fontSize: '15px',
              })}
            >
              Don't have an account?
            </div>
            <div
              class={css({
                color: '#118b44',
                fontWeight: '700',
                fontSize: '15px',
              })}
            >
              &nbsp;Sign Up
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
