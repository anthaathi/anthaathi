import { useStyletron } from '@anthaathi/solid-styletron';
import { useCssToken } from '~/Features/Core/Hooks/useCssToken';
import { RenderForm } from '~/Features/DynamicForm';
import { NoHydration } from 'solid-js/web';
import { FormControl } from '~/Features/Core/Components/FormControl';
import { Input } from '~/Features/Core/Components/Input';
import { Link, useNavigate } from '@solidjs/router';
import { useAuth } from '~/hooks/useAuth';
import { createSignal } from 'solid-js';
import { validateEmail } from '../login';
import toast from 'solid-toast';

export default function CreateAccount() {
  const [css, $theme] = useStyletron();
  const cssVar = useCssToken();
  const [state, setState] = createSignal<{
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    is_subscribed: boolean;
  }>({
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    is_subscribed: false,
  });

  const [error, setError] = createSignal<{
    email: string;
    password: string;
    firstname: string;
    lastname: string;
  }>({
    email: '',
    password: '',
    firstname: '',
    lastname: '',
  });
  const [showPassword, setShowPassword] = createSignal<boolean>(false);

  const navigate = useNavigate();
  const { createUserAccount } = useAuth();

  const createUser = async () => {
    setError({
      ...error(),
      email: '',
      password: '',
      firstname: '',
      lastname: '',
    });

    if (state().email === '') {
      setError({ ...error(), email: 'Email is required' });
    } else {
      if (!validateEmail(state().email)) {
        setError({ ...error(), email: 'Enter correct email' });
      }
    }
    if (state().firstname === '') {
      setError({ ...error(), firstname: 'First name is required' });
    }
    if (state().lastname === '') {
      setError({ ...error(), lastname: 'Last name is required' });
    }
    if (state().password === '') {
      setError({ ...error(), password: 'Password is required' });
    } else {
      if (state().password.length < 8) {
        setError({
          ...error(),
          password: 'Must contain at least 8 character(s).',
        });
      }
    }

    if (
      error().email === '' &&
      error().password === '' &&
      error().lastname === '' &&
      error().firstname === ''
    ) {
      const data = await createUserAccount(
        state().email,
        state().password,
        state().firstname,
        state().lastname,
        state().is_subscribed,
      );

      if (data && data.createCustomerV2 !== undefined) {
        toast.success('Account created successfully!');
        window.open('/my-account/', '_self');
      } else {
        if (
          data ===
          'Minimum of different classes of characters in password is 3. Classes of characters: Lower Case, Upper Case, Digits, Special Characters.'
        ) {
          setError({
            ...error(),
            password: data,
          });
        } else {
          toast.error(data);
        }
      }
    }
  };

  return (
    <>
      <form
        class={css({
          maxWidth: $theme.sizing.maxWidth,
          margin: '0 auto',
          paddingTop: $theme.sizing.scale800,
          paddingBottom: $theme.sizing.scale800,
        })}
      >
        <div
          class={css({
            backgroundColor: cssVar('login-background-color', '#FEFEFE'),
            padding: $theme.sizing.scale800,
            maxWidth: '420px',
            margin: '0 auto',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
          })}
        >
          <h1
            class={css({
              marginLeft: 0,
              marginRight: 0,
              marginBottom: $theme.sizing.scale800,
            })}
          >
            Sign up
          </h1>

          <div class={css({ width: '100%' })}>
            <FormControl for="firstname" label="First name">
              <Input
                name="firstname"
                value={state().firstname}
                error={error().firstname}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    firstname: (prev.target as HTMLInputElement).value,
                  });
                }}
                type="text"
                id="firstname"
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
          </div>
          <div class={css({ width: '100%' })}>
            <FormControl for="lastname" label="Last name">
              <Input
                name="lastname"
                error={error().lastname}
                value={state().lastname}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    lastname: (prev.target as HTMLInputElement).value,
                  });
                }}
                type="text"
                id="lastname"
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
          </div>
          <div class={css({ width: '100%' })}>
            <FormControl for="emailOrPhone" label="Email">
              <Input
                name="email"
                value={state().email}
                error={error().email}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    email: (prev.target as HTMLInputElement).value,
                  });
                }}
                type="text"
                id="emailOrPhone"
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
          </div>
          <div
            class={css({ width: '100%', marginTop: $theme.sizing.scale600 })}
          >
            <FormControl for="password" label="Password">
              <Input
                type={showPassword() ? 'text' : 'password'}
                id="password"
                name="password"
                error={error().password}
                value={state().password}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    password: (prev.target as HTMLInputElement).value,
                  });
                }}
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
            <div
              class={css({
                display: 'flex',
                marginTop: $theme.sizing.scale200,
                alignItems: 'center',
                ':hover': { cursor: 'pointer' },
              })}
              onClick={() => {
                setShowPassword(!showPassword());
              }}
            >
              <Input
                type="checkbox"
                id="is_subscribed"
                name="is_subscribed"
                checked={showPassword()}
                width="100%"
              />
              <div
                class={css({
                  color: '#000',
                  fontWeight: '500',
                  fontSize: '15px',
                  marginLeft: $theme.sizing.scale300,
                })}
              >
                Show password
              </div>
            </div>
          </div>

          <div class={css({ paddingTop: '8px' })} />

          <div
            class={css({
              width: '100%',
              marginTop: $theme.sizing.scale600,
              ':hover': { cursor: 'pointer' },
            })}
            onClick={() => {
              setState({
                ...state(),
                is_subscribed: !state().is_subscribed,
              });
            }}
          >
            <FormControl for="is_subscribed" label="">
              <div
                class={css({
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                })}
              >
                <Input
                  type="checkbox"
                  id="is_subscribed"
                  name="is_subscribed"
                  checked={state().is_subscribed}
                  width="100%"
                />
                <div
                  class={css({
                    color: '#000',
                    fontWeight: '500',
                    fontSize: '15px',
                    marginLeft: $theme.sizing.scale300,
                  })}
                >
                  Subscribe to news and updates
                </div>
              </div>
            </FormControl>
          </div>

          <div
            onClick={() => {
              createUser();
            }}
            class={css({
              marginTop: $theme.sizing.scale900,
              width: '100%',
              textAlign: 'center',
              backgroundColor: '#118b44',
              paddingTop: '12px',
              paddingBottom: '12px',
              color: '#fff',
              fontWeight: 'bold',
              fontSize: '18px',
              borderRadius: '4px',
              ':hover': { cursor: 'pointer' },
            })}
          >
            Create an account
          </div>

          <div
            onClick={() => {
              navigate('/account/login');
            }}
            class={css({
              marginTop: $theme.sizing.scale900,
              width: '100%',
              paddingTop: '12px',
              paddingBottom: '12px',
              borderRadius: '4px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              ':hover': { cursor: 'pointer' },
            })}
          >
            <div
              class={css({
                color: '#000',
                fontWeight: '500',
                fontSize: '15px',
              })}
            >
              Already have a account?
            </div>
            <div
              class={css({
                color: '#118b44',
                fontWeight: '700',
                fontSize: '15px',
              })}
            >
              &nbsp;Log in
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
