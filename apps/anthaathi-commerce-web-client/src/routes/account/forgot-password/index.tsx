import { useStyletron } from '@anthaathi/solid-styletron';
import { useCssToken } from '~/Features/Core/Hooks/useCssToken';
import { FormControl } from '~/Features/Core/Components/FormControl';
import { Input } from '~/Features/Core/Components/Input';
import { Button, Kind, Size } from '~/Features/Core/Components/Button';
import { Link, useNavigate } from '@solidjs/router';
import { createSignal } from 'solid-js';
import { useAuth } from '~/hooks/useAuth';
import { validateEmail } from '../login';
import toast from 'solid-toast';

export default function ForgetPassword() {
  const [css, $theme] = useStyletron();
  const cssVar = useCssToken();
  const [state, setState] = createSignal<{ email: string }>({
    email: '',
  });
  const [error, setError] = createSignal<{ email: string }>({
    email: '',
  });
  const navigate = useNavigate();
  const { resetPassword } = useAuth();

  const resetPasswordMethod = async () => {
    setError({ ...error(), email: '', passowrd: '' });
    if (state().email === '') {
      setError({ ...error(), email: 'Email is required' });
    } else {
      if (!validateEmail(state().email)) {
        setError({ ...error(), email: 'Enter correct email' });
      }
    }
    if (error().email === '') {
      const data = await resetPassword(state().email);
      if (data && data.requestPasswordResetEmail !== undefined) {
        toast.success('Reset password link sent!');
      } else {
        toast.error(data);
      }
    }
  };
  
  return (
    <>
      <form
        class={css({
          maxWidth: $theme.sizing.maxWidth,
          margin: '0 auto',
          paddingTop: $theme.sizing.scale800,
          paddingBottom: $theme.sizing.scale800,
        })}
      >
        <div
          class={css({
            backgroundColor: cssVar('login-background-color', '#FEFEFE'),
            padding: $theme.sizing.scale800,
            maxWidth: '420px',
            margin: '0 auto',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
          })}
        >
          <h1
            class={css({
              marginLeft: 0,
              marginRight: 0,
              marginBottom: $theme.sizing.scale800,
            })}
          >
            Reset your password
          </h1>

          <div class={css({ width: '100%' })}>
            <FormControl for="emailOrPhone" label="Email">
              <Input
                name="email"
                value={state().email}
                onInput={(prev) => {
                  setState({
                    ...state(),
                    email: (prev.target as HTMLInputElement).value,
                  });
                }}
                type="text"
                id="emailOrPhone"
                width="100%"
                $overrides={{
                  Root: { style: { width: '100%' } },
                }}
              />
            </FormControl>
          </div>

          <div class={css({ paddingTop: '12px' })} />

          <div
            onClick={() => {
              resetPasswordMethod();
            }}
            class={css({
              marginTop: $theme.sizing.scale900,
              width: '100%',
              textAlign: 'center',
              backgroundColor: '#118b44',
              paddingTop: '12px',
              paddingBottom: '12px',
              color: '#fff',
              fontWeight: 'bold',
              fontSize: '18px',
              borderRadius: '4px',
              ':hover': { cursor: 'pointer' },
            })}
          >
            Send Link
          </div>

          <div
            onClick={() => {
              navigate('/account/login');
            }}
            class={css({
              marginTop: $theme.sizing.scale900,
              width: '100%',
              paddingTop: '12px',
              paddingBottom: '12px',
              borderRadius: '4px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              ':hover': { cursor: 'pointer' },
            })}
          >
            <div
              class={css({
                color: '#000',
                fontWeight: '500',
                fontSize: '15px',
              })}
            >
              Back to?
            </div>
            <div
              class={css({
                color: '#118b44',
                fontWeight: '700',
                fontSize: '15px',
              })}
            >
              &nbsp;Log in
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
