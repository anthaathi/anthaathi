import { createEffect } from 'solid-js';

export default function () {
  createEffect(() => {
    window.location.reload();
  });

  return <></>;
}
