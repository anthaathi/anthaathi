import React from 'react';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Platform, TouchableOpacity} from 'react-native';
import HomePage from '../HomePage';
import ProfilePage from '../ProfilePage';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useIntl} from 'react-intl';
import {useTheme} from 'react-native-paper';
import Header from '../../features/containers/Core/components/Header';

const Tab = createBottomTabNavigator<RootStackParamList>();

export default function MainPage(
  _props: NativeStackScreenProps<RootStackParamList, 'Dashboard'>,
) {
  const theme = useTheme();
  const intl = useIntl();
  return (
    <>
      <Header
        languageIcon={true}
        mailIcon={false}
        logoImage={
          'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/everyday_1_256x256.png?v=1662529180'
        }
      />
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarActiveTintColor: theme.colors.titleTextColor,
          tabBarHideOnKeyboard: true,
          tabBarActiveBackgroundColor: '#F1F9F4',
          tabBarStyle: [
            Platform.OS === 'android' && {
              height: 56,
            },
          ],
          tabBarLabelStyle: {
            fontWeight: '500',
            fontSize: 11,
            paddingBottom: 5,
          },
        }}>
        <Tab.Screen
          name="HomePage"
          component={HomePage}
          options={{
            tabBarLabel: intl.formatMessage({
              defaultMessage: 'Home',
            }),
            tabBarIcon: ({color}: {color: any}) => (
              <SimpleLineIcons name="home" color={color} size={20} />
            ),
            tabBarButton: (props: any) => <TouchableOpacity {...props} />,
          }}
        />
        <Tab.Screen
          name="ProfilePage"
          component={ProfilePage}
          options={{
            tabBarLabel: intl.formatMessage({
              defaultMessage: 'Account',
            }),
            tabBarIcon: ({color}: {color: any}) => (
              <SimpleLineIcons name="user" color={color} size={20} />
            ),
            tabBarButton: (props: any) => <TouchableOpacity {...props} />,
          }}
        />
      </Tab.Navigator>
    </>
  );
}
