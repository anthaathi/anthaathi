import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import NotDelivered from '../MainTopPage/NotDelivered';
import Delivered from '../MainTopPage/Delivered';
import {Colors} from 'react-native-paper';
import DeliveryStatusCard from '../../features/containers/HomePage/components/DeliveryStatusCard';
import ReturnOrder from '../MainTopPage/ReturnOrder';

const Tab = createMaterialTopTabNavigator();

const MyTabs = () => {
  return (
    <>
      <DeliveryStatusCard />
      <Tab.Navigator
        style={{marginVertical: 5}}
        screenOptions={{
          tabBarItemStyle: {
            height: 34,
            minHeight: 30,
            padding: 0,
            marginVertical: 4,
            marginHorizontal: 6,
            borderRadius: 50,
          },
          tabBarStyle: {
            padding: 0,
            margin: 0,
            height: 42,
          },
          tabBarLabelStyle: {
            color: Colors.green900,
            textTransform: 'none',
            fontWeight: '500',
          },
          tabBarIndicatorStyle: {
            backgroundColor: Colors.green400,
          },
          lazy: true,
        }}>
        <Tab.Screen
          name="NotDelivered"
          component={NotDelivered}
          options={{tabBarLabel: 'Not Delivered'}}
        />
        <Tab.Screen
          name="Delivered"
          component={Delivered}
          options={{tabBarLabel: 'Delivered'}}
        />
        <Tab.Screen
          name="ReturnOrder"
          component={ReturnOrder}
          options={{tabBarLabel: 'Return Order'}}
        />
      </Tab.Navigator>
    </>
  );
};

export default MyTabs;
