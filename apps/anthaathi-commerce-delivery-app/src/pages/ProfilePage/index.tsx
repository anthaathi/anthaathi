/* eslint-disable react/no-unstable-nested-components */
import {View} from 'react-native';
import React from 'react';
import {Divider, List} from 'react-native-paper';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';

const ProfilePage = (
  props: NativeStackScreenProps<RootStackParamList, 'ProfilePage'>,
) => {
  return (
    <View style={{backgroundColor: 'white', flex: 1, paddingHorizontal: 5}}>
      <ListItemData
        title="Personal details"
        iconName="account"
        onPress={() => {
          props.navigation.navigate('EditProfile');
        }}
      />
      <ListItemData title="My reviews" iconName="star" />
      <ListItemData title="Settings" iconName="cog" />
    </View>
  );
};

const ListItemData = ({
  title,
  subtitle,
  iconName,
  onPress,
}: {
  title: string;
  subtitle?: string;
  iconName: string;
  onPress?: () => void;
}) => {
  return (
    <>
      <List.Item
        onPress={onPress}
        style={{paddingVertical: 15}}
        title={title}
        description={subtitle}
        left={props => <List.Icon {...props} icon={iconName} />}
        right={props => <List.Icon {...props} icon="chevron-right" />}
      />
      <Divider />
    </>
  );
};
export default ProfilePage;
