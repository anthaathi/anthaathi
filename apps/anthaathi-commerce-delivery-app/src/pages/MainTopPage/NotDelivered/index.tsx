import {View, ScrollView} from 'react-native';
import React from 'react';
import {RootStackParamList} from '../../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import CartCard from '../../../features/containers/HomePage/components/CartCard';

const NotDelivered = (
  props: NativeStackScreenProps<RootStackParamList, 'NotDelivered'>,
) => {
  return (
    <View>
      <ScrollView contentContainerStyle={{flexGrow: 1, paddingTop: 10}}>
        {[...Array(10)].map((_, index) => (
          <CartCard
            key={index.toString()}
            title="Order #123"
            statusTitle="In Transit"
            statusIcon="basket"
            deliveryDate="Sun, 17 Jul 2022"
            deliveryAddress="13C, UAE"
            noOfItems="2 Items"
            handlePress={() => {
              props.navigation.navigate('OrderDetailsPage');
            }}
            orderStatus={true}
            imageList={[
              'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/a-papaya-is-surrounded-by-fruit-on-yellow-background_900x.jpg?v=1653586970',
              'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh-vegetables-flatlay_900x.jpg?v=1653677616',
            ]}
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default NotDelivered;
