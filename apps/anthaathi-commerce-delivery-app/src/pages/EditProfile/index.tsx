import {View, ScrollView} from 'react-native';
import React from 'react';
import Header from '../../features/containers/Core/components/Header';
import CMSTextInput from '../../features/containers/Core/components/TextInput';
import {Button} from 'react-native-paper';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';

const EditProfile = (
  props: NativeStackScreenProps<RootStackParamList, 'EditProfile'>,
) => {
  return (
    <View style={{backgroundColor: 'white', flex: 1}}>
      <Header
        leftIcon={'arrow-left'}
        leftOnPress={() => props.navigation.goBack()}
        title={'User Details'}
      />

      <ScrollView contentContainerStyle={{paddingHorizontal: 5, flex: 1}}>
        <CMSTextInput label={'Name'} />
        <CMSTextInput label={'Email'} />
        <CMSTextInput label={'Mobile'} />
        <CMSTextInput label={'Alternate Mobile'} />
        <CMSTextInput label={'Name'} />
        <Button
          mode="contained"
          style={{
            marginVertical: 10,
            marginHorizontal: '4%',
            borderRadius: 4,
            width: '92%',
            position: 'absolute',
            bottom: 0,
          }}
          labelStyle={{
            paddingVertical: 5,
          }}
          onPress={() => props.navigation.goBack()}>
          Save Changes
        </Button>
      </ScrollView>
    </View>
  );
};

export default EditProfile;
