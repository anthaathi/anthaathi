export type RootStackParamList = {
  SignIn: undefined | {};
  Dashboard: undefined | {};
  HomePage: {} | undefined;
  ProfilePage: {} | undefined;
  ResetPassword: {} | undefined;
  EditProfile: {} | undefined;
  OrderDetailsPage: {} | undefined;
  Delivered: {} | undefined;
  NotDelivered: {} | undefined;
  PickerOrderDetailsPage: {} | undefined;
  ReturnItemPage: {} | undefined;
  ReturnOrder: {} | undefined;
};
