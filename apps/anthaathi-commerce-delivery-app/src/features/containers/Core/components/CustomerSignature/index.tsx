import React from 'react';
import {Text, Modal, StyleSheet, View} from 'react-native';
import {useTheme, Button} from 'react-native-paper';
import SignatureScreen, {SignatureViewRef} from 'react-native-signature-canvas';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const CustomerSignature = ({
  isVisible,
  getSign,
  // descriptionText = '',
  onRequestClose,
}: {
  isVisible: boolean;
  getSign: (sign: string) => void;
  descriptionText?: string;
  onRequestClose: () => void;
}) => {
  const theme = useTheme();
  const ref = React.useRef<SignatureViewRef>();

  const handleOK = (signature1: any) => {
    console.log(signature1);
    getSign(signature1);
  };

  const handleClear = () => {
    ref.current.clearSignature();
  };

  const handleConfirm = () => {
    ref.current.readSignature();
    onRequestClose();
  };

  const style = `.m-signature-pad--footer {display: none; margin: 0px;}
  body, html {height: 100%, width: 100%}
  `;

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        visible={isVisible}
        transparent={true}
        onRequestClose={onRequestClose}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={[styles.row, {marginBottom: 10}]}>
              <Text
                style={{
                  color: theme.colors.titleTextColor,
                  fontSize: 16,
                  fontWeight: '600',
                  marginBottom: 5,
                }}>
                Customer Sign
              </Text>
              <FontAwesome
                name="close"
                size={20}
                style={{paddingLeft: 10, color: '#000'}}
                onPress={onRequestClose}
              />
            </View>
            <View style={styles.container}>
              <SignatureScreen ref={ref} onOK={handleOK} webStyle={style} />
              <View style={styles.row}>
                <Button
                  mode="contained"
                  style={{
                    borderRadius: 4,
                    paddingHorizontal: 12,
                    backgroundColor: '#EAEBEB',
                  }}
                  labelStyle={{
                    paddingVertical: 5,
                    color: '#000',
                  }}
                  onPress={handleClear}>
                  Clear
                </Button>
                <Button
                  mode="contained"
                  style={{
                    borderRadius: 4,
                    paddingHorizontal: 12,
                  }}
                  labelStyle={{
                    paddingVertical: 5,
                  }}
                  onPress={handleConfirm}>
                  Confirm
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default CustomerSignature;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // height: 250,
    padding: 10,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    paddingTop: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  modalView: {
    height: '100%',
    width: '100%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 4,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 2,
  },
});
