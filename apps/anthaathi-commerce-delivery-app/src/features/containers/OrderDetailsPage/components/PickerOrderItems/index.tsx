import {Image, View, Text as RNText, Pressable} from 'react-native';
import React from 'react';
import {Divider, Text, useTheme} from 'react-native-paper';
import {useIntl} from 'react-intl';
import {useResponsiveValue} from '../../../../utils/useReponsiveValue';
import {ItemProps, OrderedItemProps} from '../OrderedItems';
import CheckBox from '../../../Core/components/CheckBox';

const PickerOrderItems = (props: OrderedItemProps) => {
  const theme = useTheme();
  const itemHeight = useResponsiveValue([104, 180, 290, 330]);
  const itemWidth = useResponsiveValue([104, 180, 280, 320]);
  const [checkedItems, setCheckedItems] = React.useState<number[]>([]);

  return (
    <View
      style={{
        marginHorizontal: 10,
      }}
      testID="pickerOrderedItems">
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <RNText
          style={{
            color: theme.colors.titleTextColor,
            fontSize: 16,
            fontWeight: '600',
            marginBottom: 5,
          }}>
          {props.title}
        </RNText>
      </View>

      <View
        style={{
          backgroundColor: '#fff',
          paddingVertical: 5,
          paddingHorizontal: 5,
          borderColor: theme.colors.cardBorderColor,
          borderWidth: 1,
          borderRadius: 4,
        }}>
        {props.items.map((item, index) => {
          return (
            <ItemRenderer
              key={item.key}
              item={item}
              itemHeight={itemHeight}
              itemWidth={itemWidth}
              handlePressChecked={() => {
                if (checkedItems.includes(item.id)) {
                  let copyArray = [...checkedItems];
                  copyArray.splice(copyArray.indexOf(item.id), 1);
                  setCheckedItems(copyArray);
                } else {
                  setCheckedItems(oldArray => [...oldArray, item.id]);
                }
              }}
              divider={index !== props.items.length - 1}
              checked={checkedItems.includes(item.id)}
            />
          );
        })}
      </View>
    </View>
  );
};

const ItemRenderer = ({
  item,
  itemHeight,
  itemWidth,
  divider,
  handlePressChecked,
  checked,
}: {
  item: ItemProps;
  itemHeight: number;
  itemWidth: number;
  divider: boolean;
  checked: boolean;
  handlePressChecked: () => void;
}) => {
  const theme = useTheme();
  const intl = useIntl();
  return (
    <Pressable onPress={handlePressChecked}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <View style={{marginTop: itemHeight / 4}}>
          <CheckBox
            color="#008d3e"
            status={checked}
            onPress={handlePressChecked}
          />
        </View>

        <View style={{flexDirection: 'row', marginLeft: 5}}>
          <Image
            testID="basketProductImage"
            source={{uri: item.image}}
            style={{height: itemHeight, width: itemWidth}}
          />

          <View
            style={{
              marginHorizontal: 15,
            }}>
            <Text
              testID="productName"
              variant="titleLarge"
              style={{
                marginBottom: 5,
                fontSize: 14,
                color: theme.colors.titleTextColor,
                fontWeight: '900',
              }}>
              {item.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 5,
              }}>
              <Text
                variant="titleLarge"
                style={{
                  fontSize: 14,
                  color: theme.colors.titleTextColor,
                  fontWeight: '900',
                }}>
                {intl.formatMessage({defaultMessage: 'Quantity'}) + ' :'}
              </Text>

              <Text
                testID="productPrice"
                variant="titleLarge"
                style={{
                  fontSize: 14,
                  color: theme.colors.greenTextColor,
                  fontWeight: '700',
                  marginLeft: 5,
                }}>
                {item.numberOfItems}
              </Text>
            </View>

            <Text
              variant="titleLarge"
              style={{
                fontSize: 13,
                color: theme.colors.titleTextColor,
                fontWeight: '600',
                marginBottom: 5,
              }}>
              {intl.formatNumber(item.price, {
                style: 'currency',
                currency: item.currency,
              }) +
                ' / ' +
                intl.formatMessage({defaultMessage: 'Piece'})}
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 5,
              }}>
              <Text
                variant="titleLarge"
                style={{
                  fontSize: 14,
                  color: theme.colors.titleTextColor,
                  fontWeight: '900',
                }}>
                {intl.formatMessage({defaultMessage: 'Total'}) + ' :'}
              </Text>

              <Text
                testID="productPrice"
                variant="titleLarge"
                style={{
                  fontSize: 14,
                  color: theme.colors.greenTextColor,
                  fontWeight: '700',
                  marginLeft: 5,
                }}>
                {intl.formatNumber(item.price * item.numberOfItems, {
                  style: 'currency',
                  currency: item.currency,
                })}
              </Text>
            </View>
          </View>
        </View>
      </View>
      {divider && <Divider style={{marginVertical: 10}} />}
    </Pressable>
  );
};

export default PickerOrderItems;
