import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {Card, useTheme} from 'react-native-paper';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const DeliveryStatusCard = () => {
  const [open, setOpen] = React.useState(true);
  const theme = useTheme();
  return (
    <View>
      <Card
        style={{
          marginHorizontal: 10,
          borderColor: theme.colors.cardBorderColor,
          borderWidth: 1,
          borderRadius: 4,
          marginVertical: 5,
        }}>
        <Card.Content>
          <View>
            <TouchableOpacity onPress={() => setOpen(!open)}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: open ? 10 : 0,
                }}>
                <Text
                  style={{
                    color: theme.colors.titleTextColor,
                    fontSize: 16,
                    fontWeight: '700',
                  }}>
                  Welcome Omkar
                </Text>
                <FontAwesome5
                  style={{marginLeft: 10}}
                  name={open ? 'chevron-up' : 'chevron-down'}
                  size={20}
                />
              </View>
            </TouchableOpacity>
            {open && (
              <>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <ItemRenderer
                    title="Total"
                    subtitle={20}
                    fontColor="#0B132B"
                  />
                  <ItemRenderer
                    title="Delivered"
                    subtitle={20}
                    fontColor="#008D3E"
                  />
                </View>
                <View
                  style={{
                    marginTop: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <ItemRenderer
                    title="Not Delivered"
                    subtitle={20}
                    fontColor="#E3170A"
                  />
                  <ItemRenderer
                    title="Today status"
                    subtitle={20}
                    fontColor="#FBAF00"
                  />
                </View>
              </>
            )}
          </View>
        </Card.Content>
      </Card>
    </View>
  );
};

const ItemRenderer = ({
  title,
  subtitle,
  fontColor,
}: {
  title: string;
  subtitle: number;
  fontColor?: any;
}) => {
  const theme = useTheme();
  return (
    <View style={{flexDirection: 'row'}}>
      <Text
        style={{
          color: theme.colors.titleTextColor,
          fontSize: 14,
          fontWeight: '600',
        }}>
        {title + ': '}
      </Text>
      <Text
        style={[
          {
            color: theme.colors.titleTextColor,
            fontSize: 14,
            fontWeight: '700',
          },
          fontColor && {color: fontColor},
        ]}>
        {subtitle}
      </Text>
    </View>
  );
};

export default DeliveryStatusCard;
