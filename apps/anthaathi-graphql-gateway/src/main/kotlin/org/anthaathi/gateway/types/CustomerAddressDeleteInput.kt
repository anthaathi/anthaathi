package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Input

@Input
class CustomerAddressDeleteInput {
    var customerAccessToken: String? = null

    @Id
    var id: String? = null
}

