package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class ShippingRate {
    @NonNull
    var handle: String? = null

    var price: MoneyV2? = null

    var title: String? = null
}
