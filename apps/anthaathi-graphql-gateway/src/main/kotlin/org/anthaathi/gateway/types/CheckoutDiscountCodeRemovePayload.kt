package org.anthaathi.gateway.types

class CheckoutDiscountCodeRemovePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
