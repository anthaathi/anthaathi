package org.anthaathi.commerce.checkout.utils

import java.net.URI
import java.util.*

class RelayUtils {
    class GlobalID(
        val id: String? = null,
        val type: String? = null,
        val queryParams: Map<String, MutableList<String>>? = null
    )

    companion object {
        private val EXCEPTION = Exception("Invalid ID")

        fun toGlobalID(globalId: String, assertType: String? = null): GlobalID {
            val decoder = Base64.getDecoder()
            println(globalId)
            val decoded = String(decoder.decode(globalId))

            val decodedURI = URI(decoded)

            if (decodedURI.scheme != "gid") {
                println("gid")
                throw Exception("Invalid ID")
            }

            if (decodedURI.host != "anthaathi") {
                println("anthaathi")
                throw Exception("Invalid ID")
            }

            val splitPath = decodedURI.path.split("/")

            if (assertType != null && assertType != splitPath[1]) {
                println("Invalid assert type")
                println(globalId)
                println(splitPath[1])
                println(assertType)
                println(splitPath[2])
                throw EXCEPTION
            }

            return GlobalID(
                id = splitPath[2],
                type = splitPath[1],
                queryParams = ParseURLParameter.getUrlParameters(decoded),
            )
        }

        fun fromGlobalID(input: GlobalID): String? {
            val encoder = Base64.getEncoder()

            val extraParams = if (input.queryParams != null) {
                "?" + input.queryParams.map { queryParam ->
                    return@map queryParam.value.joinToString("&") {
                        queryParam.key + "=" + it
                    }
                }.joinToString("&")
            } else {
                ""
            }

            return encoder.encodeToString("gid://anthaathi/${ input.type }/${ input.id }${ extraParams }".toByteArray())
        }
    }
}
