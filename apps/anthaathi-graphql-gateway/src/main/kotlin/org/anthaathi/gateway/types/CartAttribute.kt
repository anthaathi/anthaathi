package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Description

@Description("An array of key-value pairs that contains additional information about the cart.")
open class CartAttribute {
    open var key: String? = null
    open var value: String? = null
}
