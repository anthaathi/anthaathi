package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class CheckoutUserError : DisplayableError {
    override var field: List<String>? = null
    @NonNull
    override var message: String? = null
}
