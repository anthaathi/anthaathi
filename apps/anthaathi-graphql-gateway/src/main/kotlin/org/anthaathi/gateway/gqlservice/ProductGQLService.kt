package org.anthaathi.gateway.gqlservice

import graphql.relay.Connection
import graphql.relay.SimpleListConnection
import org.anthaathi.gateway.types.Image
import org.anthaathi.gateway.types.Product
import org.anthaathi.gateway.types.ProductVariant
import org.eclipse.microprofile.graphql.GraphQLApi
import org.eclipse.microprofile.graphql.NonNull
import org.eclipse.microprofile.graphql.Query
import org.eclipse.microprofile.graphql.Source

@GraphQLApi
class ProductGQLService {
    @Query
    fun product(handle: String?, id: String?): Product? {
        TODO()
    }

    @NonNull
    fun variants(@Source product: Product, first: Int?, last: Int?, before: String?, after: String?): Connection<ProductVariant> {
        TODO()
    }

    fun images(@Source product: Product, first: Int?, last: Int?, before: String?, after: String?): Connection<Image> {
        TODO()
    }
}
