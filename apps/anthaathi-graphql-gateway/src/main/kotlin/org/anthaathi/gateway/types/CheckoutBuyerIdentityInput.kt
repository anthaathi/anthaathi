package org.anthaathi.gateway.types

import org.anthaathi.common.CheckoutBuyerIdentityInput as CheckoutBuyerIdentityInputGRPC
import org.eclipse.microprofile.graphql.Input

@Input
class CheckoutBuyerIdentityInput : CartBuyerIdentityInput() {
    fun toGRPCObject(customerId: String): CheckoutBuyerIdentityInputGRPC {
        return CheckoutBuyerIdentityInputGRPC.newBuilder()
            .setEmail(this.email)
            .setCountryCode(this.countryCode)
            .setCustomerId(customerId)
            .build()
    }
}


