package org.anthaathi.gateway.gqlservice

import graphql.relay.Connection
import io.quarkus.grpc.GrpcClient
import org.anthaathi.common.CheckoutServiceGRPC
import org.anthaathi.gateway.types.*
import org.eclipse.microprofile.graphql.*

@GraphQLApi
class CheckoutGQLService(
    @GrpcClient("checkout")
    var client: CheckoutServiceGRPC,
) {
    @Mutation
    fun checkoutCreate(input: CheckoutCreateInput): CheckoutCreatePayload {
        return TODO("")
    }

    @Mutation
    fun checkoutLineItemsUpdate(@Id @NonNull checkoutId: String, @NonNull lineItems: List<@NonNull CheckoutLineItemUpdateInput>): CheckoutLineItemsUpdatePayload {
        TODO("")
    }

    @Mutation
    fun checkoutLineItemsReplace(@Id @NonNull checkoutId: String, @NonNull lineItems: CheckoutLineItemInput): CheckoutLineItemsReplacePayload {
        TODO("")
    }

    @Mutation
    fun checkoutShippingAddressUpdate(@Id @NonNull checkoutId: String, @NonNull shippingAddress: MailingAddressInput): CheckoutShippingAddressUpdatePayload {
        TODO("")
    }

    @Mutation
    fun checkoutLineItemsRemove(@Id @NonNull checkoutId: String, @NonNull @Id lineItemIds: List<String>): CheckoutLineItemsRemovePayload {
        TODO("")
    }

    @Mutation
    fun checkoutEmailUpdate(@Id @NonNull checkoutId: String, @NonNull email: String): CheckoutEmailUpdatePayload {
        TODO("")
    }

    @Mutation
    fun checkoutDiscountCodeApply(@Id @NonNull checkoutId: String, @NonNull discountCode: String): CheckoutDiscountCodeApplyPayload {
        TODO("")
    }

    @Mutation
    fun checkoutDiscountCodeRemove(@Id @NonNull checkoutId: String): CheckoutDiscountCodeRemovePayload {
        TODO("")
    }

    @Mutation
    fun checkoutCustomerAssociate(@Id @NonNull checkoutId: String): CheckoutCustomerAssociatePayload {
        TODO("")
    }

    @Query
    fun paymentGateways(): Connection<PaymentGateway> {
        TODO("")
    }
}
