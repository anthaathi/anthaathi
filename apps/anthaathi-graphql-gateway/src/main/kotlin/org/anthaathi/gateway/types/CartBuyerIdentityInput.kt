package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Input

@Input
open class CartBuyerIdentityInput(
    var customerAccessToken: String? = null,
    var countryCode: String? = null,
    var phone: String? = null,
    var email: String? = null,
)

