package org.anthaathi.gateway.types

import org.anthaathi.gateway.types.MailingAddressInput
import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.NonNull

@Input
class CustomerAddressCreateInput {
    @NonNull
    var customerAccessToken: String? = null
    @NonNull
    var address: MailingAddressInput? = null
}
