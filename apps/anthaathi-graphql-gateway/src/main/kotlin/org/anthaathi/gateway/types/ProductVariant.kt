package org.anthaathi.gateway.types

import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.NonNull
import java.util.*
import org.anthaathi.common.ProductVariant as ProductVariantGRPC

class ProductVariant : Node {
    private var _id: UUID? = null

    var rawId
        get() = _id
        set(value) {
            _id = value
        }

    @get:Id
    @set:Id
    override var id: String
        get() {
            return RelayUtils.fromGlobalID(
                RelayUtils.GlobalID(
                    id = _id.toString(),
                    type = "CartLineItem",
                ))!!
        }

        set(value) {
            val result = RelayUtils.toGlobalID(value, "ProductVariant")
            _id = UUID.fromString(result.id)
        }

    var title: String? = null
    var sku: String? = null
    var taxable: Boolean? = null
    var position: Int? = null
    var packaging: String? = null
    var weightUnit: String? = null
    var origin: String? = null
    var purchaseNote: String? = null
    var stock = 0L

    var priceV2: MoneyV2? = null;

    var compareAtPriceV2: MoneyV2? = null;

    @NonNull
    var product: Product? = null

    companion object {
        fun fromProductVariantGRPC(input: ProductVariantGRPC): ProductVariant {
            val returnValue = ProductVariant()

            returnValue.rawId = UUID.fromString(input.id)
            returnValue.title = input.title
            returnValue.origin = input.origin
            returnValue.packaging = input.packaging
            returnValue.origin = input.origin
            returnValue.position = input.position
            returnValue.purchaseNote = input.purchaseNote
            returnValue.sku = input.sku
            returnValue.taxable = input.taxable
            returnValue.weightUnit = input.weightUnit
            returnValue.stock = input.stock

            return returnValue
        }
    }
}
