package org.anthaathi.gateway.types

enum class DiscountApplicationAllocationMethod {
    ACROSS,
    EACH,
}
