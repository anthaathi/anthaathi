package org.anthaathi.gateway.types

import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.anthaathi.common.MailingAddress
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Ignore
import org.eclipse.microprofile.graphql.Input
import java.util.*

@Input
class MailingAddress: MailingAddressInput(), Node {
    companion object {
        const val type = "MailingAddress"
    }

    @Ignore
    var rawId: UUID? = null

    @get:Id
    @set:Id
    override var id: String
        get() = RelayUtils.fromGlobalID(RelayUtils.GlobalID(rawId!!.toString(), type))!!
        set(value) {
            val result = RelayUtils.toGlobalID(value, type)
            rawId = UUID.fromString(result.id)
        }

    override fun toGRPCType(): MailingAddress.Builder {
        return super.toGRPCType()
            .setId(this.rawId.toString())
    }
}
