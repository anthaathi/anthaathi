package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class MoneyV2 : PricingValue {
    @NonNull
    var amount: Long? = null

    @NonNull
    var currencyCode: CurrencyCode? = null
}
