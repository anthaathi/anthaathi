package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class Attribute {
    @NonNull
    var key: String? = null
    var value: String? = null
}
