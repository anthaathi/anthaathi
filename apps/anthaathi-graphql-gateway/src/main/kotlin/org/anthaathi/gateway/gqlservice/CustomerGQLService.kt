package org.anthaathi.gateway.gqlservice

import org.anthaathi.gateway.types.*
import org.eclipse.microprofile.graphql.GraphQLApi
import org.eclipse.microprofile.graphql.Mutation
import org.eclipse.microprofile.graphql.NonNull
import org.eclipse.microprofile.graphql.Query

@GraphQLApi
class CustomerGQLService {
    @Query
    fun customer(@NonNull customerAccessToken: String): Customer {
        TODO("Customer")
    }

    @Mutation
    fun customerAddressCreate(@NonNull input: CustomerAddressCreateInput): CustomerAddressCreateResponse {
        TODO("CustomerAddressCreate")
    }

    @Mutation
    fun customerAddressDelete(@NonNull input: CustomerAddressDeleteInput): CustomerAddressDeleteResponse {
        TODO("Delete")
    }

    @Mutation
    fun customerAddressUpdate(@NonNull input: CustomerAddressUpdateInput): CustomerAddressUpdateResponse {
        TODO("")
    }

    @Mutation
    fun customerDefaultAddressUpdate(@NonNull input: CustomerDefaultAddressUpdateInput): CustomerDefaultAddressUpdateResponse {
        TODO("")
    }
}

