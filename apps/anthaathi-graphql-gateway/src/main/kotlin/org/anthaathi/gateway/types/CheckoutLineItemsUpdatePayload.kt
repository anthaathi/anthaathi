package org.anthaathi.gateway.types

class CheckoutLineItemsUpdatePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
