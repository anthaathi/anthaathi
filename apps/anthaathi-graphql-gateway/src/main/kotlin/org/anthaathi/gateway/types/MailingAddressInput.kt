package org.anthaathi.gateway.types

import org.anthaathi.common.MailingAddress
import org.eclipse.microprofile.graphql.NonNull
import org.anthaathi.common.MailingAddress as MailingAddressGRPC

open class MailingAddressInput {
    @NonNull
    var address1: String? = null
    @NonNull
    var address2: String? = null
    @NonNull
    var city: String? = null

    var company: String? = null

    @NonNull
    var country: String? = null
    @NonNull
    var firstName: String? = null

    @NonNull
    var lastName: String? = null

    @NonNull
    var phone: String? = null

    @NonNull
    var province: String? = null

    var provinceCode: String? = null

    @NonNull
    var zip: String? = null

    open fun toGRPCType(): MailingAddress.Builder {
        return MailingAddressGRPC.newBuilder()
            .setAddress1(this.address1)
            .setAddress2(this.address2)
            .setCity(this.city)
            .setCompany(this.company)
            .setCountry(this.country)
            .setFirstName(this.firstName)
            .setLastName(this.lastName)
            .setPhone(this.phone)
            .setProvince(this.province)
            .setZip(this.zip)
            .setProvinceCode(this.provinceCode)
    }
}
