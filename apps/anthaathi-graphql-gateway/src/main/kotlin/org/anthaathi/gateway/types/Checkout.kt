package org.anthaathi.gateway.types

import graphql.relay.Connection
import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.eclipse.microprofile.graphql.Description
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Ignore
import org.eclipse.microprofile.graphql.NonNull
import java.util.*

class Checkout : Node {
    companion object {
        const val type = "Checkout"
    }

    @Ignore
    var rawId: UUID? = null

    @get:Id
    @set:Id
    override var id: String
        get() = RelayUtils.fromGlobalID(RelayUtils.GlobalID(rawId!!.toString(), type))!!
        set(value) {
            val result = RelayUtils.toGlobalID(value, type)
            rawId = UUID.fromString(result.id)
        }

    @NonNull
    var appliedGiftCards: List<@NonNull AppliedGiftCard>? = null

    var availableShippingRates: AvailableShippingRates? = null

    @NonNull
    var buyerIdentity: CheckoutBuyerIdentity? = null

    var completedAt: Date? = null

    @NonNull
    var createdAt: Date? = null

    @NonNull
    var currencyCode: CurrencyCode? = null

    var email: String? = null

    @NonNull
    var customAttributes: List<@NonNull Attribute>? = null

    @NonNull
    var lineItemsSubtotalPrice: MoneyV2? = null

    var lineItems: Connection<CheckoutLineItem>? = null

    var note: String? = null

    @NonNull
    var paymentDue: MoneyV2? = null

    @NonNull
    var ready: Boolean? = null

    @NonNull
    var requiresShipping: Boolean? = null

    var shippingAddress: MailingAddress? = null

    @NonNull
    var shippingDiscountAllocations: List<DiscountAllocation>? = null

    var shippingLine: ShippingRate? = null

    @NonNull
    var subtotalPrice: MoneyV2? = null

    @NonNull
    // TODO: DO IN FUTURE
    @Description("not supported yet")
    var taxExempt: Boolean = false

    @NonNull
    @Description("not supported yet")
    // TODO: Do in future
    var taxesIncluded = true

    @Description("not supported yet")
    var totalDuties: MoneyV2? = null

    @NonNull
    var totalPriceV2: MoneyV2? = null

    @NonNull
    var totalTaxV2: MoneyV2? = null

    @NonNull
    var updatedAt: MoneyV2? = null

    @NonNull
    var webUrl: String? = null

    @NonNull
    var discountApplications: Connection<DiscountApplication>? = null }

