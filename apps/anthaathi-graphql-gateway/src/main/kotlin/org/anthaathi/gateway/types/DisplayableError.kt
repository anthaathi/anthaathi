package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

interface DisplayableError {
    var field: List<String>?
    @get:NonNull
    @set:NonNull
    var message: String?
}
