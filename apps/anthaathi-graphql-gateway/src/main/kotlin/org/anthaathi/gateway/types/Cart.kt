package org.anthaathi.gateway.types

import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Ignore
import java.util.*

class Cart : Node {
    private var _id: UUID? = null

    @get:Ignore
    val rawId: UUID?
    get() {
        return _id
    }

    @get:Id
    @set:Id
    override var id: String
        get() {
            return RelayUtils.fromGlobalID(
                RelayUtils.GlobalID(
                id = _id.toString(),
                type = "Cart",
                queryParams = mapOf(
                    "hash" to mutableListOf(hash!!)
                )
            ))!!
        }

        set(value) {
            val result = RelayUtils.toGlobalID(value, "Cart")
            _id = UUID.fromString(result.id)
            hash = result.queryParams?.get("hash")?.first()
        }

    @Ignore
    var hash: String? = null
    var note: String? = null
    var createdAt: Date? = null
    var currency: String? = null

    var attributes: List<CartAttribute>? = null

    val checkoutURL: String
        get() {
            return "/checkout/${ id }/information"
        }
}
