package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Id

interface Node {
    @get:Id
    @set:Id
    var id: String
}
