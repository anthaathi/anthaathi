package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class ProductPriceRange {
    @NonNull
    var maxVariantPrice: MoneyV2? = null
    @NonNull
    var minVariantPrice: MoneyV2? = null
}
