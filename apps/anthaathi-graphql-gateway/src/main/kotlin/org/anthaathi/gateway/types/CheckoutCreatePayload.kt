package org.anthaathi.gateway.types

class CheckoutCreatePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
