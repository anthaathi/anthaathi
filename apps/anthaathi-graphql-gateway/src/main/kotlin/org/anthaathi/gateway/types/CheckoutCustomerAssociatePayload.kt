package org.anthaathi.gateway.types

class CheckoutCustomerAssociatePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
    var customer: Customer? = null
}
