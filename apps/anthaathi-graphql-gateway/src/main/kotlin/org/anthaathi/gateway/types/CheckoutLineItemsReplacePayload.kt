package org.anthaathi.gateway.types

class CheckoutLineItemsReplacePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
