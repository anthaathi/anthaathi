package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class DiscountAllocation {
    @NonNull
    var allocatedAmount: MoneyV2? = null

    @NonNull
    var discountApplication: DiscountApplication? = null
}
