package org.anthaathi.gateway.types

class CustomerAddressUpdateResponse {
    var address: MailingAddress? = null
    var customerUserErrors: List<CustomerUserError>? = null
}
