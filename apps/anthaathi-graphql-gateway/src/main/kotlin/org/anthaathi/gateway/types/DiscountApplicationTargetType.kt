package org.anthaathi.gateway.types

enum class DiscountApplicationTargetType {
    LINE_ITEM,
    SHIPPING_LINE,
}
