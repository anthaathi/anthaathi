package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.Name

@Input("CheckoutLineItemUpdateInput")
class CheckoutLineItemUpdateInput : CheckoutLineItemInput() {
    @Id
    var id: String? = null
}
