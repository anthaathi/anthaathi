import { useStyletron } from 'baseui';

export interface ProductProps {
  id: number;
  name: string;
  name_ar?: string;
  description?: string;
  category?: string;
  price: string;
  currency: string;
  image: string;
  weight_unit?: string;
  packaging?: string;
  key?: string;
  notes?: string;
}

export interface ItemProps extends ProductProps {
  numberOfItems: number;
}

export interface OrderedItemProps {
  title?: string;
  items: ItemProps[];
  handlePress?: (item: ItemProps) => void;
}

function OrderedItems(props: OrderedItemProps) {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        margin: '0 auto',
      })}
    >
      {props.items.map((item, index) => (
        <ItemRenderer key={index} item={item} />
      ))}
    </div>
  );
}

export default OrderedItems;

const ItemRenderer = ({ item }: { item: ItemProps }) => {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        border: '1px solid #F0F0F0',
        borderRadius: '2px',
        backgroundColor: '#fff',
        paddingLeft: $theme.sizing.scale600,
        paddingRight: $theme.sizing.scale600,
        paddingTop: $theme.sizing.scale500,
        paddingBottom: $theme.sizing.scale500,
        marginBottom: $theme.sizing.scale600,
        display: 'flex',
      })}
    >
      <img
        src={item.image}
        style={{
          height: '120px',
          width: '120px',
          objectFit: 'cover',
        }}
      />
      <div
        className={css({
          marginLeft: $theme.sizing.scale600,
        })}
      >
        <p
          className={css({
            ...$theme.typography.HeadingXSmall,
            fontWeight: 'bold',
            color: '#364A15',
            marginTop: 0,
            marginBottom: $theme.sizing.scale300,
          })}
        >
          {item.name}
        </p>
        <p
          className={css({
            ...$theme.typography.LabelMedium,
            fontWeight: 'normal',
            color: '#364A15',
            marginTop: 0,
            marginBottom: $theme.sizing.scale100,
          })}
        >
          {Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: item.currency,
          }).format(+item.price) + ' / Piece'}
        </p>
        <FlexText label1="Quantity" label2={item.numberOfItems} />
        <FlexText
          label1="Total"
          label2={Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: item.currency,
          }).format(+item.price * item.numberOfItems)}
        />
      </div>
    </div>
  );
};

const FlexText = ({
  label1,
  label2,
}: {
  label1: string;
  label2: string | number;
}) => {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        display: 'flex',
      })}
    >
      <p
        className={css({
          ...$theme.typography.LabelLarge,
          fontWeight: 'bold',
          color: '#364A15',
          marginTop: 0,
          marginBottom: $theme.sizing.scale200,
        })}
      >
        {label1 + ': '}
      </p>

      <p
        className={css({
          ...$theme.typography.LabelLarge,
          fontWeight: 'bold',
          color: '#008D3E',
          marginTop: 0,
          marginBottom: $theme.sizing.scale200,
        })}
      >
        {label2}
      </p>
    </div>
  );
};
