import { useStyletron } from 'baseui';
import { FlexGrid, FlexGridItem } from 'baseui/flex-grid';
import { Button } from 'baseui/button';
import { Link } from 'react-router-dom';

export interface OrderDetailsCardProps {
  key: string;
  orderNumber: string;
  placedOn: string;
  orderStatus: string;
  shipping: number;
  total: number;
  numberOfItems: number;
}

export function OrderCard(props: { title: string; list: any[] }) {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        marginTop: $theme.sizing.scale200,
        marginBottom: $theme.sizing.scale200,
        marginLeft: $theme.sizing.scale500,
        marginRight: $theme.sizing.scale500,
      })}
    >
      <p
        className={css({
          ...$theme.typography.ParagraphLarge,
          fontWeight: 'bold',
          color: '#767676',
          marginTop: $theme.sizing.scale500,
          marginBottom: $theme.sizing.scale500,
        })}
      >
        {props.title}
      </p>
      <FlexGrid
        flexGridColumnCount={[1, 1, 2, 3]}
        flexGridColumnGap="scale800"
        flexGridRowGap="scale800"
      >
        {props.list.map((item, index) => {
          return (
            <FlexGridItem key={index}>
              <OrderDetailsCard item={item} />
            </FlexGridItem>
          );
        })}
      </FlexGrid>
    </div>
  );
}

const OrderDetailsCard = ({ item }: { item: any }) => {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        border: '1px solid #F0F0F0',
        borderRadius: '2px',
        backgroundColor: '#fff',
        paddingLeft: $theme.sizing.scale600,
        paddingRight: $theme.sizing.scale600,
        paddingTop: $theme.sizing.scale500,
        paddingBottom: $theme.sizing.scale500,
      })}
    >
      <RowData label1="Order number" label2={item.increment_id} />
      <RowData label1="Placed on" label2={item.order_date} />
      <RowData label1="Status" label2={item.status} />
      <RowData label1="Total" label2={'AED ' + item.total.grand_total.value} />
      <RowData label1="Items" label2={item.items.length} />

      <Button
        $as={Link}
        to={`/my-account/order/${item.increment_id}`}
        overrides={{ Root: { style: { width: '100%' } } }}
      >
        View Details
      </Button>
    </div>
  );
};

const RowData = ({
  label1,
  label2,
}: {
  label1: string;
  label2: string | number;
}) => {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: $theme.sizing.scale300,
      })}
    >
      <p
        className={css({
          ...$theme.typography.LabelLarge,
          fontWeight: 'normal',
          color: '#000',
          marginTop: 0,
          marginBottom: 0,
          flex: 1,
        })}
      >
        {label1 + ':'}
      </p>
      <p
        className={css({
          ...$theme.typography.LabelLarge,
          fontWeight: 'bold',
          color: '#000',
          marginTop: 0,
          marginBottom: 0,
          flex: 1,
        })}
      >
        {label2}
      </p>
    </div>
  );
};
