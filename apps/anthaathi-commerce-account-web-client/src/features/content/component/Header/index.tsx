import { useStyletron } from 'baseui';
import { HeadingXXLarge } from 'baseui/typography';
import React from 'react';
import { Button, KIND, SHAPE } from 'baseui/button';
import { PLACEMENT, StatefulPopover } from 'baseui/popover';
import { Block } from 'baseui/block';
import { StatefulMenu } from 'baseui/menu';

export function Header() {
  const [css, $theme] = useStyletron();

  return (
    <header
      className={css({
        height: '64px',
        boxShadow: $theme.lighting.shadow600,
        position: 'sticky',
        backgroundColor: '#FFF',
        top: 0,
      })}
      role="banner"
    >
      <div
        className={css({
          maxWidth: '1400px',
          width: '100%',
          marginLeft: 'auto',
          marginRight: 'auto',
          height: '100%',
          paddingLeft: '12px',
          paddingRight: '12px',
          display: 'flex',
          alignItems: 'center',
        })}
      >
        <a href="/">
          <img
            className={css({ height: '38px' })}
            src="https://www.nrtcfresh.com/wp-content/uploads/elementor/thumbs/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk.png"
            alt=""
          />
        </a>

        <span className={css({ flexGrow: 1 })} />

        <StatefulPopover
          content={() => <StatefulMenu items={[{ label: 'Logout' }]} />}
          returnFocus
          placement={PLACEMENT.bottomRight}
          autoFocus
        >
          <Button kind={KIND.secondary} shape={SHAPE.circle}>
            <span className="fa fa-user" />
          </Button>
        </StatefulPopover>
      </div>
    </header>
  );
}
