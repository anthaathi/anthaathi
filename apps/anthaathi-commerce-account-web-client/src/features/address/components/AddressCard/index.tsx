import { useStyletron } from 'baseui';

export interface AddressCardProps {
  id: number;
  firstname: string;
  lastname: string;
  street: string[];
  city: string;
  region: {
    region_id: number;
    region_code: string;
    region: string;
  };
  postcode: string;
  country_code: string;
  telephone: string;
}

export function AddressCard(props: {
  address: AddressCardProps;
  editClick: (address: AddressCardProps) => void;
  deleteClick: (address: AddressCardProps) => void;
  makeDefaultClick: (address: AddressCardProps) => void;
  defaultAddress?: boolean;
}) {
  const [css, $theme] = useStyletron();

  return (
    <div
      className={css({
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <div
        className={css({
          flex: 1,
          display: 'flex',
          flexDirection: 'row',
          paddingBottom: $theme.sizing.scale700,
        })}
      >
        <div
          className={css({
            flex: 1,
            fontSize: $theme.typography.font650.fontSize,
          })}
        >
          {`${props.address.firstname} ${props.address.lastname}`}
        </div>
        <div
          className={css({
            display: 'flex',
            flexDirection: 'row',
          })}
        >
          <div
            onClick={() => props.editClick(props.address)}
            className={css({
              fontSize: $theme.typography.font350.fontSize,
              alignSelf: 'center',
              paddingLeft: '5px',
              paddingRight: '5px',
              color: '#008d3e',
              ':hover': {
                cursor: 'pointer',
                textDecorationLine: 'underline',
                textDecorationColor: '#008d3e',
              },
            })}
          >
            Edit
          </div>
          {!props.defaultAddress && (
            <div
              onClick={() => props.deleteClick(props.address)}
              className={css({
                fontSize: $theme.typography.font350.fontSize,
                alignSelf: 'center',
                paddingLeft: '5px',
                paddingRight: '5px',
                color: '#008d3e',
                ':hover': {
                  cursor: 'pointer',
                  textDecorationLine: 'underline',
                  textDecorationColor: '#008d3e',
                },
              })}
            >
              Delete
            </div>
          )}
          {!props.defaultAddress && (
          <div
            onClick={() => props.makeDefaultClick(props.address)}
            className={css({
              fontSize: $theme.typography.font350.fontSize,
              alignSelf: 'center',
              paddingLeft: '5px',
              paddingRight: '5px',
              color: '#008d3e',
              ':hover': {
                cursor: 'pointer',
                textDecorationLine: 'underline',
                textDecorationColor: '#008d3e',
              },
            })}
          >
            Make default
          </div>
        )}
        </div>
        
      </div>
      {/* <div className={css({ paddingBottom: $theme.sizing.scale100 })}>
        {`${props.address.firstname} ${props.address.lastname}`}
      </div> */}
      {props.address.street.map((item: string, index: number) => (
        <div
          key={index}
          className={css({ paddingBottom: $theme.sizing.scale100 })}
        >
          {item}
        </div>
      ))}

      <div className={css({ paddingBottom: $theme.sizing.scale100 })}>
        {props.address.city}
      </div>
      <div className={css({ paddingBottom: $theme.sizing.scale100 })}>
        {props.address.region.region}
      </div>
      <div className={css({ paddingBottom: $theme.sizing.scale700 })}>
        {props.address.telephone}
      </div>
      <div
        className={css({
          paddingBottom: $theme.sizing.scale500,
          marginBottom: $theme.sizing.scale700,
          borderBottom: '1px solid #d9d9d9',
        })}
      />
    </div>
  );
}
