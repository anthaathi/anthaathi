import { setContext } from '@apollo/client/link/context';
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';

export function getToken() {
  const storage = localStorage.getItem(
    'M2_VENIA_BROWSER_PERSISTENCE__signin_token',
  );

  if (!storage) {
    return null;
  }

  return JSON.parse(JSON.parse(storage).value);
}

const authLink = setContext(async (_, { headers }) => {
  const newHeaders = { ...headers };

  const token = getToken();

  if (token) {
    newHeaders['Authorization'] = `Bearer ${token}`;
  }

  return {
    headers: {
      ...newHeaders,
    },
  };
});

// Initialize Apollo Client
export const apolloClient = new ApolloClient({
  link: authLink.concat(
    createHttpLink({
      uri: '/graphql',
    }),
  ),
  cache: new InMemoryCache(),
});
