import * as React from 'react';
import { Navigation } from 'baseui/side-navigation';
import { useStyletron } from 'baseui';
import { ApolloProvider } from '@apollo/client';
import { apolloClient } from './utils/apollo-client';
import { Header } from './features/content/component/Header';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';

const App = () => {
  const [css, $theme] = useStyletron();

  const navigate = useNavigate();

  const path = useLocation();

  return (
    <ApolloProvider client={apolloClient}>
      <div className={css({ width: '100%' })}>
        <Header />
        <div
          className={css({
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          })}
        >
          <div
            className={css({
              maxWidth: '1440px',
              width: '100%',
              marginTop: '12px',
              minHeight: '100vh',
              [$theme.mediaQuery?.medium || '']: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'flex-start',
              },
              [$theme.mediaQuery?.small || '']: {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              },
            })}
          >
            <Navigation
              items={[
                {
                  title: 'Dashboard',
                  itemId: '/my-account/',
                },
                {
                  title: 'Orders',
                  itemId: 'order',
                },
                {
                  title: 'Address Book',
                  itemId: 'address',
                },
                // {
                //   title: 'Account Details',
                //   itemId: 'profile',
                // },
                {
                  title: 'My Rewards',
                  itemId: 'reward',
                },
              ]}
              activeItemId={
                path.pathname.replace('/my-account/', '') || '/my-account/'
              }
              onChange={({ item, event }) => {
                event.preventDefault();
                return navigate({ pathname: item.itemId });
              }}
              overrides={{
                Root: {
                  style: () => ({
                    marginLeft: $theme.sizing.scale500,
                    [$theme.mediaQuery?.large || '']: {
                      width: `calc(25% - ${$theme.sizing.scale500})`,
                    },
                    [$theme.mediaQuery?.medium || '']: {
                      width: `calc(30% - ${$theme.sizing.scale500})`,
                    },
                    [$theme.mediaQuery?.small || '']: {
                      marginRight: $theme.sizing.scale500,
                      width: `calc(100% - ${$theme.sizing.scale1000})`,
                    },
                  }),
                },
                NavItem: {
                  style: ({ $active, $theme }) => {
                    if (!$active)
                      return {
                        ':hover': {
                          color: $theme.colors.positive400,
                        },
                      };
                    return {
                      backgroundColor: $theme.colors.positive400,
                      borderLeftColor: '#0f8443',
                      color: $theme.colors.mono900,
                      ':hover': {
                        color: $theme.colors.positive400,
                      },
                    };
                  },
                },
              }}
            />
            <div
              className={css({
                marginLeft: $theme.sizing.scale500,
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(70% - ${$theme.sizing.scale1000})`,
                },
                [$theme.mediaQuery?.medium || '']: {
                  width: `calc(70% - ${$theme.sizing.scale1000})`,
                },
                [$theme.mediaQuery?.small || '']: {
                  marginRight: $theme.sizing.scale500,
                  width: `calc(100% - ${$theme.sizing.scale1000})`,
                },
              })}
            >
              <Outlet />
            </div>
          </div>
        </div>
      </div>
    </ApolloProvider>
  );
};

export default App;
