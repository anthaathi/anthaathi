import React, { Suspense, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import { BaseProvider, createLightTheme } from 'baseui';
import { Provider as StyletronProvider } from 'styletron-react';
import { Client as Styletron } from 'styletron-engine-atomic';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import OrderList from './pages/Order/OrderList';
import OrderDetailsPage from './pages/Order/OrderDetailsPage';
import AddressBook from './pages/UserAddress/AddressBook';
import { AccountDetails } from './pages/AccountDetails';
import RewardsPage from './pages/RewardsPage';

const lightTheme = createLightTheme();
const engine = new Styletron({ prefix: '_' });

const router = createBrowserRouter([
  {
    path: '/my-account/',
    element: (
      <>
        <App />
      </>
    ),
    children: [
      {
        path: '/my-account/order',
        element: <OrderList />,
      },
      {
        path: '/my-account/order/:id',
        element: <OrderDetailsPage />,
      },
      {
        path: '/my-account/address',
        element: <AddressBook />,
      },
      {
        path: '/my-account/address/:id',
        element: <AccountDetails />,
      },
      {
        path: '/my-account/profile',
        element: <>Profile</>,
      },
      {
        path: '/my-account/reward',
        element: <RewardsPage />,
      },
      {
        path: '/my-account/',
        element: <OrderList />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Suspense fallback={'Loading...'}>
      <StyletronProvider value={engine}>
        <BaseProvider theme={lightTheme} zIndex={999}>
          <RouterProvider router={router} />
        </BaseProvider>
      </StyletronProvider>
    </Suspense>
  </React.StrictMode>,
);
