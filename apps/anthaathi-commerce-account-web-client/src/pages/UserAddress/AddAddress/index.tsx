import { gql, useQuery } from '@apollo/client';
import { useStyletron } from 'baseui';
import { Button } from 'baseui/button';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Select } from 'baseui/select';
import { useFormik } from 'formik';
import { useState } from 'react';
import { Textarea } from 'baseui/textarea';

export function AddAddress(props: {
  isSubmitting: boolean;
  onChange: (values: any) => void;
  initialValue?: any;
}) {
  const [css, $theme] = useStyletron();
  const [value, setValue] = useState([]);
  const {
    loading: loadingCountries,
    data: dataCountries,
    error: errorCountries,
  } = useQuery(gql`
    query getCountries {
      countries {
        id
        full_name_english
        available_regions {
          id
          code
          name
        }
      }
    }
  `);

  const formik = useFormik({
    initialValues: props.initialValue
      ? props.initialValue
      : {
          firstName: '',
          lastName: '',
          address: '',
          apartment: '',
          area: '',
          emirate: '',
          telephone: '',
          email: '',
          country: 'AE',
        },

    onSubmit: (values) => {
      props.onChange(values);
    },
  });

  return (
    <div
      className={css({
        marginTop: $theme.sizing.scale500,
      })}
    >
      <form onSubmit={formik.handleSubmit}>
        <div className={css({ display: 'flex', flexDirection: 'column' })}>
          <div
            className={css({
              [$theme.mediaQuery?.large || '']: {
                display: 'flex',
              },
              width: '100%',
            })}
          >
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(100% - ${$theme.sizing.scale500})`,
                  marginRight: $theme.sizing.scale500,
                },
              })}
            >
              <FormControl label="First name">
                <Input
                  placeholder="Enter first name"
                  id="firstName"
                  name="firstName"
                  onChange={formik.handleChange}
                  value={formik.values.firstName}
                />
              </FormControl>
            </div>
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(100% - ${$theme.sizing.scale500})`,
                  marginLeft: $theme.sizing.scale500,
                },
              })}
            >
              <FormControl label="Last name">
                <Input
                  placeholder="Enter last name"
                  id="lastName"
                  overrides={{
                    Input: {
                      style: () => ({
                        // ':focus': { border: `2px solid #0f8443` },
                      }),
                    },
                  }}
                  name="lastName"
                  onChange={formik.handleChange}
                  value={formik.values.lastName}
                />
              </FormControl>
            </div>
          </div>
          <FormControl label="Phone Number">
            <Input
              placeholder="Enter phone number"
              name="telephone"
              onChange={formik.handleChange}
              value={formik.values.telephone}
            />
          </FormControl>
          <FormControl label="Address">
            <Textarea
              placeholder="Address"
              id="address"
              name="address"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.address}
            />
          </FormControl>
          <FormControl label="Apartment, suite, etc. (option)">
            <Input
              placeholder="Apartment, suite, etc. (option)"
              id="apartment"
              name="apartment"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.apartment}
            />
          </FormControl>
          <FormControl label="Country">
            <Select
              options={dataCountries?.countries?.map((country: any) => ({
                id: country?.id ?? '',
                label: country?.full_name_english,
              }))}
              isLoading={loadingCountries}
              value={[{ id: formik.values['country'] }]}
              onChange={(e) => {
                formik.handleChange({
                  target: {
                    name: 'country',
                    value: e.value?.[0].id,
                  },
                });
              }}
              placeholder="Country"
            />
          </FormControl>

          <div
            className={css({
              [$theme.mediaQuery?.large || '']: {
                display: 'flex',
              },
              width: '100%',
            })}
          >
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(100% - ${$theme.sizing.scale500})`,
                  marginRight: $theme.sizing.scale500,
                },
              })}
            >
              <FormControl label="Emirate">
                <Select
                  isLoading={loadingCountries}
                  placeholder="Emirate"
                  id="emirate"
                  onChange={(e) => {
                    formik.handleChange({
                      target: { name: 'emirate', value: e.value?.[0].id },
                    });
                  }}
                  options={
                    dataCountries?.countries
                      ?.find((res: any) => res?.id === formik.values.country)
                      ?.available_regions?.map((region: any) => ({
                        id: region?.id!!,
                        label: region?.name!!,
                      })) || []
                  }
                  value={[{ id: formik.values.emirate }]}
                />
              </FormControl>
            </div>
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(100% - ${$theme.sizing.scale500})`,
                  marginRight: $theme.sizing.scale500,
                },
              })}
            >
              <FormControl label="Area">
                <Select
                  placeholder="Area"
                  id="area"
                  options={[
                    {
                      label: 'Discovery Gardens',
                      id: 1,
                    },
                  ]}
                  onChange={(e) => {
                    formik.handleChange({
                      target: {
                        name: 'area',
                        value: e.value?.[0].id,
                      },
                    });
                  }}
                  value={[{ id: formik.values.area }]}
                />
              </FormControl>
            </div>
          </div>
        </div>
        <div
          className={css({
            display: 'flex',
            flexDirection: 'row-reverse',
          })}
        >
          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: () => ({
                  marginTop: $theme.sizing.scale950,
                  width: '240px',
                  backgroundColor: '#0f8443',
                  ':hover': {
                    backgroundColor: '#0f8443',
                  },
                }),
              },
            }}
          >
            Save Changes
          </Button>
        </div>
      </form>
    </div>
  );
}
