import { useStyletron } from 'baseui';
import { Button } from 'baseui/button';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Select } from 'baseui/select';
import { useFormik } from 'formik';
import { useState } from 'react';

export function AccountDetails() {
  const [css, $theme] = useStyletron();
  const [value, setValue] = useState([]);
  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      displayName: '',
      mobileNumber: '',
      emailAddress: '',
      gender: '',
      nationality: '',
    },

    onSubmit: (values) => {
      console.log(values);
    },
  });

  return (
    <div
      className={css({
        marginTop: $theme.sizing.scale500,
      })}
    >
      <form onSubmit={formik.handleSubmit}>
        <div className={css({ display: 'flex', flexDirection: 'column' })}>
          <div
            className={css({
              [$theme.mediaQuery?.large || '']: {
                display: 'flex',
              },
              width: '100%',
            })}
          >
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(100% - ${$theme.sizing.scale500})`,
                  marginRight: $theme.sizing.scale500,
                },
              })}
            >
              <FormControl label="First name">
                <Input
                  placeholder="Enter first name"
                  name="firstName"
                  onChange={formik.handleChange}
                  value={formik.values.firstName}
                />
              </FormControl>
            </div>
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery?.large || '']: {
                  width: `calc(100% - ${$theme.sizing.scale500})`,
                  marginLeft: $theme.sizing.scale500,
                },
              })}
            >
              <FormControl label="Last name">
                <Input
                  placeholder="Enter last name"
                  name="lastName"
                  onChange={formik.handleChange}
                  value={formik.values.lastName}
                />
              </FormControl>
            </div>
          </div>

          <FormControl label="Display name">
            <Input
              placeholder="Enter display name"
              name="displayName"
              onChange={formik.handleChange}
              value={formik.values.displayName}
            />
          </FormControl>

          <FormControl label="Email address">
            <Input
              placeholder="Enter email address"
              name="emailAddress"
              onChange={formik.handleChange}
              value={formik.values.emailAddress}
            />
          </FormControl>

          <FormControl label="Phone Number">
            <Input
              placeholder="Enter phone number"
              name="mobileNumber"
              onChange={formik.handleChange}
              value={formik.values.mobileNumber}
            />
          </FormControl>

          <FormControl label="Gender">
            <Select
              id="select-id"
              value={value}
              onChange={({ value }: any) => setValue(value)}
              options={[
                { id: 'Male', address: 'a1' },
                { id: 'Female', address: 'a2' },
              ]}
              labelKey="id"
              valueKey="gender"
            />
          </FormControl>

          <FormControl label="Nationality">
            <Input
              placeholder="Enter nationality"
              name="nationality"
              onChange={formik.handleChange}
              value={formik.values.nationality}
            />
          </FormControl>
        </div>
        <div
          className={css({
            display: 'flex',
            flexDirection: 'row-reverse',
          })}
        >
          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: () => ({
                  marginTop: $theme.sizing.scale950,
                  width: '240px',
                  backgroundColor: '#0f8443',
                  ':hover': {
                    backgroundColor: '#0f8443',
                  },
                }),
              },
            }}
          >
            Save Changes
          </Button>
        </div>
      </form>
    </div>
  );
}
