import React from 'react';
import { Table } from 'baseui/table-semantic';
import { useStyletron } from 'baseui';

function RewardsPage() {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({
        width: '100%',
        height: '100%',
        boxSizing: 'border-box',
        marginTop: $theme.sizing.scale650,
      })}
    >
      <div
        className={css({
          fontSize: $theme.typography.font850.fontSize,
          fontWeight: '700',
          marginBottom: '20px',
        })}
      >
        My Rewards
      </div>
      <Table
        columns={['Date', 'Change', '', 'Component', 'Action', 'Points Left']}
        data={[
          [
            '1/9/22',
            '-15.00',
            'expire in 45 days',
            'Order #123',
            'Paid',
            '25.00',
          ],
          [
            '22/7/22',
            '-25.00',
            'expire in 10 days',
            'Order #113',
            'Paid',
            '35.00',
          ],
          ['1/1/22', '+15.00', 'expired', '_', 'Register', '25.00'],
          [
            '1/9/22',
            '-15.00',
            'expire in 45 days',
            'Order #123',
            'Paid',
            '25.00',
          ],
          [
            '1/9/22',
            '-15.00',
            'expire in 45 days',
            'Order #123',
            'Paid',
            '25.00',
          ],[
            '1/9/22',
            '-15.00',
            'expire in 45 days',
            'Order #123',
            'Paid',
            '25.00',
          ],[
            '1/9/22',
            '-15.00',
            'expire in 45 days',
            'Order #123',
            'Paid',
            '25.00',
          ],
        ]}
      />
    </div>
  );
}

export default RewardsPage;
