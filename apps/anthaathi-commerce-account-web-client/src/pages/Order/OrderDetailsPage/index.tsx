import { gql, useQuery } from '@apollo/client';
import OrderDetailsCard from '../../../features/order/component/OrderDetailsCard';
import OrderedItems from '../../../features/order/component/OrderedItems';
import PaymentAndAddressDetails from '../../../features/order/component/PaymentAndAddressDetails';
import {
  GetOrderByIdQuery,
  GetOrderByIdQueryVariables,
} from '../../../__generated__/graphql';
import { useParams } from 'react-router-dom';

export default function OrderDetailsPage() {
  const query = useParams<{ id: string }>();

  const { data: order, loading } = useQuery<
    GetOrderByIdQuery,
    GetOrderByIdQueryVariables
  >(
    gql`
      query getOrderById($orderId: String!) {
        customer {
          orders(filter: { number: { eq: $orderId } }) {
            items {
              number
              order_date
              status
              total {
                grand_total {
                  value
                  currency
                }
              }
              items {
                id
                product_name
                quantity_ordered
                product_sale_price {
                  currency
                  value
                }
              }
              shipments {
                items {
                  product_sale_price {
                    currency
                    value
                  }
                }
              }
            }
          }
        }
      }
    `,
    {
      variables: {
        orderId: query.id!!,
      },
    },
  );

  if (loading) {
    return <>Loading </>;
  }

  const orderInfo = order?.customer?.orders?.items?.[0];

  return (
    <div>
      <OrderDetailsCard
        details={{
          orderNumber: '#' + orderInfo?.number,
          placedOn: orderInfo?.order_date!!,
          orderStatus: orderInfo?.status!!,
          shipping: 0,
          total: Intl.NumberFormat('en-US', {
            currency: orderInfo?.total?.grand_total.currency ?? 'AED',
            style: 'currency',
          }).format(orderInfo?.total?.grand_total.value ?? 0),
          numberOfItems: orderInfo?.shipments?.length ?? 0,
          discount: 4,
        }}
      />
      <OrderedItems
        items={
          orderInfo?.items
            ?.filter((res) => Boolean(res))
            .map((res) => ({
              id: res?.id,
              name: res?.product_name,
              key: res?.id,
              price: res?.product_sale_price.value,
              currency: res?.product_sale_price.currency,
              numberOfItems: res?.quantity_ordered,
            })) ?? [
            {
              id: 1,
              name: 'Baby Yellow Pepper',
              image:
                'https://burst.shopifycdn.com/photos/fruit-plate.jpg?width=373&height=373&format=pjpg&exif=1&iptc=1',
              key: '12',
              price: '12',
              numberOfItems: 2,
              currency: 'AED',
              weight_unit: 'KG',
              packaging: '500 gms',
            },
          ]
        }
      />
      <PaymentAndAddressDetails
        details={{
          paymentOption: 'Debit / Credit Card',
          name: 'Jane Doe',
          apartment: '13C',
          landmark: 'La Celeste Residence, Street 1A',
          city: 'Dubai',
          country: 'UAE',
          mobileNumber: '+971-21321321',
        }}
      />
    </div>
  );
}
