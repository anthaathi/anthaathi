import { gql, useQuery } from '@apollo/client';
import { useStyletron } from 'baseui';
import { Button } from 'baseui/button';
import { LoadingSpinner } from 'baseui/button/styled-components';
import React, { useEffect, useMemo, useState } from 'react';
import { OrderCard } from '../../../features/order/component/OrderCart';

const getOrderList = gql`
  query Q($currentPage: Int) {
    customer {
      orders(currentPage: $currentPage, pageSize: 2) {
        items {
          id
          order_date
          increment_id
          items {
            product_name
            quantity_ordered
          }
          total {
            grand_total {
              value
              currency
            }
          }
          status
        }
        page_info {
          page_size
          current_page
          total_pages
        }
      }
    }
  }
`;

function OrderList() {
  const [css, $theme] = useStyletron();
  const [pageNo, setPageNo] = useState<number>(1);
  const { loading, fetchMore, data, error } = useQuery(getOrderList, {
    variables: { currentPage: pageNo },
  });
  const [loadMore, setLoadMore] = useState<boolean>(false);
  const [pageLoad, setPageLoad] = useState<boolean>(true);
  const [orderList, setOrderList] = useState<any[]>([]);

  useEffect(() => {
    if (!loading) {
      setOrderList((prev) => [...prev, ...data?.customer.orders.items]);
      setPageLoad(false);
      if (
        data?.customer.orders.page_info.current_page <
        data?.customer.orders.page_info.total_pages
      ) {
        setLoadMore(true);
      } else {
        setLoadMore(false);
      }
    }
  }, [loading]);

  return (
    <>
      {pageLoad ? (
        <div>
          <LoadingSpinner />
        </div>
      ) : (
        <>
          {orderList.length === 0 ? (
            <p>There are not orders</p>
          ) : (
            <>
              <OrderCard title="June 2021" list={orderList} />
              {loadMore ? (
                <div
                  className={css({
                    textAlign: 'center',
                    marginTop: $theme.sizing.scale600,
                    width: '100%',
                    paddingTop: '12px',
                    paddingBottom: '12px',
                    fontWeight: 'bold',
                    fontSize: '18px',
                    borderRadius: '2px',
                    color: '#0f8443',
                    border: '1px solid #0f8443',
                    marginVertical: 10,
                    ':hover': { cursor: 'pointer' },
                  })}
                  onClick={() => {
                    if (!loading) {
                      fetchMore({
                        variables: {
                          currentPage: pageNo + 1,
                        },
                      });
                      setPageNo((prev) => prev + 1);
                    }
                  }}
                >
                  {loading ? (
                    <LoadingSpinner color="#0f8443" />
                  ) : (
                    'Load more...'
                  )}
                </div>
              ) : null}
            </>
          )}
        </>
      )}
    </>
  );
}

export default OrderList;
