package org.anthaathi.commerce.product.entity

import java.io.Serializable
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@Table(name = "product_variant_stock")
open class ProductVariantStock : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_variant")
    open var productVariant: ProductVariant? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_updated")
    open var userUpdated: DirectusUser? = null

    @Column(name = "date_updated")
    open var dateUpdated: OffsetDateTime? = null

    @Column(name = "stock", nullable = false)
    open var stock: Long? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "location", nullable = false)
    open var location: Location? = null

    companion object {
        private const val serialVersionUID = -3722940325709074288L
    }

    //TODO [JPA Buddy] generate columns from DB
}
