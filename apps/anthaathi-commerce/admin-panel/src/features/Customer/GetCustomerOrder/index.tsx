import { useRestConfig } from '../../../hooks/use-rest-config';
import usePromise, { States } from '../../../hooks/use-promise';
import { OrdersAPIsExtended } from '../../../services/orders';
import { useState } from 'react';
import { useStyletron } from 'baseui';
import { LabelSmall } from 'baseui/typography';

export function CustomerOrders({ customerId }: { customerId: number }) {
  const restConfig = useRestConfig();

  const [page, setPage] = useState(1);

  const [css, $theme] = useStyletron();

  const [result, error, state] = usePromise(
    () =>
      new OrdersAPIsExtended(restConfig).getV1Orders({
        searchCriteriaCurrentPage: page,
        searchCriteriaPageSize: 25,
        filterGroups: [
          [
            {
              field: 'customer_id',
              value: customerId.toString(),
              conditionType: 'eq',
            },
          ],
        ],
      }),
    [customerId]
  );

  if (state == States.pending) {
    return <>Loading</>;
  }

  return (
    <>
      <table
        className={css({ width: 'calc(100% + 26px + 26px)', margin: '-26px' })}
      >
        <thead
          className={css({
            height: '34px',
            backgroundColor: '#EEE',
          })}
        >
          <th align="left">
            <LabelSmall $style={{ fontWeight: 500, padding: '12px' }}>
              #
            </LabelSmall>
          </th>
          <th align="left">
            <LabelSmall $style={{ fontWeight: 400, padding: '12px' }}>
              Order Placed date
            </LabelSmall>
          </th>
          <th align="right">
            <LabelSmall $style={{ fontWeight: 400, padding: '12px' }}>
              Grand Total
            </LabelSmall>
          </th>
        </thead>
        <tbody>
          {result.items.map((res) => {
            return (
              <tr className={css({ height: '34px' })}>
                <td>
                  <LabelSmall $style={{ fontWeight: 400, padding: '12px' }}>
                    {res.incrementId}
                  </LabelSmall>
                </td>
                <td>
                  <LabelSmall $style={{ fontWeight: 400, padding: '12px' }}>
                    {Intl.DateTimeFormat('en-US', {}).format(
                      new Date(res.createdAt!!)
                    )}
                  </LabelSmall>
                </td>
                <td align="right">
                  <LabelSmall $style={{ fontWeight: 400, padding: '12px' }}>
                    {Intl.NumberFormat('en-US', {
                      currency: res.orderCurrencyCode,
                      style: 'currency',
                    }).format(res.grandTotal)}
                  </LabelSmall>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
