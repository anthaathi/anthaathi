import { useSearchParams } from 'react-router-dom';
import { useCallback, useMemo } from 'react';

export function usePage(): [number, (input: number) => void] {
  const [searchParams, setSearchParams] = useSearchParams();
  const page = useMemo(() => {
    return +(searchParams.get('page') ?? 1);
  }, [searchParams.get('page')]);

  const setPage = useCallback((page: number) => {
    setSearchParams({ page: page.toString() });
  }, []);

  return [page, setPage];
}
