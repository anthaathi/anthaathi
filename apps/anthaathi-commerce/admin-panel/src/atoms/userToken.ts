import { atom, AtomEffect, selector } from 'recoil';

const localStorageEffect: (key: string) => AtomEffect<string | null> =
  (key: string) =>
  ({ setSelf, onSet }) => {
    const savedValue = localStorage.getItem(key);
    if (savedValue != null) {
      const parse = JSON.parse(savedValue);
      const exp = new Date(JSON.parse(atob(parse.split('.')[1])).exp * 1000);

      if (exp.getTime() > new Date().getTime()) {
        setSelf(parse);
      }
    }

    onSet((newValue, _, isReset) => {
      isReset
        ? localStorage.removeItem(key)
        : localStorage.setItem(key, JSON.stringify(newValue));
    });
  };

export const userTokenAtom = atom<string | null>({
  key: 'userToken',
  default: null,
  effects: [localStorageEffect('current_user')],
});

export const userInfoSelector = selector<{
  exp: number;
  iat: number;
  uid: number;
  utypid: number;
}>({
  key: 'userInfo',
  get({ get }) {
    const user = get(userTokenAtom);
    if (!user) {
      return null;
    }

    const token = user?.split('.')[1];

    return JSON.parse(atob(token));
  },
});
