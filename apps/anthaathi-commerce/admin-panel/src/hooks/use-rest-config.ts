import { useRecoilValue } from 'recoil';
import { userTokenAtom } from '../atoms/userToken';
import { useMemo } from 'react';
import {
  Configuration,
  ConfigurationParameters,
  FetchParams,
} from '../__generated__';

export function useRestConfig() {
  const token = useRecoilValue(userTokenAtom);

  return useMemo(() => {
    const configParams: ConfigurationParameters = {
      basePath:
        import.meta.env.VITE_APP_BASE_PATH ??
        'https://engine.commerce.anthaathi.dev/rest/default',
      middleware: [
        {
          pre(context): Promise<FetchParams> {
            const headers: HeadersInit = {
              ...(context.init.headers || {}),
            };

            if (token) {
              // @ts-ignore
              headers['Authorization'] = `Bearer ${token}`;
            }

            return Promise.resolve({
              url: context.url,
              init: {
                ...context.init,
                headers,
              },
            });
          },
        },
      ],
    };

    return new Configuration(configParams);
  }, [token]);
}
