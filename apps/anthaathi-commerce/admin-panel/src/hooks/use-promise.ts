import { DependencyList, useEffect, useReducer } from 'react';
import { useSnackbar } from 'baseui/snackbar';

function resolvePromise<T>(promise: Promise<T> | Function) {
  if (typeof promise === 'function') {
    return promise();
  }

  return promise;
}

export enum States {
  pending = 'pending',
  rejected = 'rejected',
  resolved = 'resolved',
}

const defaultState = {
  error: undefined,
  result: undefined,
  state: States.pending,
};

function reducer(state: any, action: { type: States; payload?: any }) {
  switch (action.type) {
    case States.pending:
      return defaultState;

    case States.resolved:
      return {
        error: undefined,
        result: action.payload,
        state: States.resolved,
      };

    case States.rejected:
      return {
        error: action.payload,
        result: undefined,
        state: States.rejected,
      };

    /* istanbul ignore next */
    default:
      return state;
  }
}

export type ReturnPromise<T> = () => Promise<T>;

export type InputPromise<Result> = Promise<Result> | ReturnPromise<Result>;

function usePromise<T>(
  promise: InputPromise<T>,
  deps: React.DependencyList
): [T, any, States] {
  const [{ error, result, state }, dispatch] = useReducer(
    reducer,
    defaultState
  );

  const enqueueSnackbar = useSnackbar();

  useEffect(() => {
    promise = resolvePromise(promise);

    if (!promise) {
      return;
    }

    let canceled = false;

    dispatch({ type: States.pending });

    (promise as Promise<T>).then(
      (result) =>
        !canceled &&
        dispatch({
          payload: result,
          type: States.resolved,
        }),
      (error) => {
        console.log(error);
        if (error.response.status === 401) {
          enqueueSnackbar.enqueue({
            message: error.message,
          });
        }

        return (
          !canceled &&
          dispatch({
            payload: error,
            type: States.rejected,
          })
        );
      }
    );

    return () => {
      canceled = true;
    };
  }, deps);

  return [result, error, state];
}

export default usePromise;
