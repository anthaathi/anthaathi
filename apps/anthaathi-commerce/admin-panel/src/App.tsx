import { Client as Styletron } from 'styletron-engine-atomic';
import { Provider as StyletronProvider } from 'styletron-react';
import { BaseProvider, LightTheme, useStyletron } from 'baseui';
import {
  createBrowserRouter,
  Navigate,
  Outlet,
  RouterProvider,
  useLocation,
  useNavigate,
} from 'react-router-dom';
import Home from './pages/Home';
import { AppNavBar, NavItem } from 'baseui/app-nav-bar';
import { Overflow } from 'baseui/icon';
import React, { useCallback, useMemo } from 'react';
import Order from './pages/Order';
import SignIn from './pages/SignIn';
import { PLACEMENT, SnackbarProvider } from 'baseui/snackbar';
import { useRecoilState, useRecoilValue } from 'recoil';
import { userTokenAtom } from './atoms/userToken';
import OrderInfo from './pages/OrderInfo';
import ProductInfo from './pages/ProductInfo';
import Product from './pages/Product';
import ProductInventory from './pages/ProductInvetory';
import Customer from './pages/Customer';
import { CustomerInfo } from './pages/CustomerInfo';
import OrderInfoPrint from './pages/OrderInfoPrint';

const AppRoot = () => {
  let location = useLocation();
  const navigate = useNavigate();

  const mainItems = useMemo(() => {
    return (
      [
        { label: 'Dashboard', info: { path: '/' } },
        { label: 'Orders', info: { path: '/order' } },
        {
          label: 'Product',
          info: { path: '/product' },
          children: [
            {
              label: 'Inventory',
              info: { path: '/product/inventory' },
            },
          ],
        },
        { label: 'Customer', info: { path: '/customer' } },
        { label: 'Reports', info: { path: '/report' } },
      ] as NavItem[]
    ).map((res) => {
      return {
        ...res,
        children: res.children?.map((res) => {
          return {
            active: location.pathname === res.info.path,
            ...res,
          };
        }),
        active: location.pathname === res.info.path,
      };
    });
  }, [location.pathname]);
  const [, setUserAccessToken] = useRecoilState(userTokenAtom);

  const onUserItemSelect = useCallback(() => {
    setUserAccessToken(null);
  }, []);

  const [css, $theme] = useStyletron();

  return (
    <div
      className={css({
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
      })}
    >
      {!location.pathname.startsWith('/auth') && (
        <AppNavBar
          title="Admin"
          mainItems={mainItems}
          onMainItemSelect={(item) => {
            navigate({ pathname: item.info.path });
          }}
          overrides={{
            Root: {
              style: {
                backgroundColor: '#ededed',
                boxShadow: $theme.lighting.shadow400,
              },
            },
            DesktopMenu: {
              style: {
                maxWidth: 'none',
              },
            },
          }}
          username="Omkar Yadav"
          userItems={[
            { icon: Overflow, label: 'Logout', info: { path: '/logout' } },
          ]}
          onUserItemSelect={(item) => {
            if (item.info.path === '/logout') {
              onUserItemSelect();
            }
          }}
        />
      )}

      <div className={css({ flexGrow: 1 })}>
        <Outlet />
      </div>
    </div>
  );
};

function RequireAuth({ children }: { children: JSX.Element }) {
  let location = useLocation();
  const token = useRecoilValue(userTokenAtom);

  if (!token) {
    return (
      <Navigate
        to={`/auth/sign-in?redirect-url=${encodeURIComponent(
          location.pathname
        )}`}
        state={{ from: location }}
        replace
      />
    );
  }

  return children;
}

const router = createBrowserRouter([
  {
    element: <AppRoot />,
    children: [
      {
        path: '/',
        element: (
          <RequireAuth>
            <Home />
          </RequireAuth>
        ),
      },
      {
        path: '/customer/:id',
        element: (
          <RequireAuth>
            <CustomerInfo />
          </RequireAuth>
        ),
      },
      {
        path: '/customer',
        element: (
          <RequireAuth>
            <Customer />
          </RequireAuth>
        ),
      },
      {
        path: '/order/:id',
        element: (
          <RequireAuth>
            <OrderInfo />
          </RequireAuth>
        ),
      },
      {
        path: '/order',
        element: (
          <RequireAuth>
            <Order />
          </RequireAuth>
        ),
      },
      {
        path: '/product/inventory',
        element: (
          <RequireAuth>
            <ProductInventory />
          </RequireAuth>
        ),
      },
      {
        path: '/product/:id',
        element: (
          <RequireAuth>
            <ProductInfo />
          </RequireAuth>
        ),
      },
      {
        path: '/product',
        element: (
          <RequireAuth>
            <Product />
          </RequireAuth>
        ),
      },
      {
        path: '/auth/sign-in',
        element: <SignIn />,
      },
    ],
  },
  {
    path: '/order-print/:id',
    element: (
      <RequireAuth>
        <OrderInfoPrint />
      </RequireAuth>
    ),
  },
]);

const engine = new Styletron({
  prefix: '_',
});

export default function App() {
  return (
    <StyletronProvider value={engine}>
      <BaseProvider theme={LightTheme}>
        <SnackbarProvider placement={PLACEMENT.bottom}>
          <RouterProvider router={router} />
        </SnackbarProvider>
      </BaseProvider>
    </StyletronProvider>
  );
}
