import { useStyletron } from 'baseui';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Button } from 'baseui/button';
import { Formik } from 'formik';
import { DisplayXSmall } from 'baseui/typography';
import { IntegrationAdminTokenApi } from '../../__generated__';
import { useMemo } from 'react';
import { useSnackbar } from 'baseui/snackbar';
import { useRecoilState } from 'recoil';
import { userTokenAtom } from '../../atoms/userToken';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useRestConfig } from '../../hooks/use-rest-config';

export default function SignIn() {
  const [css, $theme] = useStyletron();

  const apiConfig = useRestConfig();

  const api = useMemo(() => {
    return new IntegrationAdminTokenApi(apiConfig);
  }, [apiConfig]);

  const { enqueue } = useSnackbar();

  const [, setUserToken] = useRecoilState(userTokenAtom);

  const navigate = useNavigate();
  const [location] = useSearchParams();

  return (
    <div
      className={css({
        maxWidth: '600px',
        margin: '0 auto',
        display: 'flex',
        alignItems: 'center',
        placeContent: 'center',
        height: '100vh',
      })}
    >
      <Formik<{ username: string; password: string }>
        initialValues={{ username: '', password: '' }}
        onSubmit={(e, formikHelpers) => {
          api
            .postV1IntegrationAdminToken({
              postV1IntegrationAdminTokenRequest: e,
            })
            .then((e) => {
              setUserToken(JSON.parse(e));
              const redirectUrl = location.get('redirect-url') || '/';
              if (redirectUrl.startsWith('/')) {
                navigate(redirectUrl);
              } else {
                navigate('/');
              }
              formikHelpers.setSubmitting(false);
            })
            .catch((e) => {
              formikHelpers.setSubmitting(false);
              if (e.response.status === 401) {
                enqueue({
                  message: 'Invalid username or password',
                });

                return e;
              }

              enqueue({
                message: e.message,
              });
            });
        }}
      >
        {({ values, isSubmitting, handleChange, handleBlur, handleSubmit }) => {
          return (
            <form
              action=""
              className={css({ width: '100%' })}
              onSubmit={handleSubmit}
            >
              <div
                className={css({
                  position: 'relative',
                  borderRadius: '10px',
                  boxShadow: $theme.lighting.shadow400,
                })}
              >
                <img
                  src="https://images.unsplash.com/photo-1530789253388-582c481c54b0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2670&q=80"
                  alt="Login Page"
                  className={css({
                    position: 'absolute',
                    width: '100%',
                    height: '210px',
                    objectFit: 'cover',
                    borderRadius: '10px',
                  })}
                />
                <div
                  className={css({
                    paddingTop: '240px',
                    paddingLeft: '24px',
                    paddingRight: '24px',
                    paddingBottom: '24px',
                  })}
                >
                  <DisplayXSmall>Sign In</DisplayXSmall>
                  <div className={css({ paddingBottom: '12px' })}></div>

                  <FormControl label="Username">
                    <Input
                      value={values.username}
                      name="username"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </FormControl>
                  <FormControl label="Password">
                    <Input
                      onChange={handleChange}
                      name="password"
                      type="password"
                      onBlur={handleBlur}
                      value={values.password}
                    />
                  </FormControl>

                  <div className={css({ paddingBottom: '12px' })}></div>

                  <Button type="submit" isLoading={isSubmitting}>
                    Sign In
                  </Button>
                </div>
              </div>
            </form>
          );
        }}
      </Formik>
    </div>
  );
}
