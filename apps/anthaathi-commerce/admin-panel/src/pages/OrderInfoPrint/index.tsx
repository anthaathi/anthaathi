import { useStyletron } from 'baseui';
import { useNavigate, useParams } from 'react-router-dom';
import { RenderAddress } from '../../features/RenderAddress';
import usePromise from '../../hooks/use-promise';
import { useRestConfig } from '../../hooks/use-rest-config';
import {
  OrdersIdApi,
  SalesDataOrderAddressInterface,
} from '../../__generated__';
import { useReactToPrint } from 'react-to-print';
import { ReactInstance, useRef } from 'react';
import { Button, KIND } from 'baseui/button';

function OrderInfoPrint() {
  const [css] = useStyletron();
  const componentRef = useRef<HTMLDivElement>(null);
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const restConfig = useRestConfig();
  const navigate = useNavigate();

  const params = useParams<{ id: string }>();

  const [result, error, state] = usePromise(() => {
    const returnValue = new OrdersIdApi(restConfig);

    return returnValue.getV1OrdersId({
      id: +(params.id || 0),
    });
  }, [restConfig, params.id]);

  return (
    <>
      {result && (
        <div ref={componentRef} className={css({ padding: '0 20px' })}>
          <table className={css({ width: '100%' })}>
            <tbody>
              <tr>
                <td className={css({ width: '412px' })}>
                  <h3>
                    <span className={css({ color: '#339966' })}>
                      <strong>TAX INVOICE</strong>
                    </span>
                  </h3>
                  <p className={css({ lineHeight: '0.6' })}>
                    NRTC Dubai International Vegetables And Fruits Trading LLC
                  </p>
                  <p className={css({ lineHeight: '0.6' })}>
                    P.O Box 21802, Dubai, UAE
                  </p>
                  <p className={css({ lineHeight: '0.6' })}>
                    Al Awir Central Market
                  </p>
                  <p className={css({ lineHeight: '0.6' })}>
                    Toll Free No: <strong>800 NRTC (67823)</strong>
                  </p>
                  <p className={css({ lineHeight: '0.6' })}>
                    Email: <a>customercare@nrtcfresh.com</a>
                  </p>
                  <p className={css({ lineHeight: '0.6' })}>
                    TRN: 100325206900003
                  </p>
                </td>
                <td className={css({ float: 'right', paddingTop: '40px' })}>
                  <img
                    src="https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x@2x.png?v=1653569545"
                    alt=""
                    width="145"
                    height="97"
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <br />
          <br />
          <div
            className={css({
              display: 'flex',
              border: '1px solid black',
              padding: '0px 20px',
              justifyCcontent: 'space-between',
            })}
          >
            <div className={css({ width: '60%' })}>
              <p className={css({ lineHeight: '0.6' })}>
                {result.customerFirstname} {result.customerLastname},
              </p>
              {/* <p className={css({ lineHeight: '1' })}>
            ${shipping.address_1 ? shipping.address_1 : '--'},
          </p> */}
              {result.billingAddress && (
                <p className={css({ lineHeight: '1' })}>
                  <RenderAddress
                    address={
                      result.billingAddress as SalesDataOrderAddressInterface
                    }
                  />
                </p>
              )}
              {/* <p className={css({lineHeight: "0.6"})}>${meta_data.find((ele) => ele.key === 'shipping_uae_area_name')?.value}, ${meta_data.find((ele) => ele.key === 'shipping_uae_city_name')?.value}</p> */}
              {/* <p className={css({lineHeight: "0.6"})}>Phone: ${shipping.phone || meta_data.find((ele) => ele.key === '_shipping_phone' || ele.key === 'shipping_phone') ? meta_data.find((ele) => ele.key === '_shipping_phone' || ele.key === 'shipping_phone')?.value : '--'}</p> */}
              <p className={css({ lineHeight: '0.6' })}>
                Email: <a>{result.customerEmail}</a>
              </p>
              <p className={css({ lineHeight: '0.6' })}>&nbsp;</p>
              <p className={css({ lineHeight: '1' })}>
                Special Instructions: --
              </p>
            </div>
            <table className={css({ width: '40%', padding: '10px 0' })}>
              <tr>
                <td>Invoice No:</td>
                <td>
                  <strong>#101011</strong>
                </td>
              </tr>
              <tr>
                <td>Delivery Date:</td>
                <td>
                  <strong>2022-12-03</strong>
                </td>
              </tr>
              <tr>
                <td>Emirate:</td>
                <td>
                  <strong>ABUDHABI</strong>
                </td>
              </tr>
              <tr>
                <td>Area:</td>
                <td>
                  <strong>AL NAHYAN</strong>
                </td>
              </tr>
              <tr>
                <td>Zone:</td>
                <td>
                  <strong>Zone B-2( Inside Side City-2) Abudhabi</strong>
                </td>
              </tr>
              <tr>
                <td>Mode of Payment:</td>
                <td>
                  <strong>Card on Delivery</strong>
                </td>
              </tr>
            </table>
          </div>
          <br />
          <br />
          <>
            <table
              className={css({
                width: '100%',
                borderCollapse: 'collapse',
                border: '1px solid black',
              })}
            >
              <thead>
                <tr>
                  <td
                    className={css({
                      width: '5%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    S.No
                  </td>
                  <td
                    className={css({
                      width: '32%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    Items
                  </td>
                  <td
                    className={css({
                      width: '20%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    Origin
                  </td>
                  <td
                    className={css({
                      width: '5%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    Packaging
                  </td>
                  <td
                    className={css({
                      width: '15%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    Rate (AED)
                  </td>
                  <td
                    className={css({
                      width: '5%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    Quantity
                  </td>
                  <td
                    className={css({
                      width: '18%',
                      padding: '5px',
                      borderCollapse: 'collapse',
                      border: '1px solid black',
                    })}
                  >
                    Total (AED - Incl. VAT)
                  </td>
                </tr>
              </thead>
              <tbody>
                {result.items.map((res, index) => {
                  return (
                    <tr key={index}>
                      <td
                        className={css({
                          width: '5%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        {index + 1}
                      </td>
                      <td
                        className={css({
                          width: '32%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        {res.name}
                      </td>
                      <td
                        className={css({
                          width: '20%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        UAE
                      </td>
                      <td
                        className={css({
                          width: '5%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        Box
                      </td>
                      <td
                        className={css({
                          width: '15%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        {Intl.NumberFormat('en-US', {
                          currency: result.orderCurrencyCode,
                          style: 'currency',
                        }).format(res.price ?? 0)}
                      </td>
                      <td
                        className={css({
                          width: '5%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        {res.qtyOrdered}
                      </td>
                      <td
                        className={css({
                          width: '18%',
                          padding: '5px',
                          borderCollapse: 'collapse',
                          border: '1px solid black',
                        })}
                      >
                        {Intl.NumberFormat('en-US', {
                          currency: result.orderCurrencyCode,
                          style: 'currency',
                        }).format(res.rowTotal ?? 0)}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </>
          <br />
          <br />

          <div
            className={css({
              display: 'flex',
              width: '400px',
              marginInlineStart: 'auto',
            })}
          >
            <table
              className={css({
                width: '100%',
                padding: '10px',
                border: '1px solid black',
              })}
            >
              <tr className={css({ textAlign: 'left' })}>
                <th className={css({ paddingBottom: '20px' })}>Total Amount</th>
              </tr>
              <tr>
                <th className={css({ fontWeight: '500', textAlign: 'left' })}>
                  Subtotal (Excl. VAT)
                </th>
                <td className={css({ textAlign: 'right' })}>
                  {result.subtotal &&
                    Intl.NumberFormat('en-US', {
                      currency: result.orderCurrencyCode,
                      style: 'currency',
                    }).format(result.subtotal)}{' '}
                </td>
              </tr>
              <tr>
                <th className={css({ fontWeight: '500', textAlign: 'left' })}>
                  Tax
                </th>
                <td className={css({ textAlign: 'right' })}>
                  {result.taxAmount &&
                    Intl.NumberFormat('en-US', {
                      currency: result.orderCurrencyCode,
                      style: 'currency',
                    }).format(result.taxAmount)}
                </td>
              </tr>
              <tr>
                <th className={css({ fontWeight: '500', textAlign: 'left' })}>
                  Shipping
                </th>
                <td className={css({ textAlign: 'right' })}>
                  {result.shippingAmount &&
                    Intl.NumberFormat('en-US', {
                      currency: result.orderCurrencyCode,
                      style: 'currency',
                    }).format(result.shippingAmount)}
                </td>
              </tr>
              <tr>
                <th className={css({ fontWeight: '500', textAlign: 'left' })}>
                  Discount
                </th>
                <td className={css({ textAlign: 'right' })}>
                  {result.discountAmount &&
                    Intl.NumberFormat('en-US', {
                      currency: result.orderCurrencyCode,
                      style: 'currency',
                    }).format(result.discountAmount)}
                </td>
              </tr>
              <tr>
                <th className={css({ fontWeight: '500', textAlign: 'left' })}>
                  Total
                </th>
                <td className={css({ textAlign: 'right' })}>
                  {result.grandTotal &&
                    Intl.NumberFormat('en-US', {
                      currency: result.orderCurrencyCode,
                      style: 'currency',
                    }).format(result.grandTotal ?? 0)}
                </td>
              </tr>
            </table>
          </div>
          <p className={css({ fontSize: '12px' })}>
            <strong>
              * This is System generated Invoice, no signature required.
            </strong>
          </p>
        </div>
      )}
      <div
        className={css({
          display: 'flex',
          justifyContent: 'flex-end',
          marginTop: '30px',
          marginRight: '20px',
        })}
      >
        <Button
          overrides={{
            BaseButton: {
              style: () => ({
                width: '120px',
                marginRight: '15px',
              }),
            },
          }}
          onClick={handlePrint}
        >
          Print
        </Button>
        <Button
          kind={KIND.secondary}
          overrides={{
            BaseButton: {
              style: () => ({
                width: '120px',
              }),
            },
          }}
          onClick={() => {
            navigate(-1);
          }}
        >
          Cancel
        </Button>
      </div>
    </>
  );
}

export default OrderInfoPrint;
