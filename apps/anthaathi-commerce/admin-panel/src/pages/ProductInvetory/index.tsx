import {
  CustomColumn,
  DataTable,
  Row,
  SortDirections,
  StringColumn,
} from 'baseui/data-table';
import { SORT_DIRECTION } from 'baseui/table';
import usePromise, { States } from '../../hooks/use-promise';
import { DataTableLoading } from '../../features/Common/Components/Loading';
import React, { useEffect, useMemo, useState } from 'react';
import { useStyletron } from 'baseui';
import {
  InventoryApiDataSourceItemInterface,
  InventorySourceItemsApi,
} from '../../__generated__';
import { FilterBuilder, FilterPageColumn } from '../../features/FilterBuilder';
import { Breadcrumbs } from 'baseui/breadcrumbs';
import { StyledLink } from 'baseui/link';
import { useRestConfig } from '../../hooks/use-rest-config';
import { Input } from 'baseui/input';
import { Button, SIZE } from 'baseui/button';
import { Pagination } from 'baseui/pagination';
import { usePage } from '../../features/FilterPage/hooks/usePage';

const useColumns = () => {
  return [
    {
      column: StringColumn({
        title: 'Product SKU',
        mapDataToValue: (data: InventoryApiDataSourceItemInterface) =>
          data.sku ?? '',
      }),
      key: 'sku',
      filterConfig: {},
    },
    {
      column: CustomColumn<InventoryApiDataSourceItemInterface, unknown>({
        title: 'Inventory',
        cellBlockAlign: 'center',
        renderCell: (cell) => {
          const [css] = useStyletron();

          const [state, setState] = useState(cell.value.quantity);
          const [previousSuccessValue, setPreviousSuccessValue] =
            useState(state);
          const [isUpdating, setIsUpdating] = useState(false);
          const restConfig = useRestConfig();

          return (
            <div className={css({ display: 'flex', alignItems: 'center' })}>
              <Input
                size={SIZE.compact}
                value={state}
                type="number"
                onChange={(e) => {
                  if (!isNaN(+e.target.value)) {
                    setState(+e.target.value);
                  }
                }}
              />
              <span className={css({ width: '12px' })} />
              <Button
                kind="secondary"
                size="compact"
                isLoading={isUpdating}
                onClick={() => {
                  setIsUpdating(true);
                  new InventorySourceItemsApi(restConfig)
                    .postV1InventorySourceitems({
                      postV1InventoryLowquantitynotificationsdeleteRequest: {
                        sourceItems: [
                          {
                            sku: cell.value.sku,
                            quantity: state,
                            status: cell.value.status,
                            sourceCode: cell.value.sourceCode,
                            extensionAttributes: cell.value.extensionAttributes,
                          },
                        ],
                      },
                    })
                    .then((docs) => {
                      setIsUpdating(false);
                      setPreviousSuccessValue(state);
                    })
                    .catch((e) => {
                      setIsUpdating(false);
                      setState(previousSuccessValue);
                    });
                }}
              >
                Update
              </Button>
            </div>
          );
        },
        mapDataToValue: (data) => data,
      }),
    },
  ] as FilterPageColumn[];
};

export default function () {
  const columns = useColumns();

  const [css, $theme] = useStyletron();
  const restConfig = useRestConfig();
  const [page, setPage] = usePage();

  const [result, error, state] = usePromise(() => {
    return new InventorySourceItemsApi(restConfig).getV1InventorySourceitems({
      searchCriteriaPageSize: 25,
      searchCriteriaCurrentPage: page,
    });
  }, [page]);

  const rows = useMemo(() => {
    return (
      (result?.items.map((res) => ({
        data: res,
        id: res.sku,
      })) as Row[]) ?? []
    );
  }, [result]);

  const numPages_ =
    (result?.totalCount ?? 0) / (result?.searchCriteria?.pageSize ?? 0);
  const currentPage_ = result?.searchCriteria?.currentPage ?? 1;

  const [numPages, setNumPages] = useState(numPages_);
  const [currentPage, setCurrentPage] = useState(currentPage_);

  useEffect(() => {
    if (!isNaN(numPages_) && numPages_ !== numPages) {
      setNumPages(numPages_);
    }
    if (!isNaN(currentPage_) && currentPage !== currentPage_) {
      setCurrentPage(currentPage_);
    }
  }, [currentPage_, numPages_]);

  const [sort, setSort] = useState<{
    index: number;
    direction: typeof SORT_DIRECTION.ASC | typeof SORT_DIRECTION.DESC;
  }>({ index: 0, direction: SORT_DIRECTION.ASC });

  return (
    <div
      className={css({
        margin: '0 auto',
        paddingInlineStart: '64px',
        paddingInlineEnd: '64px',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <div
        className={css({
          display: 'flex',
          alignItems: 'center',
          paddingTop: $theme.sizing.scale800,
          paddingBottom: $theme.sizing.scale800,
        })}
      >
        <Breadcrumbs>
          <StyledLink href="#parent">Admin</StyledLink>
          <StyledLink href="#sub">Product</StyledLink>
          <StyledLink href="#sub">Inventory</StyledLink>
        </Breadcrumbs>

        <span className={css({ flexGrow: 1 })} />
      </div>

      <FilterBuilder columns={columns} onFilter={() => {}} />

      <div className={css({ flexGrow: 1, paddingTop: '12px' })}>
        <DataTable
          sortIndex={sort.index}
          sortDirection={sort.direction}
          onSort={(columnIndex, sortDirection?: SortDirections) => {
            setSort({
              index: columnIndex,
              direction: sortDirection ?? SORT_DIRECTION.DESC,
            });
            return;
          }}
          resizableColumnWidths={true}
          loading={state === States.pending}
          columns={columns.map((res) => res.column)}
          loadingMessage={() => <DataTableLoading />}
          rowHeight={90}
          rows={rows}
        />
      </div>
      <Pagination
        numPages={Math.ceil(numPages)}
        currentPage={currentPage}
        onPageChange={({ nextPage }) => {
          setPage(nextPage);
        }}
      />
    </div>
  );
}
