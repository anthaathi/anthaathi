import React, { useEffect, useMemo, useState } from 'react';
import { useStyletron } from 'baseui';
import { DataTable, SortDirections } from 'baseui/data-table';
import { SalesDataOrderSearchResultInterface } from '../../__generated__';
import usePromise, { States } from '../../hooks/use-promise';
import { useRestConfig } from '../../hooks/use-rest-config';
import { columns } from './columns';
import { useRecoilValue } from 'recoil';
import { userInfoSelector } from '../../atoms/userToken';
import { Input, SIZE } from 'baseui/input';
import { Pagination } from 'baseui/pagination';
import { Breadcrumbs } from 'baseui/breadcrumbs';
import { StyledLink } from 'baseui/link';
import { ButtonGroup, SHAPE } from 'baseui/button-group';
import { Button } from 'baseui/button';
import { Spinner } from 'baseui/spinner';
import { OrdersAPIsExtended } from '../../services/orders';
import { FormControl } from 'baseui/form-control';
import { Cell, Grid } from 'baseui/layout-grid';
import { SORT_DIRECTION } from 'baseui/table';
import { Formik } from 'formik';
import { usePage } from '../../features/FilterPage/hooks/usePage';
import { FilterBuilder } from '../../features/FilterBuilder';
import { DataTableLoading } from '../../features/Common/Components/Loading';

export type Row<T> = {
  data: T;
  id: number | string;
};

const useColumns = () => {
  const userInfo = useRecoilValue(userInfoSelector);

  const authority = useMemo(() => {
    switch (userInfo.utypid) {
      case 2:
        return ['admin'];
    }

    return [];
  }, [userInfo]);

  return useMemo(() => columns({ authority }), [authority]);
};

const PAGE_SIZE = 40;

export default function Order() {
  const [css, $theme] = useStyletron();
  const config = useRestConfig();
  const orders = useMemo(() => new OrdersAPIsExtended(config), [config]);
  const [search, setSearch] = useState('');
  const _columns = useColumns();

  const columns = useMemo(() => {
    return _columns.map((res) => res.column);
  }, [_columns]);

  const [currentState, setCurrentState] = useState(0);

  const stringCols = useMemo(() => {
    const keys: string[] = [];

    _columns
      .filter((res) => res.column.kind === 'STRING')
      .forEach((res) => {
        if (Array.isArray(res.key)) {
          res.key.forEach((e) => {
            keys.push(e);
          });
        } else if (res.key) {
          keys.push(res.key);
        }
      });

    return keys;
  }, [_columns]);

  const [page, setPage] = usePage();

  const [sort, setSort] = useState<{
    index: number;
    direction: typeof SORT_DIRECTION.ASC | typeof SORT_DIRECTION.DESC;
  }>({ index: 0, direction: SORT_DIRECTION.ASC });

  const [result, error, state] =
    usePromise<SalesDataOrderSearchResultInterface>(
      () =>
        orders.getV1Orders({
          searchCriteriaCurrentPage: page,
          searchCriteriaPageSize: PAGE_SIZE,
          filterGroups: [
            search
              ? stringCols.map((res) => ({
                  field: res!!,
                  value: `%${search!!}%`,
                  conditionType: 'like',
                }))
              : [],
          ],
        }),
      [search, currentState, page]
    );

  const numPages_ =
    (result?.totalCount ?? 0) / (result?.searchCriteria?.pageSize ?? 0);
  const currentPage_ = result?.searchCriteria?.currentPage ?? 1;

  const [numPages, setNumPages] = useState(numPages_);
  const [currentPage, setCurrentPage] = useState(currentPage_);

  useEffect(() => {
    if (!isNaN(numPages_) && numPages_ !== numPages) {
      setNumPages(numPages_);
    }
    if (!isNaN(currentPage_) && currentPage !== currentPage_) {
      setCurrentPage(currentPage_);
    }
  }, [currentPage_, numPages_]);

  const rows = useMemo(
    () =>
      result?.items?.map((res) => ({
        data: res,
        id: res.incrementId ?? res.entityId?.toString() ?? '-',
      })) ?? [],
    [result]
  );

  return (
    <div
      className={css({
        margin: '0 auto',
        paddingInlineStart: '64px',
        paddingInlineEnd: '64px',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <div
        className={css({
          display: 'flex',
          alignItems: 'center',
          paddingTop: $theme.sizing.scale800,
        })}
      >
        <Breadcrumbs>
          <StyledLink href="#parent">Admin</StyledLink>
          <StyledLink href="#sub">Orders</StyledLink>
        </Breadcrumbs>

        <span className={css({ flexGrow: 1 })} />
      </div>

      <div
        className={css({
          paddingTop: $theme.sizing.scale800,
          paddingBottom: $theme.sizing.scale200,
          display: 'flex',
          alignItems: 'center',
        })}
      >
        <div className={css({ marginBottom: '12px' })}>
          <ButtonGroup size={SIZE.compact} selected={1} shape={SHAPE.pill}>
            <Button>Today</Button>
            <Button>Tomorrow</Button>
            <Button>All orders</Button>
          </ButtonGroup>
        </div>

        <span className={css({ flexGrow: 1 })} />

        <Button
          size={SIZE.compact}
          onClick={() => setCurrentState((prev) => prev + 1)}
        >
          Reload
        </Button>
      </div>

      <FilterBuilder columns={_columns} onFilter={console.log} />

      <div className={css({ flexGrow: 1, paddingTop: '12px' })}>
        <DataTable
          sortIndex={sort.index}
          sortDirection={sort.direction}
          onSort={(columnIndex, sortDirection?: SortDirections) => {
            setSort({
              index: columnIndex,
              direction: sortDirection ?? SORT_DIRECTION.DESC,
            });
            return;
          }}
          resizableColumnWidths={true}
          loading={state === States.pending}
          columns={columns}
          loadingMessage={() => <DataTableLoading />}
          rowHeight={90}
          rows={rows}
        />
      </div>

      <Pagination
        numPages={Math.ceil(numPages)}
        currentPage={currentPage}
        onPageChange={({ nextPage }) => {
          setPage(nextPage);
        }}
      />
    </div>
  );
}
