package org.anthaathi.commerce.address.entity

import org.anthaathi.common.CreateMailingAddressMessage
import org.anthaathi.common.MailingAddress
import java.util.*
import javax.persistence.*

@Table(name = "mailing_address")
@Entity
open class MailingAddressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "address1", nullable = true)
    open var address1: String? = null

    @Column(name = "address2", nullable = true)
    open var address2: String? = null

    @Column(name = "city", nullable = true)
    open var city: String? = null

    @Column(name = "company", nullable = true)
    open var company: String? = null

    @Column(name = "country", nullable = true)
    open var country: String? = null

    @Column(name = "first_name", nullable = true)
    open var firstName: String? = null

    @Column(name = "last_name", nullable = true)
    open var lastName: String? = null

    @Column(name = "phone", nullable = true)
    open var phone: String? = null

    @Column(name = "province", nullable = true)
    open var province: String? = null

    @Column(name = "province_code", nullable = true)
    open var provinceCode: String? = null

    @Column(name = "zip", nullable = true)
    open var zip: String? = null

    @Column(name = "customer_id", nullable = true)
    open var customerId: UUID? = null

    fun toGRPCType(): MailingAddress {
        return MailingAddress.newBuilder()
            .setId(id.toString())
            .setAddress1(address1)
            .setAddress2(address2)
            .setCity(city)
            .setCompany(company)
            .setCountry(country)
            .setFirstName(firstName)
            .setLastName(lastName)
            .setPhone(phone)
            .setProvince(province)
            .setProvinceCode(provinceCode)
            .setZip(zip)
            .build()
    }

    companion object {
        fun fromGRPCObject(input: CreateMailingAddressMessage): MailingAddressEntity {
            val returnValue = MailingAddressEntity()

            if (!input.customerId.isNullOrBlank()) {
                returnValue.customerId = UUID.fromString(input.customerId)
            }

            val address = input.address

            if (!address.id.isNullOrEmpty()) {
                returnValue.id = UUID.fromString(address.id)
            }

            returnValue.address1 = address.address1
            returnValue.address2 = address.address2
            returnValue.city = address.city
            returnValue.company = address.company
            returnValue.country = address.country
            returnValue.firstName = address.firstName
            returnValue.lastName = address.lastName
            returnValue.phone = address.phone
            returnValue.province = address.province
            returnValue.provinceCode = address.provinceCode
            returnValue.zip = address.zip

            return returnValue
        }
    }
}
