package org.anthaathi.commerce.address.repository

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import org.anthaathi.commerce.address.entity.MailingAddressEntity
import java.util.*
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class MailingAddressRepository : PanacheRepositoryBase<MailingAddressEntity, UUID> {
    fun findByCustomerId(customerId: UUID): List<MailingAddressEntity> {
        return find("customerId = ?1", customerId).list()
    }

    fun findByIdAndCustomerId(id: UUID, customerId: UUID): MailingAddressEntity {
        return find("customerId = ?1 and id = ?2", customerId, id).singleResult()
    }

    fun deleteByIdAndCustomerId(id: UUID, customerId: UUID): Boolean {
        return delete("customerId = ?1 and id = ?2", customerId, id) == 1L
    }
}
