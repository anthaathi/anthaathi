import React, { Suspense, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { BaseProvider, createLightTheme } from 'baseui';
import {
  createBrowserRouter,
  Navigate,
  Route,
  RouterProvider,
} from 'react-router-dom';
import { Provider as StyletronProvider } from 'styletron-react';
import { Client as Styletron } from 'styletron-engine-atomic';
import App from './App';
import { ApolloProvider } from '@apollo/client';
import { client } from './utils/apollo-client';
import { RecoilRoot, useRecoilValue } from 'recoil';
import { CartAtom, LoggedInAtom } from './atoms';
import { SnackbarProvider } from 'baseui/snackbar';

const engine = new Styletron({ prefix: '_' });

const Information = React.lazy(() => import('./pages/Information'));
const InformationWithLoggedIn = React.lazy(
  () => import('./pages/InformationWithLoggedInAccount'),
);
const Shipping = React.lazy(() => import('./pages/Shipping'));
const Payment = React.lazy(() => import('./pages/Payment'));

const lightTheme = createLightTheme();

const IsLoggedIn = ({
  children,
  noLoggedIn,
}: {
  children: React.ReactNode;
  noLoggedIn: React.ReactNode;
}) => {
  const authenticated = useRecoilValue(LoggedInAtom);

  if (authenticated) {
    return <>{children}</>;
  }

  return <>{noLoggedIn}</>;
};

const HasCart = ({ children }: { children: React.ReactNode }) => {
  const cart = useRecoilValue(CartAtom);

  if (cart) {
    return <>{children}</>;
  }

  return <Navigate to="/" replace />;
};

const Home = () => {
  useEffect(() => {
    window.location.reload();
  });

  return <></>;
};

const router = createBrowserRouter(
  [
    {
      path: '/checkout',
      element: (
        <HasCart>
          <App />
        </HasCart>
      ),
      children: [
        {
          path: '/checkout/information',
          element: (
            <Suspense fallback={<>Loading</>}>
              <IsLoggedIn noLoggedIn={<Information />}>
                <InformationWithLoggedIn />
              </IsLoggedIn>
            </Suspense>
          ),
        },
        {
          path: '/checkout/shipping',
          element: (
            <Suspense>
              <Shipping />
            </Suspense>
          ),
        },
        {
          path: '/checkout/payment',
          element: (
            <Suspense>
              <Payment />
            </Suspense>
          ),
        },
        {
          path: '/checkout',
          element: (
            <Suspense>
              <Navigate to="/checkout/information" replace />
            </Suspense>
          ),
        },
      ],
    },
    {
      path: '/',
      element: <Home />,
    },
  ],
  {},
);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Suspense fallback={'Loading...'}>
      <RecoilRoot>
        <StyletronProvider value={engine}>
          <ApolloProvider client={client}>
            <BaseProvider theme={lightTheme} zIndex={999}>
              <SnackbarProvider>
                <RouterProvider router={router} fallbackElement={<></>} />
              </SnackbarProvider>
            </BaseProvider>
          </ApolloProvider>
        </StyletronProvider>
      </RecoilRoot>
    </Suspense>
  </React.StrictMode>,
);
