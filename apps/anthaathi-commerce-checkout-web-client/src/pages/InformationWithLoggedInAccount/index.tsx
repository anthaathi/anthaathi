import { gql, useMutation } from '@apollo/client';
import {
  AssociateAddressToCheckoutMutation,
  AssociateAddressToCheckoutMutationVariables,
  GetCustomerAddressesQuery,
} from '../../__generated__/graphql';
import React, { useEffect, useState } from 'react';
import { Skeleton } from 'baseui/skeleton';
import { ALIGN, Radio, RadioGroup } from 'baseui/radio';
import { LabelLarge } from 'baseui/typography';
import { Button } from 'baseui/button';
import { useStyletron } from 'baseui';
import { NewAddress } from '../../Features/Account/components/NewAddress';
import { CartAtom } from '../../atoms';
import { useRecoilValue } from 'recoil';
import { useNavigate } from 'react-router-dom';
import { useQuery } from '../../Features/Core/hooks/useSuspenseQuery';

export default function InformationWithLoggedInAccount() {
  const { loading, data, error, refetch } = useQuery<GetCustomerAddressesQuery>(
    gql`
      query GetCustomerAddresses {
        __typename
        customer {
          addresses {
            id
            country_code
            city
            company
            firstname
            lastname
            street
            region {
              region
            }
          }
        }
      }
    `,
    {
      suspense: true,
    },
  );

  const [selectedAddress, setSelectedAddress] = useState<number | null>();

  const [css] = useStyletron();

  const [associateAddressToCheckout] = useMutation<
    AssociateAddressToCheckoutMutation,
    AssociateAddressToCheckoutMutationVariables
  >(gql`
    mutation AssociateAddressToCheckout($cartId: String!, $addressId: Int!) {
      setShippingAddressesOnCart(
        input: {
          cart_id: $cartId
          shipping_addresses: [{ customer_address_id: $addressId }]
        }
      ) {
        cart {
          id
        }
      }
    }
  `);

  const cart = useRecoilValue(CartAtom);

  const navigate = useNavigate();

  const onClick = () => {
    associateAddressToCheckout({
      variables: {
        cartId: JSON.parse(cart?.value!!),
        addressId: selectedAddress!!,
      },
    }).then(() => {
      navigate('/checkout/shipping');
    });
  };

  useEffect(() => {
    setSelectedAddress(data?.customer?.addresses?.[0]?.id);
  }, [loading]);

  if (loading) {
    return <Skeleton height="100px" width="100%" animation={true} />;
  }

  return (
    <>
      <LabelLarge marginBottom="scale600" marginTop="scale600">
        Your addresses
      </LabelLarge>
      <RadioGroup
        value={selectedAddress?.toString()}
        onChange={(e) => setSelectedAddress(+e.currentTarget.value)}
        name="address"
        align={ALIGN.vertical}
      >
        {data?.customer?.addresses?.map((res) => {
          return (
            <Radio
              value={res?.id?.toString()}
              key={res?.id?.toString()}
              description={
                (
                  <>
                    {res?.street} <br /> {res?.city} <br />{' '}
                    {res?.region?.region}
                  </>
                ) as never
              }
            >
              {res?.firstname} {res?.lastname}
            </Radio>
          );
        })}
      </RadioGroup>

      <div className={css({ paddingTop: '24px' })} />

      <NewAddress
        reload={() => {
          refetch();
        }}
      />

      <div className={css({ width: '100%', display: 'flex' })}>
        <span className={css({ flexGrow: 1 })} />
        <Button
          onClick={() => {
            onClick();
          }}
          colors={{backgroundColor: "#03703c", color: "white"}}
        >
          Save and Continue
        </Button>
      </div>
    </>
  );
}
