import { useNavigate } from 'react-router-dom';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { HeadingSmall, LabelLarge } from 'baseui/typography';
import { useStyletron } from 'baseui';
import {
  AddressForm,
  AddressFormValues,
} from '../../Features/ECommerce/Containers/AddressForm';
import { Button } from 'baseui/button';
import { useFormik } from 'formik';
import React, { useEffect, useState } from 'react';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import type {
  ApplyAddressMutation,
  ApplyAddressMutationVariables,
  AssociateCartToAccountMutation,
  AssociateCartToAccountMutationVariables,
  DoLoginMutation,
  GetAccountQuery,
  GetCartAddressQuery,
  GetCartAddressQueryVariables,
} from '../../__generated__/graphql';
import { useSnackbar } from 'baseui/snackbar';
import { useRecoilState, useRecoilValue } from 'recoil';
import { CartAtom, LoggedInAtom } from '../../atoms';
import { Skeleton } from 'baseui/skeleton';
import { useQuery } from '../../Features/Core/hooks/useSuspenseQuery';

export function validateEmail(email: string) {
  const res =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return res.test(String(email).toLowerCase());
}

export default function Information() {
  const [css] = useStyletron();
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: '',
      emailForOffer: false,
      saveInformation: false,
      password: '',
    },
    onSubmit: (values) => {},
  });

  const [applyAddressToCart, { loading: applyAddressToCartLoading }] =
    useMutation<ApplyAddressMutation, ApplyAddressMutationVariables>(gql`
      mutation ApplyAddress(
        $cartId: String!
        $address: CartAddressInput!
        $email: String!
      ) {
        setShippingAddressesOnCart(
          input: { cart_id: $cartId, shipping_addresses: { address: $address } }
        ) {
          cart {
            id
          }
        }

        setGuestEmailOnCart(input: { cart_id: $cartId, email: $email }) {
          cart {
            id
          }
        }
      }
    `);

  const cart = useRecoilValue(CartAtom);

  const { loading: addressLoading, data: addressInfo } = useQuery<
    GetCartAddressQuery,
    GetCartAddressQueryVariables
  >(
    gql`
      query GetCartAddress($cartId: String!) {
        cart(cart_id: $cartId) {
          email
          shipping_addresses {
            firstname
            lastname
            street
            telephone
            city
            company
            country {
              code
              label
            }
            postcode
            region {
              code
              label
              region_id
            }
          }
        }
      }
    `,
    {
      variables: {
        cartId: JSON.parse(cart?.value!!),
      },
      suspense: true,
    },
  );

  const [doesEmailExists, { data, error, loading }] =
    useLazyQuery<GetAccountQuery>(gql`
      query getAccount($email: String!) {
        isEmailAvailable(email: $email) {
          is_email_available
        }
      }
    `);

  const [doLogin, { loading: isDoLoginLoading }] =
    useMutation<DoLoginMutation>(gql`
      mutation doLogin($email: String!, $password: String!) {
        generateCustomerToken(email: $email, password: $password) {
          token
        }
      }
    `);

  const [associateCart] = useMutation<
    AssociateCartToAccountMutation,
    AssociateCartToAccountMutationVariables
  >(gql`
    mutation associateCartToAccount($cartId: String!) {
      assignCustomerToGuestCart(cart_id: $cartId) {
        id
      }
    }
  `);

  const onChange = (data: AddressFormValues) => {
    applyAddressToCart({
      variables: {
        email: formik.values.email,
        cartId: JSON.parse(cart?.value!!),
        address: {
          firstname: data.firstName,
          lastname: data.lastName,
          telephone: data.telephone,
          country_code: data.country,
          street: data.address.split('\n'),
          city: data.area,
          region_id: +data.emirate,
        },
      },
    })
      .then((docs) => {
        navigate('../shipping');
      })
      .catch((e) => {
        console.log(e.message);
      });
  };

  const { enqueue } = useSnackbar();

  const [, setUserToken] = useRecoilState(LoggedInAtom);
  const [cartId, setCartId] = useRecoilState(CartAtom);
  const [errorEmail, setErrorEmail] = useState('')

  const shippingAddress = addressInfo?.cart?.shipping_addresses?.[0];

  useEffect(() => {
    if (addressInfo?.cart?.email) {
      formik.setFieldValue('email', addressInfo?.cart?.email);
    }
  }, [addressInfo?.cart?.email]);

  return (
    <>
      <div className={css({ display: 'flex', alignItems: 'center' })}>
        <HeadingSmall
          className={css({ flex: 4 })}
          marginTop="scale100"
          marginBottom="scale400"
        >
          Contact Information
        </HeadingSmall>
        <span className={css({ flex: 2 })} />
      </div>
      <FormControl
        label="Email"
        htmlFor="email"
        caption={() => (errorEmail? errorEmail : null)}
        overrides={{
          Caption: {
            style: () => ({
              color: 'red',
            }),
          },
        }}
      >
        <Input
          id="email"
          name="email"
          type="text"
          onChange={(e) => {
            setErrorEmail('');
            const value = e.target.value;
            if (value === '') {
              setErrorEmail('Email is required!');
            } else {
              if (!validateEmail(value)) {
                setErrorEmail('Email is not valid!');
              }
            }
            doesEmailExists({
              variables: {
                email: value,
              },
            });

            formik.handleChange({
              target: {
                value,
                name: 'email',
              },
            });
          }}
          value={formik.values.email}
        />
      </FormControl>

      {data?.isEmailAvailable?.is_email_available === false && (
        <>
          <FormControl htmlFor="password" label="Password">
            <Input
              id="password"
              type="password"
              placeholder="Password"
              value={formik.values.password}
              onChange={formik.handleChange}
            />
          </FormControl>

          <div className={css({ display: 'flex', placeContent: 'flex-end' })}>
            <Button
              type="button"
              isLoading={isDoLoginLoading}
              onClick={() =>
                doLogin({
                  variables: {
                    email: formik.values.email,
                    password: formik.values.password,
                  },
                }).then((docs) => {
                  const errorMessage = docs.errors?.[0].message;

                  if (errorMessage) {
                    enqueue({
                      message: errorMessage,
                    });
                  } else {
                    const token = docs.data?.generateCustomerToken?.token;

                    if (token) {
                      setUserToken({
                        value: JSON.stringify(token),
                        timeStored: new Date().getTime(),
                      });
                    } else {
                      enqueue({ message: 'Unable to login' });
                    }
                    if (cartId?.value) {
                      associateCart({
                        variables: { cartId: JSON.parse(cartId.value) },
                        context: {
                          headers: {
                            Authorization: `Bearer ${token}`,
                          },
                        },
                      }).then((docs) => {
                        setCartId({
                          value: JSON.stringify(
                            docs.data?.assignCustomerToGuestCart?.id,
                          ),
                          timeStored: new Date().getTime(),
                        });
                      });
                    }
                  }
                })
              }
            >
              Login
            </Button>
          </div>
        </>
      )}

      <LabelLarge marginTop="scale600" marginBottom="scale600">
        Shipping address
      </LabelLarge>

      {addressLoading ? (
        <Skeleton width="100%" height="420px" />
      ) : (
        <AddressForm
          initialValue={{
            firstName: shippingAddress?.firstname ?? '',
            lastName: shippingAddress?.lastname ?? '',
            telephone: shippingAddress?.telephone!! ?? '',
            country: shippingAddress?.country.code as never,
            address: shippingAddress?.street.join('\n') ?? '',
            area: shippingAddress?.city ?? '',
            emirate: (shippingAddress?.region?.region_id as never) ?? '',
          }}
          isSubmitting={isDoLoginLoading}
          onChange={onChange}
          submitText="SAVE AND CONTINUE"
        />
      )}

      <div
        className={css({
          marginBottom: '12px',
        })}
      />
    </>
  );
}
