import { atom, AtomEffect } from 'recoil';

const localStorageEffect: (key: string, isJWT?: boolean) => AtomEffect<any> =
  (key, isJWT = false) =>
  ({ setSelf, onSet }) => {
    const savedValue = localStorage.getItem(key);
    if (savedValue != null) {
      const result = JSON.parse(JSON.parse(savedValue).value).split('.')[1];

      if (isJWT) {
        if (result) {
          const expTime = new Date(JSON.parse(atob(result)).exp * 1000);

          if (expTime.getTime() > new Date().getTime()) {
            setSelf(JSON.parse(savedValue));
          }
        }
      } else {
        setSelf(JSON.parse(savedValue));
      }
    }

    onSet((newValue, _, isReset) => {
      isReset || newValue === undefined || newValue === null
        ? localStorage.removeItem(key)
        : localStorage.setItem(key, JSON.stringify(newValue));
    });
  };

export const LoggedInAtom = atom<{ value: string; timeStored: number } | null>({
  default: null,
  key: 'M2_VENIA_BROWSER_PERSISTENCE__signin_token',
  effects: [
    localStorageEffect('M2_VENIA_BROWSER_PERSISTENCE__signin_token', true),
  ],
});

export const CartAtom = atom<{ value: string; timeStored: number } | null>({
  default: null,
  key: 'M2_VENIA_BROWSER_PERSISTENCE__cartId',
  effects: [localStorageEffect('M2_VENIA_BROWSER_PERSISTENCE__cartId')],
});
