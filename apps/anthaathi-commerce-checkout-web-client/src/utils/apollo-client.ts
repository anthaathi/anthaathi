import {
  ApolloClient,
  ApolloLink,
  createHttpLink,
  from,
  InMemoryCache,
} from '@apollo/client';

const httpLink = createHttpLink({ uri: '/graphql' });

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers = {} }) => {
    const userToken = localStorage.getItem(
      'M2_VENIA_BROWSER_PERSISTENCE__signin_token',
    );

    const newHeaders: Record<string, string> = {
      ...headers,
    };

    if (userToken) {
      newHeaders['Authorization'] = `Bearer ${JSON.parse(
        JSON.parse(userToken).value,
      )}`;
    }

    return {
      headers: newHeaders,
    };
  });
  return forward(operation);
});

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: '/graphql',
  link: from([authMiddleware, httpLink]),
});
