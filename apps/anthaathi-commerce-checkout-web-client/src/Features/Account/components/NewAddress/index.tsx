import { Button, KIND } from 'baseui/button';
import { Plus } from 'baseui/icon';
import React, { useState } from 'react';
import { useStyletron } from 'baseui';
import {
  Modal,
  ModalBody,
  ModalButton,
  ModalFooter,
  ModalHeader,
} from 'baseui/modal';
import { AddressForm } from '../../../ECommerce/Containers/AddressForm';
import { gql, useMutation } from '@apollo/client';
import {
  AddNewAddressForCustomerMutation,
  AddNewAddressForCustomerMutationVariables,
} from '../../../../__generated__/graphql';

export function NewAddress({ reload }: { reload: () => void }) {
  const [css] = useStyletron();
  const [isDialogOpen, setDialogOpen] = useState(false);

  const [addNewAddress, { loading }] = useMutation<
    AddNewAddressForCustomerMutation,
    AddNewAddressForCustomerMutationVariables
  >(gql`
    mutation addNewAddressForCustomer($input: CustomerAddressInput!) {
      createCustomerAddress(input: $input) {
        id
      }
    }
  `);

  return (
    <>
      <Button
        kind={KIND.secondary}
        endEnhancer={<Plus />}
        type="button"
        onClick={() => {
          setDialogOpen(true);
        }}
      >
        New Address
      </Button>

      <Modal
        isOpen={isDialogOpen}
        closeable
        animate
        autoFocus
        onClose={() => {
          setDialogOpen(false);
        }}
      >
        <ModalHeader>Add new Address</ModalHeader>

        <ModalBody>
          <AddressForm
            isSubmitting={loading}
            onChange={(values) => {
              addNewAddress({
                variables: {
                  input: {
                    firstname: values.firstName,
                    lastname: values.lastName,
                    telephone: values.telephone,
                    country_code: values.country,
                    street: values.address.split('\n'),
                    city: values.area,
                    region: {
                      region_id: +values.emirate,
                    },
                  },
                },
              }).then((docs) => {
                setDialogOpen(false);
                reload();
              });
            }}
            initialValue={{}}
          />
        </ModalBody>
      </Modal>
    </>
  );
}
