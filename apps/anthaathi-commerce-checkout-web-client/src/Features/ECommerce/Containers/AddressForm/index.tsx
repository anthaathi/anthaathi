import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Select } from 'baseui/select';
import { FlexGrid, FlexGridItem } from 'baseui/flex-grid';
import { useFormik } from 'formik';
import React, { useEffect } from 'react';
import { useStyletron } from 'baseui';
import { Textarea } from 'baseui/textarea';
import {
  CountryCodeEnum,
  GetCountriesQuery,
} from '../../../../__generated__/graphql';
import { Button } from 'baseui/button';
import { gql } from '@apollo/client';
import { useQuery } from '../../../Core/hooks/useSuspenseQuery';
import * as Yup from 'yup';

export interface AddressFormValues {
  firstName: string;
  lastName: string;
  address: string;
  apartment: string;
  area: string;
  emirate: number;
  country: CountryCodeEnum;
  telephone: string;
}

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required('First name is required.'),
  lastName: Yup.string().required('Last name is required.'),
  telephone: Yup.number().required('Phone number is required.'),
  area: Yup.number().required('Area is required.'),
  country: Yup.string().required('Country is required.'),
  emirate: Yup.string().required('Emirate is required.'),
  address: Yup.string().required('Address is required.'),
});

export function AddressForm({
  onChange,
  initialValue,
  isSubmitting,
  noSaveButton,
  submitText,
}: {
  initialValue: Partial<AddressFormValues>;
  onChange: (values: AddressFormValues) => void;
  isSubmitting?: boolean;
  noSaveButton?: boolean;
  submitText?: string;
}) {
  const [css, $theme] = useStyletron();
  const formik = useFormik<AddressFormValues>({
    initialValues: Object.assign(
      {
        firstName: '',
        lastName: '',
        address: '',
        apartment: '',
        area: '',//null as never as number,
        emirate: '',
        telephone: '',
        country: 'AE',
      },
      initialValue || {},
    ),
    validationSchema,
    onSubmit: (values) => {
      onChange(values);
    },
  });

  useEffect(() => {
    if (noSaveButton) {
      if (formik.isValid) {
        onChange(formik.values);
      }
    }
  });

  const {
    loading: loadingCountries,
    data: dataCountries,
    error: errorCountries,
  } = useQuery<GetCountriesQuery>(
    gql`
      query getCountries {
        countries {
          id
          full_name_english
          available_regions {
            id
            code
            name
          }
        }
      }
    `,
    {
      suspense: true,
    },
  );

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className={css({ marginTop: $theme.sizing.scale500 })}>
        <FlexGrid
          flexGridColumnCount={[1, 1, 2, 2]}
          flexGridColumnGap="scale700"
          flexGridRowGap="scale100"
        >
          <FlexGridItem>
            <FormControl
              label="First name"
              caption={() =>
                formik.errors.firstName ? formik.errors.firstName : null
              }
              overrides={{
                Caption: {
                  style: () => ({
                    color: 'red',
                  }),
                },
              }}
            >
              <Input
                placeholder="First name"
                id="firstName"
                name="firstName"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.firstName}
              />
            </FormControl>
          </FlexGridItem>
          <FlexGridItem>
            <FormControl
              label="Last name"
              caption={() =>
                formik.errors.lastName ? formik.errors.lastName : null
              }
              overrides={{
                Caption: {
                  style: () => ({
                    color: 'red',
                  }),
                },
              }}
            >
              <Input
                placeholder="Last name"
                id="lastName"
                name="lastName"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.lastName}
              />
            </FormControl>
          </FlexGridItem>
        </FlexGrid>

        <FormControl
          label="Phone Number"
          caption={() =>
            formik.errors.telephone ? formik.errors.telephone : null
          }
          overrides={{
            Caption: {
              style: () => ({
                color: 'red',
              }),
            },
          }}
        >
          <Input
            type="tel"
            name="telephone"
            id="phoneNumber"
            placeholder="Phone number"
            onChange={formik.handleChange}
            value={formik.values.telephone}
          />
        </FormControl>

        <FormControl
          label="Address"
          caption={() => (formik.errors.address ? formik.errors.address : null)}
          overrides={{
            Caption: {
              style: () => ({
                color: 'red',
              }),
            },
          }}
        >
          <Textarea
            placeholder="Address"
            id="address"
            name="address"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.address}
          />
        </FormControl>

        <FormControl
          label="Apartment, suite, etc. (option)"
          caption={() =>
            formik.errors.apartment ? formik.errors.apartment : null
          }
          overrides={{
            Caption: {
              style: () => ({
                color: 'red',
              }),
            },
          }}
        >
          <Input
            placeholder="Apartment, suite, etc. (option)"
            id="apartment"
            name="apartment"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.apartment}
          />
        </FormControl>

        <FormControl
          label="Country"
          caption={() => (formik.errors.country ? formik.errors.country : null)}
          overrides={{
            Caption: {
              style: () => ({
                color: 'red',
              }),
            },
          }}
        >
          <Select
            options={dataCountries?.countries?.map((country: any) => ({
              id: country?.id ?? '',
              label: country?.full_name_english,
            }))}
            isLoading={loadingCountries}
            value={[{ id: formik.values['country'] }]}
            onChange={(e) => {
              formik.handleChange({
                target: {
                  name: 'country',
                  value: e.value?.[0].id,
                },
              });
            }}
            placeholder="Country"
          />
        </FormControl>

        <FlexGrid
          flexGridColumnCount={[1, 1, 2, 2]}
          flexGridColumnGap="scale700"
          flexGridRowGap="scale100"
        >
          <FlexGridItem>
            <FormControl
              label="Emirate"
              caption={() =>
                formik.errors.emirate ? formik.errors.emirate : null
              }
              overrides={{
                Caption: {
                  style: () => ({
                    color: 'red',
                  }),
                },
              }}
            >
              <Select
                isLoading={loadingCountries}
                placeholder="Emirate"
                id="emirate"
                onChange={(e) => {
                  formik.handleChange({
                    target: { name: 'emirate', value: e.value?.[0].id },
                  });
                }}
                options={
                  dataCountries?.countries
                    ?.find((res: any) => res?.id === formik.values.country)
                    ?.available_regions?.map((region: any) => ({
                      id: region?.id!!,
                      label: region?.name!!,
                    })) || []
                }
                value={[{ id: formik.values.emirate }]}
              />
            </FormControl>
          </FlexGridItem>

          <FlexGridItem>
            <FormControl
              label="Area"
              caption={() => (formik.errors.area ? formik.errors.area : null)}
              overrides={{
                Caption: {
                  style: () => ({
                    color: 'red',
                  }),
                },
              }}
            >
              <Select
                placeholder="Area"
                id="area"
                options={[
                  {
                    label: 'Discovery Gardens',
                    id: 1,
                  },
                ]}
                onChange={(e) => {
                  formik.handleChange({
                    target: {
                      name: 'area',
                      value: e.value?.[0].id,
                    },
                  });
                }}
                value={[{ id: formik.values.area }]}
              />
            </FormControl>
          </FlexGridItem>
        </FlexGrid>
      </div>

      {!noSaveButton && (
        <div
          className={css({
            display: 'flex',
            placeContent: 'flex-end',
          })}
        >
          <Button
            isLoading={isSubmitting}
            type="submit"
            overrides={{
              BaseButton: {
                style: () => ({
                  backgroundColor: '#108a43',
                  ':hover': { backgroundColor: '#108a43' },
                }),
              },
            }}
          >
            {submitText || 'Save'}
          </Button>
        </div>
      )}
    </form>
  );
}
