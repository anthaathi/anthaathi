import { Input, SIZE } from 'baseui/input';
import { useStyletron } from 'baseui';
import { Button, SIZE as ButtonSize, KIND as ButtonKind } from 'baseui/button';
import { Drawer } from 'baseui/drawer';
import { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import { ApplyDiscountCodeToCartMutation } from '../../../../__generated__/graphql';
import { useRecoilValue } from 'recoil';
import { CartAtom } from '../../../../atoms';
import { FormControl } from 'baseui/form-control';

export function DiscountCode() {
  const [css, $theme] = useStyletron();
  const [isOpen, setIsOpen] = useState(false);
  const cart = useRecoilValue(CartAtom);
  const [couponCode, setCouponCode] = useState('');
  const [error, setError] = useState('');

  const [applyCode, { loading }] =
    useMutation<ApplyDiscountCodeToCartMutation>(gql`
      mutation ApplyDiscountCodeToCart($cardId: String!, $coupanCode: String!) {
        applyCouponToCart(
          input: { cart_id: $cardId, coupon_code: $coupanCode }
        ) {
          cart {
            id
          }
        }
      }
    `);

  return (
    <div
      className={css({
        paddingTop: '12px',
        paddingBottom: '12px',
        display: 'flex',
        flexDirection: 'row-reverse',
      })}
    >
      <Button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        size={ButtonSize.compact}
        kind={ButtonKind.tertiary}
        overrides={{
          Root: {
            style: {
              width: '120px',
              marginLeft: '6px',
              ':hover': {
                backgroundColor: '#f1fff1',
                textUnderlineOffset: '2px',
                textDecorationLine: 'underline',
              },
            },
          },
        }}
      >
        APPLY
      </Button>
      <Drawer
        isOpen={isOpen}
        autoFocus
        onClose={() => setIsOpen(!isOpen)}
        overrides={{
          DrawerBody: {
            style: ({ $theme }) => ({
              marginRight: 0,
              paddingRight: $theme.sizing.scale1000,
            }),
          },
        }}
      >
        <div>
          <div
            className={css({
              position: 'sticky',
              top: 0,
              backgroundColor: '#FFF',
            })}
          >
            <h2
              className={css({
                fontFamily: $theme.typography.HeadingMedium.fontFamily,
                marginTop: 0,
                marginBottom: 0,
              })}
            >
              Cart
            </h2>
            <div
              className={css({
                paddingTop: '12px',
                paddingBottom: '12px',
                display: 'flex',
              })}
            >
              <FormControl error={error}>
                <Input
                  size={SIZE.compact}
                  placeholder="Enter coupon code"
                  value={couponCode}
                  error={Boolean(error)}
                  onChange={(e) => setCouponCode(e.target.value)}
                />
              </FormControl>

              <div>
                <Button
                  size={ButtonSize.compact}
                  overrides={{
                    Root: { style: { width: '120px', marginLeft: '6px' } },
                  }}
                  isLoading={loading}
                  onClick={() => {
                    applyCode({
                      variables: {
                        cardId: JSON.parse(cart?.value ?? ''),
                        coupanCode: couponCode,
                      },
                    })
                      .then((docs) => {
                        setError('');
                        setIsOpen(false);
                      })
                      .catch((e) => {
                        setError(e.graphQLErrors?.[0]?.message ?? e.message);
                      });
                  }}
                >
                  APPLY
                </Button>
              </div>
            </div>
          </div>

          <DiscountCoupon
            imgSrc="https://1000logos.net/wp-content/uploads/2021/03/Paytm_Logo.png"
            title="Get up to 10 AED Paytm cashback using Paytm Wallet"
            subtitle="Valid on total value of items worth 100 AED or more."
            highlightedSubtitle="You will get up to 10 AED Paytm cashback with this code"
            couponCode="N E W M E M B E R"
            onApplyClick={() => {
              setIsOpen(!isOpen);
            }}
          />
          <DiscountCoupon
            imgSrc="https://mma.prnewswire.com/media/1699082/Simpl_Logo.jpg?w=200"
            title="Get 5% cashback on transactions above 500 AED"
            subtitle="Valid on total value of items worth 500 AED or more."
            highlightedSubtitle="You will get flat 5% discount on total price"
            couponCode="S I M P L"
            onApplyClick={() => {
              setIsOpen(!isOpen);
            }}
          />
          <DiscountCoupon
            imgSrc="https://assets.stickpng.com/images/580b57fcd9996e24bc43c530.png"
            title="Get up to 10% cashback using PayPal Wallet"
            subtitle="Valid on total value of items worth 100 AED or more."
            highlightedSubtitle="You will get up to 10 AED PayPal cashback with this code"
            couponCode="P A L"
            onApplyClick={() => {
              setIsOpen(!isOpen);
            }}
          />
          <DiscountCoupon
            imgSrc="https://1000logos.net/wp-content/uploads/2021/03/Paytm_Logo.png"
            title="Get up to 10 AED Paytm cashback using Paytm Wallet"
            subtitle="Valid on total value of items worth 100 AED or more."
            highlightedSubtitle="You will get up to 10 AED Paytm cashback with this code"
            couponCode="N E W M E M B E R"
            onApplyClick={() => {
              setIsOpen(!isOpen);
            }}
          />
          <DiscountCoupon
            imgSrc="https://mma.prnewswire.com/media/1699082/Simpl_Logo.jpg?w=200"
            title="Get 5% cashback on transactions above 500 AED"
            subtitle="Valid on total value of items worth 500 AED or more."
            highlightedSubtitle="You will get flat 5% discount on total price"
            couponCode="S I M P L"
            onApplyClick={() => {
              setIsOpen(!isOpen);
            }}
          />
        </div>
      </Drawer>
    </div>
  );
}

export interface DiscountCouponProps {
  imgSrc: string;
  title: string;
  subtitle: string;
  couponCode: string;
  highlightedSubtitle: string;
  onApplyClick: () => void;
}

export function DiscountCoupon(props: DiscountCouponProps) {
  const [css, $theme] = useStyletron();

  return (
    <div
      className={css({
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        padding: $theme.sizing.scale500,
        borderBottom: '1px solid #d9d9d9',
      })}
    >
      <div
        className={css({
          height: '50px',
          width: '100px',
          marginBottom: $theme.sizing.scale300,
          lineHeight: '1.5',
          letterSpacing: '0.05em',
        })}
      >
        <img
          src={props.imgSrc}
          className={css({
            height: '100%',
            width: '100%',
            objectFit: 'contain',
          })}
        />
      </div>
      <div
        className={css({
          ...$theme.typography.LabelLarge,
          marginBottom: $theme.sizing.scale300,
        })}
      >
        {props.title}
      </div>
      <div
        className={css({
          ...$theme.typography.LabelSmall,
          color: 'slategrey',
          marginBottom: $theme.sizing.scale300,
        })}
      >
        {props.subtitle}
      </div>
      <div
        className={css({
          marginBottom: $theme.sizing.scale300,
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          columnGap: $theme.sizing.scale500,
          rowGap: $theme.sizing.scale200,
        })}
      >
        <CouponCode title={props.couponCode} />
        <Button size={ButtonSize.compact} onClick={props.onApplyClick}>
          Apply
        </Button>
      </div>
      <div
        className={css({
          ...$theme.typography.LabelSmall,
          color: '#118b44',
          marginBottom: $theme.sizing.scale300,
        })}
      >
        {props.highlightedSubtitle}
      </div>
    </div>
  );
}

export function CouponCode(props: { title: string }) {
  const [css, $theme] = useStyletron();

  return (
    <div
      className={css({
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '150px',
        textAlign: 'center',
        backgroundColor: '#f5f5f5',
        border: '2px dashed #118b44',
        borderRadius: '5px',
        ...$theme.typography.LabelMedium,
      })}
    >
      {props.title}
    </div>
  );
}
