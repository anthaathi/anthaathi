import { useStyletron } from 'baseui';
import { VisuallyHiddenStyle } from '../../../Core/Style/VisuallyHidden';
import { gql } from '@apollo/client';
import {
  GetCartSummaryQuery,
  GetCartSummaryQueryVariables,
} from '../../../../__generated__/graphql';
import { useRecoilValue } from 'recoil';
import { CartAtom } from '../../../../atoms';
import { useEffect, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { useQuery } from '../../../Core/hooks/useSuspenseQuery';

export interface CartSummaryItem {
  title: string;
  content: string;
}

export function CartSummary() {
  const [css, $theme] = useStyletron();

  const cart = useRecoilValue(CartAtom);

  const { loading, data, refetch } = useQuery<
    GetCartSummaryQuery,
    GetCartSummaryQueryVariables
  >(
    gql`
      query GetCartSummary($cartId: String!) {
        cart(cart_id: $cartId) {
          prices {
            grand_total {
              value
              currency
            }
            applied_taxes {
              amount {
                currency
                value
              }
              label
            }
            discounts {
              label
              amount {
                currency
                value
              }
            }
            subtotal_excluding_tax {
              value
              currency
            }
            subtotal_including_tax {
              value
              currency
            }
          }
        }
      }
    `,
    {
      variables: {
        cartId: JSON.parse(cart?.value!!),
      },
      suspense: true,
    },
  );

  const currency = data?.cart?.prices?.grand_total?.currency;

  const location = useLocation();

  useEffect(() => {
    refetch({
      cartId: JSON.parse(cart?.value!!),
    });
  }, [location.pathname]);

  const intl = useMemo(() => {
    if (!currency) {
      return null;
    }

    return new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: currency,
    });
  }, [currency]);

  const items = useMemo(() => {
    return [
      ...(data?.cart?.prices?.discounts?.map((res) => ({
        title: res?.label,
        content: intl?.format(res?.amount?.value || 0) || '',
      })) || []),
      {
        title: 'Subtotal (Excl. VAT)',
        content: intl?.format(
          data?.cart?.prices?.subtotal_excluding_tax?.value ?? 0,
        ),
      },
      {
        title: 'Subtotal (Incl. VAT)',
        content: intl?.format(
          data?.cart?.prices?.subtotal_including_tax?.value ?? 0,
        ),
      },
    ].filter((res) => res.content) as CartSummaryItem[];
  }, []);

  if (loading) {
    return <></>;
  }

  return (
    <div className={css({})}>
      <table className={css({ width: '100%' })}>
        <caption className={css({ ...VisuallyHiddenStyle })}>
          Cost summary
        </caption>
        <thead>
          <tr>
            <th scope="col">
              <span className={css(VisuallyHiddenStyle)}>Description</span>
            </th>
            <th scope="col">
              <span className={css(VisuallyHiddenStyle)}>Price</span>
            </th>
          </tr>
        </thead>
        <tbody className={css({})}>
          {items.map((res, index) => (
            <tr
              className={css({ paddingBottom: '12px' })}
              key={res.title + index}
            >
              <th
                className={css({
                  ...$theme.typography.LabelSmall,
                  textAlign: 'left',
                })}
                scope="row"
              >
                {res.title}
              </th>
              <td className={css({ textAlign: 'right' })}>
                <span
                  className={css({
                    ...$theme.typography.LabelSmall,
                    textAlign: 'right',
                  })}
                  data-checkout-subtotal-price-target="7300"
                >
                  {res.content}
                </span>
              </td>
            </tr>
          ))}
        </tbody>
        <tfoot
          className={css({
            ':after': {
              content: '',
              position: 'absolute',
              width: '100%',
            },
          })}
        >
          <tr className={css({})}>
            <th className={css({ textAlign: 'left' })} scope="row">
              <span
                className={css({
                  ...$theme.typography.LabelLarge,
                  textAlign: 'left',
                  paddingBottom: '24px',
                })}
              >
                Total
              </span>
              <span
                className={css({
                  ...$theme.typography.LabelSmall,
                  display: 'block',
                  textAlign: 'left',
                })}
              >
                Including&nbsp;
                <span data-checkout-total-taxes-target="348">
                  {intl?.format(
                    data?.cart?.prices?.applied_taxes
                      ?.map((res) => res?.amount.value)
                      .reduce((res1, res2) => (res2 ?? 0) - (res1 ?? 0), 0) ??
                      0,
                  )}
                </span>{' '}
                in taxes
              </span>
            </th>
            <td
              className={css({ textAlign: 'right' })}
              data-presentment-currency="AED"
            >
              <span
                className={css({
                  ...$theme.typography.LabelSmall,
                  marginLeft: '6px',
                })}
              >
                {intl?.format(data?.cart?.prices?.grand_total?.value ?? 0)}
              </span>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
}
