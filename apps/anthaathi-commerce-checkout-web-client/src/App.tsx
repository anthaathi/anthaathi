import { Outlet } from 'react-router-dom';
import { Header } from './Features/Content/Components/Header';
import { useStyletron } from 'baseui';
import { CartProductList } from './Features/ECommerce/Components/CartProductList';
import { CartSummary } from './Features/ECommerce/Components/CartTotal';
import { DiscountCode } from './Features/ECommerce/Components/DiscountCode';
import { Breadcrumb } from './Features/Content/Components/Breadcrumb';
import { Accordion, Panel } from 'baseui/accordion';
import { useState } from 'react';
import { Toaster } from 'react-hot-toast';
function App() {
  const [css, $theme] = useStyletron();
  const [expand, setExpanded] = useState(false);

  return (
    <div
      className={css({
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <Header />
      <div
        className={css({
          flexGrow: 1,
          position: 'relative',
          display: 'flex',
          [$theme.mediaQuery.medium]: {
            ':after': {
              backgroundColor: '#f1fff1',
              width: '38%',
              height: '100%',
              top: 0,
              bottom: 0,
              position: 'absolute',
              right: 0,
              zIndex: 0,
              content: "''",
              boxShadow: '1px 0 0 rgba(0,0,0,0.1) inset',
            },
          },
        })}
      >
        <div
          className={css({
            display: 'flex',
            maxWidth: '1200px',
            width: '100%',
            marginLeft: 'auto',
            marginRight: 'auto',
            position: 'relative',
            zIndex: 1,
            flexGrow: 1,
          })}
        >
          <div
            className={css({
              width: '100%',
              [$theme.mediaQuery.large]: {
                width: 'calc(100% - 38%)',
              },
              [$theme.mediaQuery.medium]: {
                width: 'calc(100% - 48%)',
              },
              padding: $theme.sizing.scale600,
            })}
          >
            <Breadcrumb />

            <div className={css({ marginBottom: $theme.sizing.scale700 })} />

            <Accordion
              accordion
              overrides={{
                Root: {
                  style: {
                    display: 'block',
                    [$theme.mediaQuery.medium]: {
                      display: 'none',
                    },
                  },
                },
                Header: {
                  style: {
                    paddingLeft: $theme.sizing.scale200,
                    paddingRight: $theme.sizing.scale200,
                    paddingTop: $theme.sizing.scale100,
                    paddingBottom: $theme.sizing.scale100,
                    backgroundColor: '#f1fff1',
                  },
                },
                Content: {
                  style: {
                    backgroundColor: '#f1fff1',
                    paddingLeft: '0px',
                    paddingRight: '0px',
                    paddingTop: '0px',
                  },
                },
                PanelContainer: {
                  style: {
                    marginBottom: $theme.sizing.scale500,
                    backgroundColor: '#f1fff1',
                  },
                },
                ToggleIcon: {
                  component: () => <></>,
                },
              }}
              onChange={({ expanded }) => {
                if (expanded.length === 0) {
                  setExpanded(false);
                } else {
                  setExpanded(true);
                }
              }}
            >
              <Panel
                title={
                  <div
                    className={css({
                      display: 'flex',
                      alignItems: 'center',
                    })}
                  >
                    <div
                      className={css({
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                      })}
                    >
                      <span
                        className="fa fa-shopping-cart fa-lg"
                        aria-hidden="true"
                      />
                      <div
                        className={css({
                          marginLeft: $theme.sizing.scale200,
                          marginRight: $theme.sizing.scale200,
                          ...$theme.typography.HeadingXSmall,
                        })}
                      >
                        {expand ? 'Hide order summary' : 'Show order summary'}
                      </div>
                    </div>
                    <div
                      className={css({
                        flex: 1,
                      })}
                    >
                      {expand ? (
                        <span
                          className="fa fa-angle-up fa-lg"
                          aria-hidden="true"
                        />
                      ) : (
                        <span
                          className="fa fa-angle-down fa-lg"
                          aria-hidden="true"
                        />
                      )}
                    </div>
                  </div>
                }
              >
                <div
                  className={css({
                    display: 'block',
                    backgroundColor: '#f1fff1',
                    padding: '0.6rem',
                  })}
                >
                  <CartProductList />
                  <div
                    className={css({
                      paddingBottom: '1px',
                      backgroundColor: $theme.borders.border200.borderColor,
                    })}
                  />
                  <DiscountCode />
                  <div
                    className={css({
                      paddingBottom: '1px',
                      backgroundColor: $theme.borders.border200.borderColor,
                    })}
                  />
                  <CartSummary />
                </div>
              </Panel>
            </Accordion>

            <Outlet />
          </div>
          <div
            className={css({
              display: 'none',
              backgroundColor: 'white',
              padding: '2rem',
              [$theme.mediaQuery.medium || '']: {
                display: 'block',
                width: '48%',
                backgroundColor: '#f1fff1',
              },
              [$theme.mediaQuery.large]: {
                display: 'block',
                width: '38%',
                backgroundColor: '#f1fff1',
              },
            })}
          >
            <CartProductList />
            <div
              className={css({
                width: '100%',
                paddingBottom: '1px',
                backgroundColor: $theme.borders.border200.borderColor,
              })}
            />
            <DiscountCode />
            <div
              className={css({
                width: '100%',
                paddingBottom: '1px',
                backgroundColor: $theme.borders.border200.borderColor,
              })}
            />
            <CartSummary />
          </div>
        </div>
      </div>
      <Toaster />
    </div>
  );
}

export default App;
