package org.anthaathi.authentication.repository

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import org.anthaathi.authentication.entity.CustomerDefaultAddress
import java.util.*
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class CustomerDefaultAddressRepository : PanacheRepositoryBase<CustomerDefaultAddress, UUID> {
    fun updateDefaultAddress(customerId: UUID, addressId: UUID): Boolean {
        return update("defaultAddressId = ?1 WHERE id = ?2", addressId, customerId) == 1
    }
}
