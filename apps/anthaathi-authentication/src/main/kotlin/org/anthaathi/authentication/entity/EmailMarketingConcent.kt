package org.anthaathi.authentication.entity

import java.io.Serializable
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "email_marketing_concent")
open class EmailMarketingConcent : Serializable {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "state")
    open var state: String? = null

    @Column(name = "opt_in_level")
    open var optInLevel: String? = null

    @Column(name = "concent_updated_at")
    open var concentUpdatedAt: OffsetDateTime? = null

    companion object {
        private const val serialVersionUID = -3701237422492215337L
    }
}
