package org.anthaathi.authentication.service

import org.anthaathi.common.Customer
import org.keycloak.admin.client.Keycloak
import java.text.DateFormat
import java.time.Instant
import java.util.*
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class CustomerService(
    @Inject
    var keycloak: Keycloak
) {
    fun findById(id: UUID): Customer {
        val user = keycloak.realm("master").users().get(id.toString()).toRepresentation() ?: throw Exception("User not found")

        val df: DateFormat = DateFormat.getTimeInstance()
        df.timeZone = TimeZone.getTimeZone("utc")

        return Customer.newBuilder()
            .setFirstName(user.firstName.orEmpty())
            .setLastName(user.lastName.orEmpty())
            .setEmail(user.email.orEmpty())
            .setId(user.id)
            .setCreatedAt(df.format(Date(user.createdTimestamp)))
            .build()
    }
}
