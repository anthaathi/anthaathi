package org.anthaathi.authentication.entity

import java.io.Serializable
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "sms_marketing_consent")
open class SmsMarketingConsent : Serializable {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "date_created")
    open var dateCreated: OffsetDateTime? = null

    @Column(name = "date_updated")
    open var dateUpdated: OffsetDateTime? = null

    @Column(name = "state")
    open var state: String? = null

    @Column(name = "opt_in_level")
    open var optInLevel: String? = null

    @Column(name = "consent_updated_at")
    open var consentUpdatedAt: OffsetDateTime? = null

    @Column(name = "consent_collected_from")
    open var consentCollectedFrom: String? = null

    companion object {
        private const val serialVersionUID = 1859957304505701762L
    }
}
