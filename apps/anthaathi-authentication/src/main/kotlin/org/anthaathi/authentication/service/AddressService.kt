package org.anthaathi.authentication.service

import io.smallrye.mutiny.Uni
import org.anthaathi.authentication.entity.CustomerAddress
import org.anthaathi.authentication.repository.CustomerAddressRepository
import org.anthaathi.authentication.repository.CustomerDefaultAddressRepository
import org.anthaathi.common.*
import java.util.*
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.primaryConstructor

@ApplicationScoped
class AddressService(
    @Inject
    var customerAddress: CustomerAddressRepository,
    @Inject
    var customerDefaultAddressRepository: CustomerDefaultAddressRepository,
) {
    fun customerAddressCreate(request: CustomerAddressCreateInput): Uni<CustomerAddressCreateResponse> {
        return Uni.createFrom().item {
            val input = CustomerAddress.fromGRPCType(request.address)
            customerAddress.persist(input)

            CustomerAddressCreateResponse.newBuilder()
                .setCustomerAddress(input.toGRPCType())
                .build()
        }
    }

    fun customerAddressDelete(request: CustomerAddressDeleteInput): Uni<CustomerAddressDeleteResponse> {
        return Uni.createFrom().item {
            customerAddress.deleteCustomerAddress(UUID.fromString(request.customerId), UUID.fromString(request.id))

            CustomerAddressDeleteResponse
                .newBuilder()
                .setDeletedCustomerAddressId(request.customerId)
                .build()
        }
    }

    fun customerAddressUpdate(request: CustomerAddressUpdateInput): Uni<CustomerAddressUpdateResponse> {
        return Uni.createFrom().item {
            val address = customerAddress.findByCustomerIdAndId(UUID.fromString(request.customerId), UUID.fromString(request.id))

            val add = CustomerAddress.fromGRPCType(request.address)

            val c = add.merge(address)

            customerAddress.persist(c)

            CustomerAddressUpdateResponse
                .newBuilder()
                .setCustomerAddress(c.toGRPCType())
                .build()
        }
    }

    fun customerDefaultAddressUpdate(request: CustomerDefaultAddressUpdateInput?): Uni<CustomerDefaultAddressUpdateResponse> {
        return Uni.createFrom().item {
            request?.let {
                val updated = this.customerDefaultAddressRepository
                    .updateDefaultAddress(UUID.fromString(request.addressId), UUID.fromString(request.customerId))

                CustomerDefaultAddressUpdateResponse
                    .newBuilder()
                    .setIsUpdated(updated)
                    .build()
            }
        }
    }
}

inline infix fun <reified T : Any> T.merge(other: T): T {
    val propertiesByName = T::class.declaredMemberProperties.associateBy { it.name }
    val primaryConstructor = T::class.primaryConstructor
        ?: throw IllegalArgumentException("merge type must have a primary constructor")
    val args = primaryConstructor.parameters.associateWith { parameter ->
        val property = propertiesByName[parameter.name]
            ?: throw IllegalStateException("no declared member property found with name '${parameter.name}'")
        (property.get(this) ?: property.get(other))
    }
    return primaryConstructor.callBy(args)
}
