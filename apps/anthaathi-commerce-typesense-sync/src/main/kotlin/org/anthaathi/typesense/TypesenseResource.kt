package org.anthaathi.typesense

import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/update")
class TypesenseResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    fun update(): String {
        return ""
    }
}
